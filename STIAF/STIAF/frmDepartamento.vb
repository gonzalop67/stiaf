﻿Public Class frmDepartamento

    Private Sub frmDepartamento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPGetCiudades()
        ' Estado de los botones de comando
        ' En todos los casos de niveles de seguridad
        btnModificar.Enabled = False     ' Modificar
        btnEliminar.Enabled = False     ' Eliminar
        If CInt(VGNivelSeg) <= 4 Then  ' Invitados y Usuarios de otros módulos
            btnIngresar.Enabled = False  ' Ingresar
        End If
    End Sub

    Private Sub FPGetCiudades()
        ' Aquí obtengo las ciudades definidas en la base de datos
        Dim consulta As String = "SELECT * FROM ciudad ORDER BY ci_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "ciudad")
        Dim NumRegistros As Byte = registro.Tables("ciudad").Rows.Count
        ReDim VGCodCiudad(NumRegistros)
        cboCiudad.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 1 To NumRegistros
                VGCodCiudad(i - 1) = registro.Tables("ciudad").Rows(i - 1).Item("cod_ciudad")
                cboCiudad.Items.Add(registro.Tables("ciudad").Rows(i - 1).Item("ci_nombre"))
            Next i
            cboCiudad.SelectedIndex = 0
        End If
    End Sub

    Private Sub cboCiudad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCiudad.SelectedIndexChanged
        ' Aquí obtengo las sucursales asociadas
        FPGetSucursales()
    End Sub

    Private Sub FPGetSucursales()
        ' Aquí obtengo las sucursales asociadas a la ciudad escogida
        Dim consulta As String = "SELECT * FROM sucursal WHERE cod_ciudad = " + Trim(VGCodCiudad(cboCiudad.SelectedIndex))
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "sucursal")
        Dim NumRegistros As Byte = registro.Tables("sucursal").Rows.Count
        ReDim VGCodSucursal(NumRegistros)
        cboSucursal.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 1 To NumRegistros
                VGCodSucursal(i - 1) = registro.Tables("sucursal").Rows(i - 1).Item("cod_sucursal")
                cboSucursal.Items.Add(registro.Tables("sucursal").Rows(i - 1).Item("su_nombre"))
            Next i
            cboSucursal.SelectedIndex = 0
            FPGetDepartamentos()
        End If
    End Sub

    Private Sub FPGetDepartamentos()
        ' Aquí obtengo los departamentos asociados a la sucursal escogida
        Dim consulta As String = "SELECT * FROM departamento WHERE cod_sucursal = " + Trim(VGCodSucursal(cboSucursal.SelectedIndex))
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "departamento")
        Dim NumRegistros As Byte = registro.Tables("departamento").Rows.Count
        ReDim VGCodDepartamento(NumRegistros)
        lstDepartamentos.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 1 To NumRegistros
                VGCodDepartamento(i - 1) = registro.Tables("departamento").Rows(i - 1).Item("cod_departamento")
                lstDepartamentos.Items.Add(registro.Tables("departamento").Rows(i - 1).Item("de_nombre"))
            Next i
        End If
    End Sub

    Private Sub lstDepartamentos_DoubleClick(sender As Object, e As EventArgs) Handles lstDepartamentos.DoubleClick
        ' Primero obtengo el cod_sucursal
        TxtCodigo.Text = VGCodDepartamento(lstDepartamentos.SelectedIndex)
        FillFields()
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM departamento WHERE cod_departamento = " & Trim(TxtCodigo.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "departamento")
        Dim NumRegistros As Byte = registro.Tables("departamento").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar el departamento...")
        Else
            TxtNombre.Text = registro.Tables("departamento").Rows(0).Item("de_nombre")
        End If
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = False
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If Trim(TxtNombre.Text) = "" Then
            MsgBox("Debe digitar el dato para el nombre del Departamento.", vbExclamation, "Error en el ingreso de datos")
            TxtNombre.Focus()
        ElseIf FPBuscarCampo("departamento", "de_nombre", Trim(TxtNombre.Text)) Then
            MsgBox("El nombre del Departamento ya se encuentra ingresado " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtNombre.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla departamento
            VGStrSQL$ = "INSERT INTO departamento (cod_sucursal, de_nombre) VALUES ("
            ' Código de la sucursal
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodSucursal(cboSucursal.SelectedIndex)) & ","
            ' Nombre de la Sucursal
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtNombre.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido insertado", vbInformation, "STIAF")
            ' Estado de los botones de comando
            If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
                btnIngresar.Enabled = False
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            VGStrSQL$ = "SELECT max(cod_departamento) AS secuencial FROM departamento"
            Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "maximo")
            Dim NumRegistros As Byte = registro.Tables("maximo").Rows.Count
            If NumRegistros = 0 Then
                MsgBox("Ocurrió un error al recuperar el máximo cod_departamento...")
            Else
                TxtCodigo.Text = CStr(registro.Tables("maximo").Rows(0).Item("secuencial"))
            End If

            ' Actualizo la lista de sucursales
            FPGetDepartamentos()
            FPLimpiar()
        End If
    End Sub

    Private Sub FPLimpiar()
        ' Borrar el contenido de los campos en pantalla
        TxtCodigo.Text = ""
        TxtNombre.Text = ""
        TxtNombre.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ' Procedimiento para modificar el nombre del departamento
        VGStrSQL$ = "UPDATE departamento SET de_nombre = '"
        ' Campo que contiene el nombre
        VGStrSQL$ = VGStrSQL$ & Trim(TxtNombre.Text) & "'"
        ' Cláusula WHERE
        VGStrSQL$ = VGStrSQL$ & " where cod_departamento = " & TxtCodigo.Text
        Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
        comando.ExecuteNonQuery()
        MsgBox("El registro ha sido actualizado", vbInformation, "STIAF")
        FPGetDepartamentos()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla departamentos
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGStrSQL$ = "delete * from departamento where cod_departamento = " & TxtCodigo.Text
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido eliminado", vbInformation, "STIAF")
            FPLimpiar()
            FPGetDepartamentos()
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class