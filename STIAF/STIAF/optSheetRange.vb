﻿Imports System.Data
Imports System.Data.OleDb
Public Class optSheetRange
    Dim con As New OleDb.OleDbConnection()
    Dim connectionString As String = ""

    Dim sCampoElegido As String = ""
    Dim sTituloElegido As String = ""

    Dim NumRegistros As Byte ' Variable para almacenar el número de registros que devuelve una consulta

    Dim VLEmpresa(0, 3)   ' Array que contiene la información de las empresas
    Dim VLCodEmpresa, VLCodTipoActivo, VLCodCiudad, VLCodSucursal, VLCodUsuario As Integer
    Private Sub optSheetRange_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Integer
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        lblSheetPath.Text = "Archivo: " & sNameFile
        ' Aquí obtengo las empresas definidas en la base de datos
        Dim consulta As String = "SELECT * FROM empresa ORDER BY em_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "empresas")
        Dim NumRegistros As Byte = registro.Tables("empresas").Rows.Count
        ReDim VLEmpresa(NumRegistros, 3)
        cboEmpresa.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 0 To NumRegistros - 1
                cboEmpresa.Items.Add(registro.Tables("empresas").Rows(i).Item("em_nombre"))
                VLEmpresa(i, 1) = registro.Tables("empresas").Rows(i).Item("cod_empresa")
                VLEmpresa(i, 2) = registro.Tables("empresas").Rows(i).Item("em_nombre")
                VLEmpresa(i, 3) = i
            Next i
            cboEmpresa.SelectedIndex = 0
        End If
        'call to fill the list box with default
        FillSheetList()
        'Recupero los campos de la tabla "activo"
        consulta = "SELECT * FROM activo WHERE 1 = 2"
        adaptador = New OleDb.OleDbDataAdapter(consulta, cn)
        adaptador.Fill(registro, "activo")
        For i = 0 To registro.Tables("activo").Columns.Count - 1
            sNomCampo = registro.Tables("activo").Columns(i).ColumnName
            If sNomCampo <> "cod_activo" And _
                sNomCampo <> "cod_empresa" And _
                sNomCampo <> "Ingresado por el usuario" And _
                sNomCampo <> "Modificado por el usuario" And _
                sNomCampo <> "Fecha de Modificacion" And _
                sNomCampo <> "Asignado" Then
                lstCampos.Items.Add(registro.Tables("activo").Columns(i).ColumnName)
            End If
        Next
        ' resize the first column
        sprCampos.RowHeadersWidth = 23
        ' restablece el cursor normal
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub FillSheetList()
        'open the excel file using OLEDB
        lstSheetranges.Items.Clear()
        'Obtengo la extensión del nombre de archivo
        Dim extension As String = System.IO.Path.GetExtension(sNameFile)
        If extension = ".xls" Then 'Excel 97-2003 file
            OpenExcelFile(False)
        ElseIf extension = ".xlsx" Then 'Excel 2007 file
            OpenExcelFile(True)
        End If
    End Sub

    Private Sub OpenExcelFile(isOpenXMLFormat As Boolean)

        Dim schemaTable As DataTable

        If isOpenXMLFormat Then
            'read a 2007 file
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        sNameFile + ";Extended Properties='Excel 8.0;HDR=YES;'"
        Else
            'read a 97-2003 file
            connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                    sNameFile + ";Extended Properties=Excel 8.0;"
        End If
        Try
            con.ConnectionString = connectionString
            con.Open()

            'get all the available sheets
            schemaTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})

            'Enumere el nombre de las hojas de cálculo en el ListBox
            For i = 0 To schemaTable.Rows.Count - 1
                sNomCampo = schemaTable.Rows(i)!TABLE_NAME.ToString
                If isOpenXMLFormat Then
                    If Mid(sNomCampo, sNomCampo.Length - 1, 1) = "$" Then
                        lstSheetranges.Items.Add(Mid(sNomCampo, 2, sNomCampo.Length - 3))
                    End If
                Else
                    If Mid(sNomCampo, sNomCampo.Length, 1) = "$" Then
                        lstSheetranges.Items.Add(Mid(sNomCampo, 1, sNomCampo.Length - 1))
                    End If
                End If
            Next i

            con.Close()

            ' Si no salta algún error la conexión fue exitosa
        Catch ex As Exception
            ' Existe un error
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub butExit_Click(sender As Object, e As EventArgs) Handles butExit.Click
        Me.Close()
    End Sub

    Private Sub lstSheetranges_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstSheetranges.SelectedIndexChanged

        'make sure something is selected
        If IsNothing(lstSheetranges.SelectedItem) Then
            Exit Sub
        End If

        Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim con As New OleDb.OleDbConnection()
        con.ConnectionString = connectionString
        Dim cmd As New OleDb.OleDbDataAdapter("select * from [" + lstSheetranges.SelectedItem.ToString() + "$]", con)

        con.Open()
        Dim excelDataSet As New DataSet
        cmd.Fill(excelDataSet)
        con.Close()

        'bind the GridView to the DataTable
        faRangeSample.DataSource = excelDataSet.Tables(0).DefaultView

        'resize all columns' widths to fit the data
        For i = 0 To faRangeSample.Columns.Count - 1
            faRangeSample.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells)
        Next

        lstTitulos.Items.Clear()

        For i = 0 To faRangeSample.Columns.Count - 1
            lstTitulos.Items.Add(excelDataSet.Tables(0).Columns(i).ColumnName.ToString)
        Next i

        Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub lstCampos_DoubleClick(sender As Object, e As EventArgs) Handles lstCampos.DoubleClick
        FPAsociarCampos()
    End Sub

    Private Sub lstCampos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstCampos.SelectedIndexChanged
        sCampoElegido = lstCampos.SelectedItem
    End Sub

    Private Sub lstTitulos_DoubleClick(sender As Object, e As EventArgs) Handles lstTitulos.DoubleClick
        FPAsociarCampos()
    End Sub

    Private Sub lstTitulos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstTitulos.SelectedIndexChanged
        sTituloElegido = lstTitulos.SelectedItem
    End Sub

    Private Sub butAsociar_Click(sender As Object, e As EventArgs) Handles butAsociar.Click
        FPAsociarCampos()
    End Sub

    Private Sub FPAsociarCampos()
        ' Procedimiento para asociar los campos de la tabla con los titulos de la hoja Excel
        If sCampoElegido <> "" And sTituloElegido <> "" Then
            sprCampos.Rows.Add(sCampoElegido, sTituloElegido)
            lstCampos.Items.RemoveAt(lstCampos.SelectedIndex)
            lstTitulos.Items.RemoveAt(lstTitulos.SelectedIndex)
        End If
    End Sub

    Private Sub butDesAsociar_Click(sender As Object, e As EventArgs) Handles butDesAsociar.Click
        Dim rowToDelete As Int32 = sprCampos.Rows.GetFirstRow(DataGridViewElementStates.Selected)
        If rowToDelete > -1 Then
            lstCampos.Items.Add(sprCampos.CurrentRow.Cells("Column1").Value.ToString)
            lstTitulos.Items.Add(sprCampos.CurrentRow.Cells("Column2").Value.ToString)
            sprCampos.Rows.RemoveAt(rowToDelete)
        Else
            MsgBox("Elija los campos a quitar la asociación.", vbInformation, "STIAF")
        End If
    End Sub

    Private Sub butLoad_Click(sender As Object, e As EventArgs) Handles butLoad.Click
        On Error GoTo Error_MayCauseAnError
        If cboEmpresa.SelectedIndex < 0 Then
            MsgBox("No se han definido Empresas.", vbInformation, "STIAF")
        ElseIf faRangeSample.Rows.Count = 0 Then
            MsgBox("Debe elegir la hoja de cálculo a procesar.", vbInformation, "STIAF")
        ElseIf sprCampos.Rows.Count = 0 Then
            MsgBox("Debe asociar los campos con los títulos de la hoja de cálculo.", vbInformation, "STIAF")
        ElseIf cboEmpresa.SelectedIndex > -1 And faRangeSample.Rows.Count > 0 Then
            ' Aquí subo los datos del grid a access
            Cursor = System.Windows.Forms.Cursors.WaitCursor
            ' Primero verifico si hay registros para eliminar
            Sql = "SELECT * FROM activo WHERE cod_empresa = " & VLCodEmpresa
            Dim da As New OleDb.OleDbDataAdapter(Sql, cn)
            Dim ds As New DataSet
            da.Fill(ds, "tabla")
            If ds.Tables("tabla").Rows.Count > 0 Then
                ' Primero elimino los registros de la tabla activo de la empresa seleccionada
                Sql = "DELETE * FROM activo WHERE cod_empresa = " & VLCodEmpresa
                Dim comando As New OleDb.OleDbCommand(Sql, cn)
                comando.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
            End If
            Focos.Maximum = faRangeSample.Rows.Count
            For Each filafa As DataGridViewRow In faRangeSample.Rows
                ' Acá formamos la consulta de inserción de datos
                VGStrSQL = "INSERT INTO activo (cod_empresa,"
                For Each fila As DataGridViewRow In sprCampos.Rows
                    VGStrSQL = VGStrSQL & "[" & fila.Cells("Column1").Value.ToString & "],"
                Next
                VGStrSQL = VGStrSQL & "Asignado,[Fecha de Modificacion]) values (" & Trim(VLCodEmpresa) & ","
                ' Recorremos las columnas del faRangeSample
                For Each columna As DataGridViewColumn In faRangeSample.Columns
                    For Each fila As DataGridViewRow In sprCampos.Rows
                        sTituloElegido = fila.Cells("Column2").Value.ToString
                        If sTituloElegido = columna.Name Then
                            Select Case UCase(columna.Name)
                                Case "TIPO DE ACTIVO"
                                    ' Primero verifico si no existe el registro
                                    Sql = "SELECT * FROM tipo_activo WHERE descripcion = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim registro As New DataSet
                                    adaptador.Fill(registro, "tabla")
                                    If registro.Tables("tabla").Rows.Count > 0 Then
                                        ' Existe el registro, hay que asociar el cod_tipo_activo
                                        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("cod_tipo_activo")) & ","
                                        VLCodTipoActivo = Trim(registro.Tables("tabla").Rows(0).Item("cod_tipo_activo"))
                                    Else
                                        ' No existe hay que crear primero el registro
                                        Sql = "INSERT INTO tipo_activo (descripcion) VALUES ('" & _
                                           Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                        Sql = "SELECT MAX(cod_tipo_activo) FROM tipo_activo"
                                        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                        Dim dsT As New DataSet
                                        daT.Fill(dsT, "maximo")
                                        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                        VLCodTipoActivo = Trim(dsT.Tables("maximo").Rows(0).Item(0))
                                    End If
                                    Exit For
                                Case "CLASE DE ACTIVO"
                                    ' Primero verifico si no existe el registro
                                    Sql = "SELECT * FROM clase_activo WHERE descripcion = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim registro As New DataSet
                                    adaptador.Fill(registro, "tabla")
                                    If registro.Tables("tabla").Rows.Count > 0 Then
                                        ' Existe el registro, hay que asociar el cod_clase_activo
                                        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("cod_clase_activo")) & ","
                                    Else
                                        ' No existe hay que crear primero el registro
                                        Sql = "INSERT INTO clase_activo (cod_tipo_activo, descripcion) VALUES (" & _
                                            Trim(VLCodTipoActivo) & ",'" & _
                                            Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                        Sql = "SELECT MAX(cod_clase_activo) FROM clase_activo"
                                        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                        Dim dsT As New DataSet
                                        daT.Fill(dsT, "maximo")
                                        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                    End If
                                    Exit For
                                    'Case "USUARIO"
                                    '    ' Primero verifico si no existe el registro
                                    '    Sql = "SELECT * FROM usuario_toma WHERE nombre = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    '    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    '    Dim registro As New DataSet
                                    '    adaptador.Fill(registro, "tabla")
                                    '    If registro.Tables("tabla").Rows.Count > 0 Then
                                    '        ' Existe el registro, hay que asociar el id_usuario
                                    '        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("id_usuario")) & ","
                                    '        VLCodUsuario = Trim(registro.Tables("tabla").Rows(0).Item("id_usuario"))
                                    '    Else
                                    '        ' No existe hay que crear primero el registro
                                    '        Sql = "INSERT INTO usuario_toma (nombre) VALUES ('" & _
                                    '            Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                    '        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                    '        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                    '        Sql = "SELECT MAX(id_usuario) FROM usuario_toma"
                                    '        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    '        Dim dsT As New DataSet
                                    '        daT.Fill(dsT, "maximo")
                                    '        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                    '        VLCodUsuario = Trim(dsT.Tables("maximo").Rows(0).Item(0))
                                    '    End If
                                    '    Exit For
                                    'Case "COD USUARIO"
                                    '    ' Aquí actualizo el Código del Usuario para la Toma
                                    '    Sql = "UPDATE usuario_toma SET codigo = '" + filafa.Cells(columna.Name).Value.ToString + "' WHERE id_usuario = " + CStr(VLCodUsuario)
                                    '    Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                    '    comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                    '    VGStrSQL = VGStrSQL & "'" & Trim(filafa.Cells(columna.Name).Value.ToString) & "',"
                                    '    Exit For
                                Case "CIUDAD"
                                    ' Primero verifico si no existe el registro
                                    Sql = "SELECT * FROM ciudad WHERE ci_nombre = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim registro As New DataSet
                                    adaptador.Fill(registro, "tabla")
                                    If registro.Tables("tabla").Rows.Count > 0 Then
                                        ' Existe el registro, hay que asociar el cod_clase_activo
                                        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("cod_ciudad")) & ","
                                        VLCodCiudad = Trim(registro.Tables("tabla").Rows(0).Item("cod_ciudad"))
                                    Else
                                        ' No existe hay que crear primero el registro
                                        Sql = "INSERT INTO ciudad (ci_nombre) VALUES ('" & _
                                            Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                        Sql = "SELECT MAX(cod_ciudad) FROM ciudad"
                                        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                        Dim dsT As New DataSet
                                        daT.Fill(dsT, "maximo")
                                        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                        VLCodCiudad = Trim(dsT.Tables("maximo").Rows(0).Item(0))
                                    End If
                                    Exit For
                                Case "SUCURSAL"
                                    ' Primero verifico si no existe el registro
                                    Sql = "SELECT * FROM sucursal WHERE su_nombre = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim registro As New DataSet
                                    adaptador.Fill(registro, "tabla")
                                    If registro.Tables("tabla").Rows.Count > 0 Then
                                        ' Existe el registro, hay que asociar el cod_sucursal
                                        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("cod_sucursal")) & ","
                                        VLCodSucursal = Trim(registro.Tables("tabla").Rows(0).Item("cod_sucursal"))
                                    Else
                                        ' No existe hay que crear primero el registro
                                        Sql = "INSERT INTO sucursal (cod_ciudad, su_nombre) VALUES (" & _
                                            Trim(VLCodCiudad) & ",'" & _
                                            Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                        Sql = "SELECT MAX(cod_sucursal) FROM sucursal"
                                        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                        Dim dsT As New DataSet
                                        daT.Fill(dsT, "maximo")
                                        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                        VLCodSucursal = Trim(dsT.Tables("maximo").Rows(0).Item(0))
                                    End If
                                    Exit For
                                Case "DEPARTAMENTO"
                                    ' Primero verifico si no existe el registro
                                    Sql = "SELECT * FROM departamento WHERE de_nombre = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim registro As New DataSet
                                    adaptador.Fill(registro, "tabla")
                                    If registro.Tables("tabla").Rows.Count > 0 Then
                                        ' Existe el registro, hay que asociar el cod_clase_activo
                                        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("cod_departamento")) & ","
                                    Else
                                        ' No existe hay que crear primero el registro
                                        Sql = "INSERT INTO departamento (cod_sucursal, de_nombre) VALUES (" & _
                                            Trim(VLCodSucursal) & ",'" & _
                                            Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                        Sql = "SELECT MAX(cod_departamento) FROM departamento"
                                        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                        Dim dsT As New DataSet
                                        daT.Fill(dsT, "maximo")
                                        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                    End If
                                    Exit For
                                Case "SITUACION ACTIVO"
                                    ' Primero verifico si no existe el registro
                                    Sql = "SELECT * FROM situacion WHERE descripcion = '" + filafa.Cells(columna.Name).Value.ToString + "'"
                                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim registro As New DataSet
                                    adaptador.Fill(registro, "tabla")
                                    If registro.Tables("tabla").Rows.Count > 0 Then
                                        ' Existe el registro, hay que asociar el cod_clase_activo
                                        VGStrSQL = VGStrSQL & Trim(registro.Tables("tabla").Rows(0).Item("cod_situacion")) & ","
                                    Else
                                        ' No existe hay que crear primero el registro
                                        Sql = "INSERT INTO situacion (descripcion) VALUES ('" & _
                                            Trim(filafa.Cells(columna.Name).Value.ToString) & "')"
                                        Dim comandoT As New OleDb.OleDbCommand(Sql, cn)
                                        comandoT.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                                        Sql = "SELECT MAX(cod_situacion) FROM situacion"
                                        Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                        Dim dsT As New DataSet
                                        daT.Fill(dsT, "maximo")
                                        VGStrSQL = VGStrSQL & Trim(dsT.Tables("maximo").Rows(0).Item(0).ToString) & ","
                                    End If
                                    Exit For
                                Case "VALOR ADQUISICION", "PERIODO DEPRECIACION"
                                    If Trim(filafa.Cells(columna.Name).Value.ToString) = "" Then
                                        VGStrSQL = VGStrSQL & "0,"
                                    Else
                                        VGStrSQL = VGStrSQL & FPCambiarComaAPunto(filafa.Cells(columna.Name).Value.ToString) & ","
                                    End If
                                    Exit For
                                Case "FEC UBIC ACTIVO"
                                    If Trim(filafa.Cells(columna.Name).Value.ToString) = "" Then VGStrSQL = VGStrSQL & "#" & Format(Now, "dd/MM/yyyy") & "#,"
                                    Exit For
                                Case Else
                                    VGStrSQL = VGStrSQL & "'" & FPReemplazarComillas(FPCambiarComaAPunto(filafa.Cells(columna.Name).Value.ToString)) & "',"
                                    Exit For
                            End Select
                        End If
                    Next
                Next
                VGStrSQL = VGStrSQL & "false,#" & Format(Now, "dd/MM/yyyy") & "#)"
                Dim comandoF As New OleDb.OleDbCommand(VGStrSQL, cn)
                comandoF.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                'Actualizo el "avance" del progressbar
                Contador = Contador + 1
                If Contador < Focos.Maximum Then Focos.Value = Contador
            Next
            Sql = "SELECT * FROM activo WHERE [Descripcion 1] = ''"
            Dim daD As New OleDb.OleDbDataAdapter(Sql, cn)
            Dim dsD As New DataSet
            daD.Fill(dsD, "tabla")
            If dsD.Tables("tabla").Rows.Count > 0 Then
                'Elimino los registros en blanco
                Sql = "DELETE * FROM activo WHERE [Descripcion 1] = ''"
                Dim comandoD As New OleDb.OleDbCommand(Sql, cn)
                comandoD.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...
                Contador = Contador - dsD.Tables("tabla").Rows.Count
            End If
            MsgBox("Total de Registros Insertados: " & Contador)
            Cursor = System.Windows.Forms.Cursors.Default
            Me.Close()
        End If
        Exit Sub
Error_MayCauseAnError:
        MsgBox("Error en la aplicación: " & Err.Description.ToString)
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        ' Aquí se obtiene el código de la empresa
        For i = 0 To UBound(VLEmpresa, 1) - 1
            If VLEmpresa(i, 2) = cboEmpresa.SelectedItem Then
                VLCodEmpresa = VLEmpresa(i, 1)
                Exit For
            End If
        Next i
    End Sub
End Class