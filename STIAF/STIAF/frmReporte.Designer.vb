﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdIrAExcel = New System.Windows.Forms.Button()
        Me.cmdSalir = New System.Windows.Forms.Button()
        Me.txtTotalValorCompra = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdActivos = New System.Windows.Forms.DataGridView()
        CType(Me.grdActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdIrAExcel
        '
        Me.cmdIrAExcel.Location = New System.Drawing.Point(485, 330)
        Me.cmdIrAExcel.Name = "cmdIrAExcel"
        Me.cmdIrAExcel.Size = New System.Drawing.Size(75, 23)
        Me.cmdIrAExcel.TabIndex = 14
        Me.cmdIrAExcel.Text = "Ir a Excel"
        Me.cmdIrAExcel.UseVisualStyleBackColor = True
        '
        'cmdSalir
        '
        Me.cmdSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdSalir.Location = New System.Drawing.Point(566, 330)
        Me.cmdSalir.Name = "cmdSalir"
        Me.cmdSalir.Size = New System.Drawing.Size(75, 23)
        Me.cmdSalir.TabIndex = 13
        Me.cmdSalir.Text = "Salir"
        Me.cmdSalir.UseVisualStyleBackColor = True
        '
        'txtTotalValorCompra
        '
        Me.txtTotalValorCompra.Location = New System.Drawing.Point(135, 332)
        Me.txtTotalValorCompra.Name = "txtTotalValorCompra"
        Me.txtTotalValorCompra.Size = New System.Drawing.Size(100, 20)
        Me.txtTotalValorCompra.TabIndex = 12
        Me.txtTotalValorCompra.Text = "0.00"
        Me.txtTotalValorCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 335)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Total Valor de Compra:"
        '
        'grdActivos
        '
        Me.grdActivos.AllowUserToAddRows = False
        Me.grdActivos.AllowUserToDeleteRows = False
        Me.grdActivos.AllowUserToOrderColumns = True
        Me.grdActivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdActivos.Location = New System.Drawing.Point(2, 0)
        Me.grdActivos.Name = "grdActivos"
        Me.grdActivos.ReadOnly = True
        Me.grdActivos.RowHeadersWidth = 55
        Me.grdActivos.Size = New System.Drawing.Size(639, 324)
        Me.grdActivos.TabIndex = 10
        '
        'frmReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 353)
        Me.Controls.Add(Me.cmdIrAExcel)
        Me.Controls.Add(Me.cmdSalir)
        Me.Controls.Add(Me.txtTotalValorCompra)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.grdActivos)
        Me.Name = "frmReporte"
        Me.Text = "Reporte"
        CType(Me.grdActivos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdIrAExcel As System.Windows.Forms.Button
    Friend WithEvents cmdSalir As System.Windows.Forms.Button
    Friend WithEvents txtTotalValorCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grdActivos As System.Windows.Forms.DataGridView
End Class
