﻿Public Class frmUsuarioToma

    Private Sub frmUsuarioToma_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPGetUsuarios()
        ' Estado de los botones de comando
        ' En todos los casos de niveles de seguridad
        btnModificar.Enabled = False     ' Modificar
        btnEliminar.Enabled = False     ' Eliminar
        If CInt(VGNivelSeg) <= 4 Then  ' Invitados y Usuarios de otros módulos
            btnIngresar.Enabled = False  ' Ingresar
        End If
    End Sub

    Private Sub FPGetUsuarios()
        ' Obtengo las ciudades ingresados en la base de datos
        Dim consulta As String = "SELECT * FROM usuario_toma ORDER BY nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "usuario")
        Dim NumRegistros As Byte = registro.Tables("usuario").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("No existen usuarios para la toma...")
        Else
            ReDim VGCodUsuarioToma(NumRegistros)
            lstUsuarios.Items.Clear()
            For i = 1 To NumRegistros
                VGCodUsuarioToma(i - 1) = registro.Tables("usuario").Rows(i - 1).Item("id_usuario")
                lstUsuarios.Items.Add(registro.Tables("usuario").Rows(i - 1).Item("nombre"))
            Next i
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lstUsuarios_DoubleClick(sender As Object, e As EventArgs) Handles lstUsuarios.DoubleClick
        ' Primero obtengo el cod_ciudad
        Id_Usuario.Text = VGCodUsuarioToma(lstUsuarios.SelectedIndex)
        FillFields()
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM usuario_toma WHERE id_usuario = " & Trim(Id_Usuario.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "usuario")
        Dim NumRegistros As Byte = registro.Tables("usuario").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar el usuario para la toma...")
        Else
            TxtCodigo.Text = registro.Tables("usuario").Rows(0).Item("codigo")
            TxtNombre.Text = registro.Tables("usuario").Rows(0).Item("nombre")
        End If
        TxtNombre.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = False
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If Trim(TxtNombre.Text) = "" Then
            MsgBox("Debe digitar el dato para el nombre del Usuario para la Toma.", vbExclamation, "Error en el ingreso de datos")
            TxtNombre.Focus()
        ElseIf Trim(TxtCodigo.Text) = "" Then
            MsgBox("Debe digitar el dato para el código del Usuario para la Toma.", vbExclamation, "Error en el ingreso de datos")
            TxtCodigo.Focus()
        ElseIf FPBuscarCampo("usuario_toma", "nombre", Trim(TxtNombre.Text)) Then
            MsgBox("El nombre del Usuario para la Toma ya se encuentra ingresado " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtNombre.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla usuario_toma
            VGStrSQL$ = "INSERT INTO usuario_toma (nombre, codigo) VALUES ("
            ' Nombre del Usuario para la Toma
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtNombre.Text) & "',"
            ' Código del Usuario para la Toma
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtCodigo.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido insertado", vbInformation, "STIAF")
            ' Estado de los botones de comando
            If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
                btnIngresar.Enabled = False
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            VGStrSQL$ = "SELECT max(id_usuario) AS secuencial FROM usuario_toma"
            Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "maximo")
            Dim NumRegistros As Byte = registro.Tables("maximo").Rows.Count
            If NumRegistros = 0 Then
                MsgBox("Ocurrió un error al recuperar el máximo cod_ciudad...")
            Else
                Id_Usuario.Text = CStr(registro.Tables("maximo").Rows(0).Item("secuencial"))
            End If

            ' Actualizo la lista de empresas
            FPGetUsuarios()
            FPLimpiar()
        End If
    End Sub

    Private Sub FPLimpiar()
        ' Borrar el contenido de los campos en pantalla
        TxtCodigo.Text = ""
        TxtNombre.Text = ""
        TxtNombre.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ' Procedimiento para modificar el nombre y código del usuario para la toma
        VGStrSQL$ = "UPDATE usuario_toma SET nombre = '"
        ' Campo que contiene el nombre
        VGStrSQL$ = VGStrSQL$ & Trim(TxtNombre.Text) & "', "
        ' Campo que contiene el código
        VGStrSQL$ = VGStrSQL$ & "codigo = '" & Trim(TxtCodigo.Text) & "'"
        ' Cláusula WHERE
        VGStrSQL$ = VGStrSQL$ & " WHERE id_usuario = " & Id_Usuario.Text
        Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
        comando.ExecuteNonQuery()
        MsgBox("El registro ha sido actualizado", vbInformation, "STIAF")
        FPGetUsuarios()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla ciudad
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGStrSQL$ = "DELETE * FROM usuario_toma where id_usuario = " & Id_Usuario.Text
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido eliminado", vbInformation, "STIAF")
            FPLimpiar()
            FPGetUsuarios()
        End If
    End Sub
End Class