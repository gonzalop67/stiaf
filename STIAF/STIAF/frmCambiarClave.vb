﻿Public Class frmCambiarClave

    Private Sub frmCambiarClave_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "Cambiar Clave [" + VGUsuario$ + "]"
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim Password As String
        If Trim(TxtClaveActual.Text) = "" Then
            MessageBox.Show("Ingrese su contraseña actual ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtClaveActual.Focus()
        ElseIf Trim(TxtNueva.Text) = "" Then
            MessageBox.Show("Ingrese su nueva contraseña ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNueva.Focus()
        ElseIf Trim(TxtRedigitada.Text) = "" Then
            MessageBox.Show("Ingrese su nueva contraseña redigitada ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtRedigitada.Focus()
        ElseIf Trim(TxtClaveActual.Text) <> VGPasswd Then
            MessageBox.Show("Contraseña actual digitada no coincide ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TxtClaveActual.Focus()
        ElseIf Trim(TxtNueva.Text) <> Trim(TxtRedigitada.Text) Then
            MessageBox.Show("Contraseña nueva y redigitada no coinciden ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TxtNueva.Focus()
        Else
            ' Procedimiento para actualizar la contraseña del usuario
            Password$ = EncryptString(Trim(TxtNueva.Text), ENCRYPT)
            VGStrSQL$ = "UPDATE usuario SET us_passwd = '"
            ' Variable que contiene la nueva contraseña encriptada
            VGStrSQL$ = VGStrSQL$ & Password$ & "'"
            ' Cláusula WHERE
            VGStrSQL$ = VGStrSQL$ & " WHERE cod_usuario = " & Trim(VGCodUsuario)
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("Contraseña actualizada correctamente.", vbInformation, "STIAF")
            Me.Close()
        End If
    End Sub
End Class