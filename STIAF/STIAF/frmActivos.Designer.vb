﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActivos))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.btnReporte = New System.Windows.Forms.ToolStripButton()
        Me.lstResponse = New System.Windows.Forms.ListBox()
        Me.lstResultado = New System.Windows.Forms.ListBox()
        Me.chkModUsuario = New System.Windows.Forms.CheckBox()
        Me.chkIngUsuario = New System.Windows.Forms.CheckBox()
        Me.txtAdicional8 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtAdicional7 = New System.Windows.Forms.TextBox()
        Me.txtAdicional6 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtAdicional5 = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtAdicional4 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtAdicional3 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtAdicional2 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtAdicional1 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtDepreciacion = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtNroMotor = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtModelo = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.cboClaseActivo = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtAreaFisica = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtCargo = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCodUsuario = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtVidaUtil = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtPeriodoDepreciacion = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtValorLibros = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtValorAdquisicion = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtFechaCompra = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboSituacion = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDepartamento = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboSucursal = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboCiudad = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtFecUbicActivo = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtColor = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPlaca = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtChasis = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboTipoActivo = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCodigoBarras = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCodigoOriginal = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grdTotales8 = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.btnNotas = New System.Windows.Forms.ToolStripButton()
        Me.cboEmpresa = New System.Windows.Forms.ComboBox()
        Me.btnLimpiar = New System.Windows.Forms.ToolStripButton()
        Me.btnFirstRecord = New System.Windows.Forms.Button()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.btnNextRecord = New System.Windows.Forms.Button()
        Me.btnPrevRecord = New System.Windows.Forms.Button()
        Me.Focos = New System.Windows.Forms.ProgressBar()
        Me.cboTipoDeBaja = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnLastRecord = New System.Windows.Forms.Button()
        Me.btnBajar = New System.Windows.Forms.Button()
        Me.btnBackup = New System.Windows.Forms.ToolStripButton()
        Me.lblTotalRegistros = New System.Windows.Forms.Label()
        Me.btnLastActivo = New System.Windows.Forms.Button()
        Me.btnNextActivo = New System.Windows.Forms.Button()
        Me.btnPrevActivo = New System.Windows.Forms.Button()
        Me.btnFirstActivo = New System.Windows.Forms.Button()
        Me.grdTotales6 = New System.Windows.Forms.DataGridView()
        Me.grdTotales5 = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.grdActivos = New System.Windows.Forms.DataGridView()
        Me.grdTotales7 = New System.Windows.Forms.DataGridView()
        Me.btnBuscar = New System.Windows.Forms.ToolStripButton()
        Me.btnGuardar = New System.Windows.Forms.ToolStripButton()
        Me.btnCopiar = New System.Windows.Forms.ToolStripButton()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.grdTotales1 = New System.Windows.Forms.DataGridView()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.tlb = New System.Windows.Forms.ToolStrip()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.grdTotales2 = New System.Windows.Forms.DataGridView()
        Me.lblCodActivo = New System.Windows.Forms.Label()
        Me.grdTotales4 = New System.Windows.Forms.DataGridView()
        Me.grdTotales3 = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdTotales8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTotales6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTotales5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTotales7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTotales1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlb.SuspendLayout()
        CType(Me.grdTotales2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTotales4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdTotales3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtUsuario
        '
        Me.txtUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuario.Location = New System.Drawing.Point(376, 175)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(179, 20)
        Me.txtUsuario.TabIndex = 77
        Me.txtUsuario.Tag = "Usuario"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(865, 208)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(77, 13)
        Me.Label34.TabIndex = 73
        Me.Label34.Text = "33. Adicional 7"
        '
        'btnReporte
        '
        Me.btnReporte.Image = CType(resources.GetObject("btnReporte.Image"), System.Drawing.Image)
        Me.btnReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnReporte.Name = "btnReporte"
        Me.btnReporte.Size = New System.Drawing.Size(51, 35)
        Me.btnReporte.Text = "&Reporte"
        Me.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'lstResponse
        '
        Me.lstResponse.FormattingEnabled = True
        Me.lstResponse.Location = New System.Drawing.Point(9, 143)
        Me.lstResponse.Name = "lstResponse"
        Me.lstResponse.Size = New System.Drawing.Size(81, 17)
        Me.lstResponse.TabIndex = 78
        Me.lstResponse.Visible = False
        '
        'lstResultado
        '
        Me.lstResultado.FormattingEnabled = True
        Me.lstResultado.Location = New System.Drawing.Point(10, 120)
        Me.lstResultado.Name = "lstResultado"
        Me.lstResultado.Size = New System.Drawing.Size(81, 17)
        Me.lstResultado.TabIndex = 76
        Me.lstResultado.Visible = False
        '
        'chkModUsuario
        '
        Me.chkModUsuario.AutoSize = True
        Me.chkModUsuario.Enabled = False
        Me.chkModUsuario.Location = New System.Drawing.Point(1013, 257)
        Me.chkModUsuario.Name = "chkModUsuario"
        Me.chkModUsuario.Size = New System.Drawing.Size(144, 17)
        Me.chkModUsuario.TabIndex = 36
        Me.chkModUsuario.Text = "Modificado por el usuario"
        Me.chkModUsuario.UseVisualStyleBackColor = True
        '
        'chkIngUsuario
        '
        Me.chkIngUsuario.AutoSize = True
        Me.chkIngUsuario.Enabled = False
        Me.chkIngUsuario.Location = New System.Drawing.Point(868, 257)
        Me.chkIngUsuario.Name = "chkIngUsuario"
        Me.chkIngUsuario.Size = New System.Drawing.Size(139, 17)
        Me.chkIngUsuario.TabIndex = 35
        Me.chkIngUsuario.Text = "Ingresado por el usuario"
        Me.chkIngUsuario.UseVisualStyleBackColor = True
        '
        'txtAdicional8
        '
        Me.txtAdicional8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional8.Location = New System.Drawing.Point(984, 231)
        Me.txtAdicional8.Name = "txtAdicional8"
        Me.txtAdicional8.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional8.TabIndex = 34
        Me.txtAdicional8.Tag = "Adicional 8"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(865, 234)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(77, 13)
        Me.Label35.TabIndex = 75
        Me.Label35.Text = "34. Adicional 8"
        '
        'txtAdicional7
        '
        Me.txtAdicional7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional7.Location = New System.Drawing.Point(984, 205)
        Me.txtAdicional7.Name = "txtAdicional7"
        Me.txtAdicional7.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional7.TabIndex = 33
        Me.txtAdicional7.Tag = "Adicional 7"
        '
        'txtAdicional6
        '
        Me.txtAdicional6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional6.Location = New System.Drawing.Point(984, 179)
        Me.txtAdicional6.Name = "txtAdicional6"
        Me.txtAdicional6.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional6.TabIndex = 32
        Me.txtAdicional6.Tag = "Adicional 6"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lstResponse)
        Me.GroupBox1.Controls.Add(Me.txtUsuario)
        Me.GroupBox1.Controls.Add(Me.lstResultado)
        Me.GroupBox1.Controls.Add(Me.chkModUsuario)
        Me.GroupBox1.Controls.Add(Me.chkIngUsuario)
        Me.GroupBox1.Controls.Add(Me.txtAdicional8)
        Me.GroupBox1.Controls.Add(Me.Label35)
        Me.GroupBox1.Controls.Add(Me.txtAdicional7)
        Me.GroupBox1.Controls.Add(Me.Label34)
        Me.GroupBox1.Controls.Add(Me.txtAdicional6)
        Me.GroupBox1.Controls.Add(Me.Label33)
        Me.GroupBox1.Controls.Add(Me.txtAdicional5)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.txtAdicional4)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.txtAdicional3)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.txtAdicional2)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.txtAdicional1)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.txtDepreciacion)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.txtNroMotor)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.txtModelo)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.txtMarca)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.cboClaseActivo)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.txtAreaFisica)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.txtCargo)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtCodUsuario)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.txtVidaUtil)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtPeriodoDepreciacion)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.txtValorLibros)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtValorAdquisicion)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtFechaCompra)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cboSituacion)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.cboDepartamento)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.cboSucursal)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.cboCiudad)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtFecUbicActivo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtColor)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtPlaca)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtChasis)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cboTipoActivo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCodigoBarras)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtCodigoOriginal)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1238, 281)
        Me.GroupBox1.TabIndex = 60
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = " Datos del Activo: "
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(865, 182)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(77, 13)
        Me.Label33.TabIndex = 71
        Me.Label33.Text = "32. Adicional 6"
        '
        'txtAdicional5
        '
        Me.txtAdicional5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional5.Location = New System.Drawing.Point(984, 153)
        Me.txtAdicional5.Name = "txtAdicional5"
        Me.txtAdicional5.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional5.TabIndex = 31
        Me.txtAdicional5.Tag = "Adicional 5"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(865, 156)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(77, 13)
        Me.Label32.TabIndex = 69
        Me.Label32.Text = "31. Adicional 5"
        '
        'txtAdicional4
        '
        Me.txtAdicional4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional4.Location = New System.Drawing.Point(984, 124)
        Me.txtAdicional4.Name = "txtAdicional4"
        Me.txtAdicional4.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional4.TabIndex = 30
        Me.txtAdicional4.Tag = "Adicional 4"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(865, 127)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(77, 13)
        Me.Label31.TabIndex = 67
        Me.Label31.Text = "30. Adicional 4"
        '
        'txtAdicional3
        '
        Me.txtAdicional3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional3.Location = New System.Drawing.Point(984, 96)
        Me.txtAdicional3.Name = "txtAdicional3"
        Me.txtAdicional3.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional3.TabIndex = 29
        Me.txtAdicional3.Tag = "Adicional 3"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(865, 99)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(77, 13)
        Me.Label30.TabIndex = 65
        Me.Label30.Text = "29. Adicional 3"
        '
        'txtAdicional2
        '
        Me.txtAdicional2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional2.Location = New System.Drawing.Point(984, 70)
        Me.txtAdicional2.Name = "txtAdicional2"
        Me.txtAdicional2.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional2.TabIndex = 28
        Me.txtAdicional2.Tag = "Adicional 2"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(865, 73)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(77, 13)
        Me.Label29.TabIndex = 63
        Me.Label29.Text = "28. Adicional 2"
        '
        'txtAdicional1
        '
        Me.txtAdicional1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAdicional1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAdicional1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdicional1.Location = New System.Drawing.Point(984, 44)
        Me.txtAdicional1.Name = "txtAdicional1"
        Me.txtAdicional1.Size = New System.Drawing.Size(248, 20)
        Me.txtAdicional1.TabIndex = 27
        Me.txtAdicional1.Tag = "Adicional 1"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(865, 47)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(77, 13)
        Me.Label28.TabIndex = 61
        Me.Label28.Text = "27. Adicional 1"
        '
        'txtDepreciacion
        '
        Me.txtDepreciacion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtDepreciacion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtDepreciacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepreciacion.Location = New System.Drawing.Point(984, 17)
        Me.txtDepreciacion.Name = "txtDepreciacion"
        Me.txtDepreciacion.Size = New System.Drawing.Size(248, 20)
        Me.txtDepreciacion.TabIndex = 26
        Me.txtDepreciacion.Tag = "Deprec/Men/Anual"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(865, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(118, 13)
        Me.Label27.TabIndex = 58
        Me.Label27.Text = "26. Deprec/Men/Anual"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtDescripcion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Location = New System.Drawing.Point(97, 96)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(456, 73)
        Me.txtDescripcion.TabIndex = 4
        Me.txtDescripcion.Tag = "Descripcion 1"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(7, 99)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(75, 13)
        Me.Label26.TabIndex = 56
        Me.Label26.Text = "4. Descripcion"
        '
        'txtNroMotor
        '
        Me.txtNroMotor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtNroMotor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtNroMotor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroMotor.Location = New System.Drawing.Point(97, 254)
        Me.txtNroMotor.Name = "txtNroMotor"
        Me.txtNroMotor.Size = New System.Drawing.Size(179, 20)
        Me.txtNroMotor.TabIndex = 8
        Me.txtNroMotor.Tag = "Serie / # Motor"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(7, 257)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(88, 13)
        Me.Label25.TabIndex = 54
        Me.Label25.Text = "8. Serie / #Motor"
        '
        'txtModelo
        '
        Me.txtModelo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtModelo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtModelo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelo.Location = New System.Drawing.Point(97, 228)
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Size = New System.Drawing.Size(179, 20)
        Me.txtModelo.TabIndex = 7
        Me.txtModelo.Tag = "Modelo"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(7, 231)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(54, 13)
        Me.Label24.TabIndex = 52
        Me.Label24.Text = "7. Modelo"
        '
        'txtMarca
        '
        Me.txtMarca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtMarca.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.Location = New System.Drawing.Point(97, 202)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Size = New System.Drawing.Size(179, 20)
        Me.txtMarca.TabIndex = 6
        Me.txtMarca.Tag = "Marca"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(7, 205)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(49, 13)
        Me.Label23.TabIndex = 50
        Me.Label23.Text = "6. Marca"
        '
        'cboClaseActivo
        '
        Me.cboClaseActivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClaseActivo.FormattingEnabled = True
        Me.cboClaseActivo.Location = New System.Drawing.Point(97, 175)
        Me.cboClaseActivo.Name = "cboClaseActivo"
        Me.cboClaseActivo.Size = New System.Drawing.Size(179, 21)
        Me.cboClaseActivo.TabIndex = 5
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 178)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(93, 13)
        Me.Label22.TabIndex = 48
        Me.Label22.Text = "5. Clase de Activo"
        '
        'txtAreaFisica
        '
        Me.txtAreaFisica.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtAreaFisica.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtAreaFisica.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAreaFisica.Location = New System.Drawing.Point(376, 254)
        Me.txtAreaFisica.Name = "txtAreaFisica"
        Me.txtAreaFisica.Size = New System.Drawing.Size(179, 20)
        Me.txtAreaFisica.TabIndex = 15
        Me.txtAreaFisica.Tag = "Area Fisica"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(284, 257)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(77, 13)
        Me.Label21.TabIndex = 46
        Me.Label21.Text = "15. Area Fisica"
        '
        'txtCargo
        '
        Me.txtCargo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtCargo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtCargo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCargo.Location = New System.Drawing.Point(376, 228)
        Me.txtCargo.Name = "txtCargo"
        Me.txtCargo.Size = New System.Drawing.Size(179, 20)
        Me.txtCargo.TabIndex = 14
        Me.txtCargo.Tag = "Cargo"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(286, 231)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(53, 13)
        Me.Label20.TabIndex = 44
        Me.Label20.Text = "14. Cargo"
        '
        'txtCodUsuario
        '
        Me.txtCodUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtCodUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtCodUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodUsuario.Location = New System.Drawing.Point(376, 202)
        Me.txtCodUsuario.Name = "txtCodUsuario"
        Me.txtCodUsuario.Size = New System.Drawing.Size(179, 20)
        Me.txtCodUsuario.TabIndex = 13
        Me.txtCodUsuario.Tag = "Cod Usuario"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(286, 205)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(83, 13)
        Me.Label19.TabIndex = 42
        Me.Label19.Text = "13. Cod Usuario"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(286, 179)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(61, 13)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "12. Usuario"
        '
        'txtVidaUtil
        '
        Me.txtVidaUtil.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtVidaUtil.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtVidaUtil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVidaUtil.Location = New System.Drawing.Point(680, 254)
        Me.txtVidaUtil.Name = "txtVidaUtil"
        Me.txtVidaUtil.Size = New System.Drawing.Size(179, 20)
        Me.txtVidaUtil.TabIndex = 25
        Me.txtVidaUtil.Tag = "Vida Util"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(561, 257)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(64, 13)
        Me.Label17.TabIndex = 38
        Me.Label17.Text = "25. Vida Util"
        '
        'txtPeriodoDepreciacion
        '
        Me.txtPeriodoDepreciacion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtPeriodoDepreciacion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtPeriodoDepreciacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPeriodoDepreciacion.Location = New System.Drawing.Point(680, 228)
        Me.txtPeriodoDepreciacion.Name = "txtPeriodoDepreciacion"
        Me.txtPeriodoDepreciacion.Size = New System.Drawing.Size(179, 20)
        Me.txtPeriodoDepreciacion.TabIndex = 24
        Me.txtPeriodoDepreciacion.Tag = "Periodo Depreciacion"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(561, 231)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(110, 13)
        Me.Label16.TabIndex = 36
        Me.Label16.Text = "24. Per. Depreciacion"
        '
        'txtValorLibros
        '
        Me.txtValorLibros.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtValorLibros.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtValorLibros.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtValorLibros.Location = New System.Drawing.Point(680, 202)
        Me.txtValorLibros.Name = "txtValorLibros"
        Me.txtValorLibros.Size = New System.Drawing.Size(179, 20)
        Me.txtValorLibros.TabIndex = 23
        Me.txtValorLibros.Tag = "Valor Libros"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(561, 205)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 13)
        Me.Label15.TabIndex = 34
        Me.Label15.Text = "23. Valor Libros"
        '
        'txtValorAdquisicion
        '
        Me.txtValorAdquisicion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtValorAdquisicion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtValorAdquisicion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtValorAdquisicion.Location = New System.Drawing.Point(680, 176)
        Me.txtValorAdquisicion.Name = "txtValorAdquisicion"
        Me.txtValorAdquisicion.Size = New System.Drawing.Size(179, 20)
        Me.txtValorAdquisicion.TabIndex = 22
        Me.txtValorAdquisicion.Tag = "Valor Adquisicion"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(561, 179)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(106, 13)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "22. Valor Adquisicion"
        '
        'txtFechaCompra
        '
        Me.txtFechaCompra.CustomFormat = "dd/MM/yyyy"
        Me.txtFechaCompra.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFechaCompra.Location = New System.Drawing.Point(680, 150)
        Me.txtFechaCompra.Name = "txtFechaCompra"
        Me.txtFechaCompra.Size = New System.Drawing.Size(179, 20)
        Me.txtFechaCompra.TabIndex = 21
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(561, 153)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(94, 13)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "21. Fecha Compra"
        '
        'cboSituacion
        '
        Me.cboSituacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSituacion.FormattingEnabled = True
        Me.cboSituacion.Location = New System.Drawing.Point(680, 123)
        Me.cboSituacion.Name = "cboSituacion"
        Me.cboSituacion.Size = New System.Drawing.Size(179, 21)
        Me.cboSituacion.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(561, 126)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(102, 13)
        Me.Label12.TabIndex = 28
        Me.Label12.Text = "20. Situacion Activo"
        '
        'cboDepartamento
        '
        Me.cboDepartamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartamento.FormattingEnabled = True
        Me.cboDepartamento.Location = New System.Drawing.Point(680, 96)
        Me.cboDepartamento.Name = "cboDepartamento"
        Me.cboDepartamento.Size = New System.Drawing.Size(179, 21)
        Me.cboDepartamento.TabIndex = 19
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(561, 99)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(92, 13)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "19. Departamento"
        '
        'cboSucursal
        '
        Me.cboSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSucursal.FormattingEnabled = True
        Me.cboSucursal.Location = New System.Drawing.Point(680, 69)
        Me.cboSucursal.Name = "cboSucursal"
        Me.cboSucursal.Size = New System.Drawing.Size(179, 21)
        Me.cboSucursal.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(561, 72)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 13)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "18. Sucursal"
        '
        'cboCiudad
        '
        Me.cboCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCiudad.FormattingEnabled = True
        Me.cboCiudad.Location = New System.Drawing.Point(680, 43)
        Me.cboCiudad.Name = "cboCiudad"
        Me.cboCiudad.Size = New System.Drawing.Size(179, 21)
        Me.cboCiudad.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(561, 46)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "17. Ciudad"
        '
        'txtFecUbicActivo
        '
        Me.txtFecUbicActivo.CustomFormat = "dd/MM/yyyy"
        Me.txtFecUbicActivo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtFecUbicActivo.Location = New System.Drawing.Point(680, 17)
        Me.txtFecUbicActivo.Name = "txtFecUbicActivo"
        Me.txtFecUbicActivo.Size = New System.Drawing.Size(179, 20)
        Me.txtFecUbicActivo.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(561, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(113, 13)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "16. Fecha Ubic Activo"
        '
        'txtColor
        '
        Me.txtColor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtColor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtColor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtColor.Location = New System.Drawing.Point(374, 69)
        Me.txtColor.Name = "txtColor"
        Me.txtColor.Size = New System.Drawing.Size(179, 20)
        Me.txtColor.TabIndex = 11
        Me.txtColor.Tag = "Color"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(284, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "11. Color"
        '
        'txtPlaca
        '
        Me.txtPlaca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtPlaca.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtPlaca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPlaca.Location = New System.Drawing.Point(374, 43)
        Me.txtPlaca.Name = "txtPlaca"
        Me.txtPlaca.Size = New System.Drawing.Size(179, 20)
        Me.txtPlaca.TabIndex = 10
        Me.txtPlaca.Tag = "# Placa"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(284, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "10. # Placa"
        '
        'txtChasis
        '
        Me.txtChasis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtChasis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtChasis.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtChasis.Location = New System.Drawing.Point(374, 17)
        Me.txtChasis.Name = "txtChasis"
        Me.txtChasis.Size = New System.Drawing.Size(179, 20)
        Me.txtChasis.TabIndex = 9
        Me.txtChasis.Tag = "# Chasis"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(284, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "9. # Chasis"
        '
        'cboTipoActivo
        '
        Me.cboTipoActivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoActivo.FormattingEnabled = True
        Me.cboTipoActivo.Location = New System.Drawing.Point(97, 69)
        Me.cboTipoActivo.Name = "cboTipoActivo"
        Me.cboTipoActivo.Size = New System.Drawing.Size(179, 21)
        Me.cboTipoActivo.TabIndex = 3
        Me.cboTipoActivo.Tag = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "3. Tipo de Activo"
        '
        'txtCodigoBarras
        '
        Me.txtCodigoBarras.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtCodigoBarras.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtCodigoBarras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoBarras.Location = New System.Drawing.Point(97, 43)
        Me.txtCodigoBarras.Name = "txtCodigoBarras"
        Me.txtCodigoBarras.Size = New System.Drawing.Size(179, 20)
        Me.txtCodigoBarras.TabIndex = 2
        Me.txtCodigoBarras.Tag = "Codigo Barras"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "2. Código Barras "
        '
        'txtCodigoOriginal
        '
        Me.txtCodigoOriginal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtCodigoOriginal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtCodigoOriginal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoOriginal.Location = New System.Drawing.Point(97, 17)
        Me.txtCodigoOriginal.Name = "txtCodigoOriginal"
        Me.txtCodigoOriginal.Size = New System.Drawing.Size(179, 20)
        Me.txtCodigoOriginal.TabIndex = 1
        Me.txtCodigoOriginal.Tag = "Codigo Original"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "1. Código Original "
        '
        'grdTotales8
        '
        Me.grdTotales8.AllowUserToAddRows = False
        Me.grdTotales8.AllowUserToDeleteRows = False
        Me.grdTotales8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdTotales8.DefaultCellStyle = DataGridViewCellStyle5
        Me.grdTotales8.Location = New System.Drawing.Point(1087, 562)
        Me.grdTotales8.Name = "grdTotales8"
        Me.grdTotales8.ReadOnly = True
        Me.grdTotales8.RowHeadersVisible = False
        Me.grdTotales8.Size = New System.Drawing.Size(153, 53)
        Me.grdTotales8.TabIndex = 70
        '
        'btnSalir
        '
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(51, 35)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnNotas
        '
        Me.btnNotas.Image = CType(resources.GetObject("btnNotas.Image"), System.Drawing.Image)
        Me.btnNotas.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNotas.Name = "btnNotas"
        Me.btnNotas.Size = New System.Drawing.Size(51, 35)
        Me.btnNotas.Text = "N&otas"
        Me.btnNotas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'cboEmpresa
        '
        Me.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresa.FormattingEnabled = True
        Me.cboEmpresa.Location = New System.Drawing.Point(51, 4)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(229, 21)
        Me.cboEmpresa.TabIndex = 51
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Image = CType(resources.GetObject("btnLimpiar.Image"), System.Drawing.Image)
        Me.btnLimpiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(51, 35)
        Me.btnLimpiar.Text = "&Limpiar"
        Me.btnLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnFirstRecord
        '
        Me.btnFirstRecord.Location = New System.Drawing.Point(286, 3)
        Me.btnFirstRecord.Name = "btnFirstRecord"
        Me.btnFirstRecord.Size = New System.Drawing.Size(28, 22)
        Me.btnFirstRecord.TabIndex = 52
        Me.btnFirstRecord.Text = "<<"
        Me.btnFirstRecord.UseVisualStyleBackColor = True
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Location = New System.Drawing.Point(1, 7)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(51, 13)
        Me.lblEmpresa.TabIndex = 50
        Me.lblEmpresa.Text = "Empresa:"
        '
        'btnNextRecord
        '
        Me.btnNextRecord.Location = New System.Drawing.Point(337, 3)
        Me.btnNextRecord.Name = "btnNextRecord"
        Me.btnNextRecord.Size = New System.Drawing.Size(25, 22)
        Me.btnNextRecord.TabIndex = 54
        Me.btnNextRecord.Text = ">"
        Me.btnNextRecord.UseVisualStyleBackColor = True
        '
        'btnPrevRecord
        '
        Me.btnPrevRecord.Location = New System.Drawing.Point(313, 3)
        Me.btnPrevRecord.Name = "btnPrevRecord"
        Me.btnPrevRecord.Size = New System.Drawing.Size(25, 22)
        Me.btnPrevRecord.TabIndex = 53
        Me.btnPrevRecord.Text = "<"
        Me.btnPrevRecord.UseVisualStyleBackColor = True
        '
        'Focos
        '
        Me.Focos.Location = New System.Drawing.Point(691, 3)
        Me.Focos.Name = "Focos"
        Me.Focos.Size = New System.Drawing.Size(294, 23)
        Me.Focos.TabIndex = 59
        '
        'cboTipoDeBaja
        '
        Me.cboTipoDeBaja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDeBaja.FormattingEnabled = True
        Me.cboTipoDeBaja.Location = New System.Drawing.Point(467, 5)
        Me.cboTipoDeBaja.Name = "cboTipoDeBaja"
        Me.cboTipoDeBaja.Size = New System.Drawing.Size(161, 21)
        Me.cboTipoDeBaja.TabIndex = 57
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(395, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Bajar a Excel:"
        '
        'btnLastRecord
        '
        Me.btnLastRecord.Location = New System.Drawing.Point(361, 3)
        Me.btnLastRecord.Name = "btnLastRecord"
        Me.btnLastRecord.Size = New System.Drawing.Size(28, 22)
        Me.btnLastRecord.TabIndex = 55
        Me.btnLastRecord.Text = ">>"
        Me.btnLastRecord.UseVisualStyleBackColor = True
        '
        'btnBajar
        '
        Me.btnBajar.Location = New System.Drawing.Point(630, 4)
        Me.btnBajar.Name = "btnBajar"
        Me.btnBajar.Size = New System.Drawing.Size(60, 22)
        Me.btnBajar.TabIndex = 58
        Me.btnBajar.Text = "Bajar"
        Me.btnBajar.UseVisualStyleBackColor = True
        '
        'btnBackup
        '
        Me.btnBackup.Image = CType(resources.GetObject("btnBackup.Image"), System.Drawing.Image)
        Me.btnBackup.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.Size = New System.Drawing.Size(51, 35)
        Me.btnBackup.Text = "B&ackup"
        Me.btnBackup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'lblTotalRegistros
        '
        Me.lblTotalRegistros.AutoSize = True
        Me.lblTotalRegistros.Location = New System.Drawing.Point(6, 215)
        Me.lblTotalRegistros.Name = "lblTotalRegistros"
        Me.lblTotalRegistros.Size = New System.Drawing.Size(162, 13)
        Me.lblTotalRegistros.TabIndex = 17
        Me.lblTotalRegistros.Text = "Total de Registros Encontrados: "
        '
        'btnLastActivo
        '
        Me.btnLastActivo.Location = New System.Drawing.Point(1204, 210)
        Me.btnLastActivo.Name = "btnLastActivo"
        Me.btnLastActivo.Size = New System.Drawing.Size(28, 22)
        Me.btnLastActivo.TabIndex = 16
        Me.btnLastActivo.Text = ">>"
        Me.btnLastActivo.UseVisualStyleBackColor = True
        Me.btnLastActivo.Visible = False
        '
        'btnNextActivo
        '
        Me.btnNextActivo.Location = New System.Drawing.Point(1180, 210)
        Me.btnNextActivo.Name = "btnNextActivo"
        Me.btnNextActivo.Size = New System.Drawing.Size(25, 22)
        Me.btnNextActivo.TabIndex = 15
        Me.btnNextActivo.Text = ">"
        Me.btnNextActivo.UseVisualStyleBackColor = True
        Me.btnNextActivo.Visible = False
        '
        'btnPrevActivo
        '
        Me.btnPrevActivo.Location = New System.Drawing.Point(1156, 210)
        Me.btnPrevActivo.Name = "btnPrevActivo"
        Me.btnPrevActivo.Size = New System.Drawing.Size(25, 22)
        Me.btnPrevActivo.TabIndex = 14
        Me.btnPrevActivo.Text = "<"
        Me.btnPrevActivo.UseVisualStyleBackColor = True
        Me.btnPrevActivo.Visible = False
        '
        'btnFirstActivo
        '
        Me.btnFirstActivo.Location = New System.Drawing.Point(1129, 210)
        Me.btnFirstActivo.Name = "btnFirstActivo"
        Me.btnFirstActivo.Size = New System.Drawing.Size(28, 22)
        Me.btnFirstActivo.TabIndex = 13
        Me.btnFirstActivo.Text = "<<"
        Me.btnFirstActivo.UseVisualStyleBackColor = True
        Me.btnFirstActivo.Visible = False
        '
        'grdTotales6
        '
        Me.grdTotales6.AllowUserToAddRows = False
        Me.grdTotales6.AllowUserToDeleteRows = False
        Me.grdTotales6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdTotales6.DefaultCellStyle = DataGridViewCellStyle6
        Me.grdTotales6.Location = New System.Drawing.Point(779, 562)
        Me.grdTotales6.Name = "grdTotales6"
        Me.grdTotales6.ReadOnly = True
        Me.grdTotales6.RowHeadersVisible = False
        Me.grdTotales6.Size = New System.Drawing.Size(153, 53)
        Me.grdTotales6.TabIndex = 68
        '
        'grdTotales5
        '
        Me.grdTotales5.AllowUserToAddRows = False
        Me.grdTotales5.AllowUserToDeleteRows = False
        Me.grdTotales5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTotales5.Location = New System.Drawing.Point(623, 562)
        Me.grdTotales5.Name = "grdTotales5"
        Me.grdTotales5.ReadOnly = True
        Me.grdTotales5.RowHeadersVisible = False
        Me.grdTotales5.Size = New System.Drawing.Size(155, 53)
        Me.grdTotales5.TabIndex = 67
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblTotalRegistros)
        Me.GroupBox2.Controls.Add(Me.btnLastActivo)
        Me.GroupBox2.Controls.Add(Me.btnNextActivo)
        Me.GroupBox2.Controls.Add(Me.btnPrevActivo)
        Me.GroupBox2.Controls.Add(Me.btnFirstActivo)
        Me.GroupBox2.Controls.Add(Me.grdActivos)
        Me.GroupBox2.Location = New System.Drawing.Point(4, 318)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1238, 238)
        Me.GroupBox2.TabIndex = 61
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = " Activos Registrados: "
        '
        'grdActivos
        '
        Me.grdActivos.AllowUserToAddRows = False
        Me.grdActivos.AllowUserToDeleteRows = False
        Me.grdActivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdActivos.Location = New System.Drawing.Point(3, 16)
        Me.grdActivos.Name = "grdActivos"
        Me.grdActivos.ReadOnly = True
        Me.grdActivos.RowHeadersWidth = 30
        Me.grdActivos.Size = New System.Drawing.Size(1229, 191)
        Me.grdActivos.TabIndex = 0
        Me.grdActivos.VirtualMode = True
        '
        'grdTotales7
        '
        Me.grdTotales7.AllowUserToAddRows = False
        Me.grdTotales7.AllowUserToDeleteRows = False
        Me.grdTotales7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTotales7.Location = New System.Drawing.Point(933, 562)
        Me.grdTotales7.Name = "grdTotales7"
        Me.grdTotales7.ReadOnly = True
        Me.grdTotales7.RowHeadersVisible = False
        Me.grdTotales7.Size = New System.Drawing.Size(153, 53)
        Me.grdTotales7.TabIndex = 69
        '
        'btnBuscar
        '
        Me.btnBuscar.Image = CType(resources.GetObject("btnBuscar.Image"), System.Drawing.Image)
        Me.btnBuscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(51, 35)
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnGuardar
        '
        Me.btnGuardar.Image = CType(resources.GetObject("btnGuardar.Image"), System.Drawing.Image)
        Me.btnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(51, 35)
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnCopiar
        '
        Me.btnCopiar.Image = CType(resources.GetObject("btnCopiar.Image"), System.Drawing.Image)
        Me.btnCopiar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCopiar.Name = "btnCopiar"
        Me.btnCopiar.Size = New System.Drawing.Size(51, 35)
        Me.btnCopiar.Text = "&Copiar"
        Me.btnCopiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnNuevo
        '
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(51, 35)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'grdTotales1
        '
        Me.grdTotales1.AllowUserToAddRows = False
        Me.grdTotales1.AllowUserToDeleteRows = False
        Me.grdTotales1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTotales1.Location = New System.Drawing.Point(4, 562)
        Me.grdTotales1.Name = "grdTotales1"
        Me.grdTotales1.ReadOnly = True
        Me.grdTotales1.RowHeadersVisible = False
        Me.grdTotales1.Size = New System.Drawing.Size(153, 53)
        Me.grdTotales1.TabIndex = 63
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.BackColor = System.Drawing.SystemColors.Control
        Me.lblPorcentaje.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPorcentaje.Location = New System.Drawing.Point(987, 8)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(70, 13)
        Me.lblPorcentaje.TabIndex = 72
        Me.lblPorcentaje.Text = "0 %"
        Me.lblPorcentaje.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tlb
        '
        Me.tlb.Dock = System.Windows.Forms.DockStyle.Right
        Me.tlb.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlb.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnCopiar, Me.btnGuardar, Me.btnBuscar, Me.btnBackup, Me.btnLimpiar, Me.btnReporte, Me.btnNotas, Me.btnSalir})
        Me.tlb.Location = New System.Drawing.Point(1251, 0)
        Me.tlb.Name = "tlb"
        Me.tlb.Size = New System.Drawing.Size(54, 618)
        Me.tlb.TabIndex = 71
        '
        'grdTotales2
        '
        Me.grdTotales2.AllowUserToAddRows = False
        Me.grdTotales2.AllowUserToDeleteRows = False
        Me.grdTotales2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdTotales2.DefaultCellStyle = DataGridViewCellStyle7
        Me.grdTotales2.Location = New System.Drawing.Point(158, 562)
        Me.grdTotales2.Name = "grdTotales2"
        Me.grdTotales2.ReadOnly = True
        Me.grdTotales2.RowHeadersVisible = False
        Me.grdTotales2.Size = New System.Drawing.Size(153, 53)
        Me.grdTotales2.TabIndex = 64
        '
        'lblCodActivo
        '
        Me.lblCodActivo.AutoSize = True
        Me.lblCodActivo.Location = New System.Drawing.Point(1227, 15)
        Me.lblCodActivo.Name = "lblCodActivo"
        Me.lblCodActivo.Size = New System.Drawing.Size(13, 13)
        Me.lblCodActivo.TabIndex = 62
        Me.lblCodActivo.Text = "0"
        Me.lblCodActivo.Visible = False
        '
        'grdTotales4
        '
        Me.grdTotales4.AllowUserToAddRows = False
        Me.grdTotales4.AllowUserToDeleteRows = False
        Me.grdTotales4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdTotales4.DefaultCellStyle = DataGridViewCellStyle8
        Me.grdTotales4.Location = New System.Drawing.Point(466, 562)
        Me.grdTotales4.Name = "grdTotales4"
        Me.grdTotales4.ReadOnly = True
        Me.grdTotales4.RowHeadersVisible = False
        Me.grdTotales4.Size = New System.Drawing.Size(155, 53)
        Me.grdTotales4.TabIndex = 66
        '
        'grdTotales3
        '
        Me.grdTotales3.AllowUserToAddRows = False
        Me.grdTotales3.AllowUserToDeleteRows = False
        Me.grdTotales3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTotales3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTotales3.Location = New System.Drawing.Point(312, 562)
        Me.grdTotales3.Name = "grdTotales3"
        Me.grdTotales3.ReadOnly = True
        Me.grdTotales3.RowHeadersVisible = False
        Me.grdTotales3.Size = New System.Drawing.Size(153, 53)
        Me.grdTotales3.TabIndex = 65
        '
        'frmActivos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 618)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grdTotales8)
        Me.Controls.Add(Me.cboEmpresa)
        Me.Controls.Add(Me.btnFirstRecord)
        Me.Controls.Add(Me.lblEmpresa)
        Me.Controls.Add(Me.btnNextRecord)
        Me.Controls.Add(Me.btnPrevRecord)
        Me.Controls.Add(Me.Focos)
        Me.Controls.Add(Me.cboTipoDeBaja)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnLastRecord)
        Me.Controls.Add(Me.btnBajar)
        Me.Controls.Add(Me.grdTotales6)
        Me.Controls.Add(Me.grdTotales5)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.grdTotales7)
        Me.Controls.Add(Me.grdTotales1)
        Me.Controls.Add(Me.lblPorcentaje)
        Me.Controls.Add(Me.tlb)
        Me.Controls.Add(Me.grdTotales2)
        Me.Controls.Add(Me.lblCodActivo)
        Me.Controls.Add(Me.grdTotales4)
        Me.Controls.Add(Me.grdTotales3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmActivos"
        Me.Text = "Registro de Activos Fijos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdTotales8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTotales6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTotales5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.grdActivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTotales7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTotales1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlb.ResumeLayout(False)
        Me.tlb.PerformLayout()
        CType(Me.grdTotales2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTotales4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdTotales3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents btnReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents lstResponse As System.Windows.Forms.ListBox
    Friend WithEvents lstResultado As System.Windows.Forms.ListBox
    Friend WithEvents chkModUsuario As System.Windows.Forms.CheckBox
    Friend WithEvents chkIngUsuario As System.Windows.Forms.CheckBox
    Friend WithEvents txtAdicional8 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtAdicional7 As System.Windows.Forms.TextBox
    Friend WithEvents txtAdicional6 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtAdicional5 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtAdicional4 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtAdicional3 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtAdicional2 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtAdicional1 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtDepreciacion As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtNroMotor As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtModelo As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cboClaseActivo As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtAreaFisica As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtCargo As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCodUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtVidaUtil As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtPeriodoDepreciacion As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtValorLibros As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtValorAdquisicion As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtFechaCompra As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboSituacion As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDepartamento As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtFecUbicActivo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtColor As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPlaca As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtChasis As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboTipoActivo As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoBarras As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoOriginal As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grdTotales8 As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnNotas As System.Windows.Forms.ToolStripButton
    Friend WithEvents cboEmpresa As System.Windows.Forms.ComboBox
    Friend WithEvents btnLimpiar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnFirstRecord As System.Windows.Forms.Button
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents btnNextRecord As System.Windows.Forms.Button
    Friend WithEvents btnPrevRecord As System.Windows.Forms.Button
    Friend WithEvents Focos As System.Windows.Forms.ProgressBar
    Friend WithEvents cboTipoDeBaja As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnLastRecord As System.Windows.Forms.Button
    Friend WithEvents btnBajar As System.Windows.Forms.Button
    Friend WithEvents btnBackup As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblTotalRegistros As System.Windows.Forms.Label
    Friend WithEvents btnLastActivo As System.Windows.Forms.Button
    Friend WithEvents btnNextActivo As System.Windows.Forms.Button
    Friend WithEvents btnPrevActivo As System.Windows.Forms.Button
    Friend WithEvents btnFirstActivo As System.Windows.Forms.Button
    Friend WithEvents grdTotales6 As System.Windows.Forms.DataGridView
    Friend WithEvents grdTotales5 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents grdActivos As System.Windows.Forms.DataGridView
    Friend WithEvents grdTotales7 As System.Windows.Forms.DataGridView
    Friend WithEvents btnBuscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCopiar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents grdTotales1 As System.Windows.Forms.DataGridView
    Friend WithEvents lblPorcentaje As System.Windows.Forms.Label
    Friend WithEvents tlb As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents grdTotales2 As System.Windows.Forms.DataGridView
    Friend WithEvents lblCodActivo As System.Windows.Forms.Label
    Friend WithEvents grdTotales4 As System.Windows.Forms.DataGridView
    Friend WithEvents grdTotales3 As System.Windows.Forms.DataGridView
End Class
