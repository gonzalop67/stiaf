﻿Public Class frmCiudad

    Private Sub frmCiudad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPGetCiudades()
        ' Estado de los botones de comando
        ' En todos los casos de niveles de seguridad
        btnModificar.Enabled = False     ' Modificar
        btnEliminar.Enabled = False     ' Eliminar
        If CInt(VGNivelSeg) <= 4 Then  ' Invitados y Usuarios de otros módulos
            btnIngresar.Enabled = False  ' Ingresar
        End If
    End Sub

    Private Sub FPGetCiudades()
        ' Obtengo las ciudades ingresados en la base de datos
        Dim consulta As String = "SELECT * FROM ciudad ORDER BY ci_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "ciudad")
        Dim NumRegistros As Byte = registro.Tables("ciudad").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("No existen ciudades...")
        Else
            ReDim VGCodCiudad(NumRegistros)
            lstCiudades.Items.Clear()
            For i = 1 To NumRegistros
                VGCodCiudad(i - 1) = registro.Tables("ciudad").Rows(i - 1).Item("cod_ciudad")
                lstCiudades.Items.Add(registro.Tables("ciudad").Rows(i - 1).Item("ci_nombre"))
            Next i
        End If
    End Sub

    Private Sub lstCiudades_DoubleClick(sender As Object, e As EventArgs) Handles lstCiudades.DoubleClick
        ' Primero obtengo el cod_ciudad
        TxtCodigo.Text = VGCodCiudad(lstCiudades.SelectedIndex)
        FillFields()
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM ciudad WHERE cod_ciudad = " & Trim(TxtCodigo.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "ciudad")
        Dim NumRegistros As Byte = registro.Tables("ciudad").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar la ciudad...")
        Else
            TxtNombre.Text = registro.Tables("ciudad").Rows(0).Item("ci_nombre")
        End If
        TxtNombre.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = False
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If Trim(TxtNombre.Text) = "" Then
            MsgBox("Debe digitar el dato para el nombre de la Ciudad.", vbExclamation, "Error en el ingreso de datos")
            TxtNombre.Focus()
        ElseIf FPBuscarCampo("ciudad", "ci_nombre", Trim(TxtNombre.Text)) Then
            MsgBox("El nombre de la Ciudad ya se encuentra ingresado " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtNombre.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla ciudad
            VGStrSQL$ = "INSERT INTO ciudad (ci_nombre) VALUES ("
            ' Nombre de la Ciudad
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtNombre.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido insertado", vbInformation, "STIAF")
            ' Estado de los botones de comando
            If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
                btnIngresar.Enabled = False
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            VGStrSQL$ = "SELECT max(cod_ciudad) AS secuencial FROM ciudad"
            Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "maximo")
            Dim NumRegistros As Byte = registro.Tables("maximo").Rows.Count
            If NumRegistros = 0 Then
                MsgBox("Ocurrió un error al recuperar el máximo cod_ciudad...")
            Else
                TxtCodigo.Text = CStr(registro.Tables("maximo").Rows(0).Item("secuencial"))
            End If

            ' Actualizo la lista de empresas
            FPGetCiudades()
            FPLimpiar()
        End If
    End Sub

    Private Sub FPLimpiar()
        ' Borrar el contenido de los campos en pantalla
        TxtCodigo.Text = ""
        TxtNombre.Text = ""
        TxtNombre.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ' Procedimiento para modificar el nombre de la ciudad
        VGStrSQL$ = "UPDATE ciudad SET ci_nombre = '"
        ' Campo que contiene el nombre
        VGStrSQL$ = VGStrSQL$ & Trim(TxtNombre.Text) & "'"
        ' Cláusula WHERE
        VGStrSQL$ = VGStrSQL$ & " where cod_ciudad = " & TxtCodigo.Text
        Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
        comando.ExecuteNonQuery()
        MsgBox("El registro ha sido actualizado", vbInformation, "STIAF")
        FPGetCiudades()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla ciudad
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGStrSQL$ = "delete * from ciudad where cod_ciudad = " & TxtCodigo.Text
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido eliminado", vbInformation, "STIAF")
            FPLimpiar()
            FPGetCiudades()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class