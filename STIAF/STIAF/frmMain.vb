﻿Public Class frmMain

    Private Sub EmpresaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EmpresaToolStripMenuItem.Click
        Dim NewForm As New frmEmpresa

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub TipoDeActivoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TipoDeActivoToolStripMenuItem.Click
        Dim NewForm As New frmTipoActivo

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub ClaseDeActivoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClaseDeActivoToolStripMenuItem.Click
        Dim NewForm As New frmClaseActivo()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub SituaciónDeActivoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SituaciónDeActivoToolStripMenuItem.Click
        Dim NewForm As New frmSitActivo()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub CiudadesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CiudadesToolStripMenuItem.Click
        Dim NewForm As New frmCiudad()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub SucursalesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SucursalesToolStripMenuItem.Click
        Dim NewForm As New frmSucursal()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub DepartamentosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DepartamentosToolStripMenuItem.Click
        Dim NewForm As New frmDepartamento()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub UsuariosDeLaTomaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosDeLaTomaToolStripMenuItem.Click
        Dim NewForm As New frmUsuarioToma()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub CambiarClaveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiarClaveToolStripMenuItem.Click
        frmCambiarClave.ShowDialog()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim NewForm As New frmUsuarios()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub AbrirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirToolStripMenuItem.Click
        Dim NewForm As New frmActivos()

        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        NewForm.MdiParent = Me

        NewForm.Show()
    End Sub

    Private Sub frmMain_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        End  'Invoca al cierre inmediato de la aplicación
    End Sub

    Private Sub SubirDeExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SubirDeExcelToolStripMenuItem.Click
        Dim OpenFileDialog As New OpenFileDialog
        Try
            'OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
            OpenFileDialog.Filter = "Excel 2007 (*.xlsx)|*.xlsx|Excel 97-2003 (*.xls)|*.xls|Todos los archivos (*.*)|*.*"
            If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                sNameFile = OpenFileDialog.FileName
                ' Aquí se realiza el proceso de pasar los datos desde Excel
                ' a la base de datos en Access
                optSheetRange.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error de ejecución..." & ex.Message, MsgBoxStyle.Critical, "STIAF")
        End Try
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
        cn.Close()
        Application.Exit()
    End Sub
End Class