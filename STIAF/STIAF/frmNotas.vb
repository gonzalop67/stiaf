﻿Public Class frmNotas

    Dim VLCodEmpresaSel As String = basMain.VGCodEmpresaSel

    Private Sub frmNotas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Aquí recupero las notas de la empresa si existen
        Dim consulta As String = "SELECT em_notas FROM empresa WHERE cod_empresa = " & Trim(VLCodEmpresaSel)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "notas")
        txtNotas.Text = registro.Tables("notas").Rows(0).Item("em_notas")
    End Sub

    Private Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        ' Actualizar el campo em_notas de la tabla empresa
        VGStrSQL$ = "UPDATE empresa SET em_notas = '" & Trim(txtNotas.Text) & "' WHERE cod_empresa = " & Trim(VLCodEmpresaSel)
        Dim comando As New OleDb.OleDbCommand(VGStrSQL$, cn)
        comando.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...

        MsgBox("Notas actualizadas exitosamente.", vbInformation, "Notas")
    End Sub
End Class