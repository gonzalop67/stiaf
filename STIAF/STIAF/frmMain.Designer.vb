﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.EspecificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmpresaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TipoDeActivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaseDeActivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SituaciónDeActivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CiudadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SucursalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepartamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.UsuariosDeLaTomaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.CambiarClaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.SubirDeExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BajarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentanasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EspecificacionesToolStripMenuItem, Me.ActivosToolStripMenuItem, Me.VentanasToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(923, 24)
        Me.MenuStrip.TabIndex = 1
        Me.MenuStrip.Text = "MenuStrip"
        '
        'EspecificacionesToolStripMenuItem
        '
        Me.EspecificacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmpresaToolStripMenuItem, Me.ToolStripSeparator1, Me.TipoDeActivoToolStripMenuItem, Me.ClaseDeActivoToolStripMenuItem, Me.SituaciónDeActivoToolStripMenuItem, Me.ToolStripSeparator2, Me.CiudadesToolStripMenuItem, Me.SucursalesToolStripMenuItem, Me.DepartamentosToolStripMenuItem, Me.ToolStripSeparator3, Me.UsuariosDeLaTomaToolStripMenuItem, Me.ToolStripSeparator4, Me.CambiarClaveToolStripMenuItem, Me.UsuariosToolStripMenuItem})
        Me.EspecificacionesToolStripMenuItem.Name = "EspecificacionesToolStripMenuItem"
        Me.EspecificacionesToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.EspecificacionesToolStripMenuItem.Text = "&Especificaciones"
        '
        'EmpresaToolStripMenuItem
        '
        Me.EmpresaToolStripMenuItem.Name = "EmpresaToolStripMenuItem"
        Me.EmpresaToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.EmpresaToolStripMenuItem.Text = "&Empresa"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(177, 6)
        '
        'TipoDeActivoToolStripMenuItem
        '
        Me.TipoDeActivoToolStripMenuItem.Name = "TipoDeActivoToolStripMenuItem"
        Me.TipoDeActivoToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.TipoDeActivoToolStripMenuItem.Text = "&Tipo de Activo"
        '
        'ClaseDeActivoToolStripMenuItem
        '
        Me.ClaseDeActivoToolStripMenuItem.Name = "ClaseDeActivoToolStripMenuItem"
        Me.ClaseDeActivoToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ClaseDeActivoToolStripMenuItem.Text = "&Clase de Activo"
        '
        'SituaciónDeActivoToolStripMenuItem
        '
        Me.SituaciónDeActivoToolStripMenuItem.Name = "SituaciónDeActivoToolStripMenuItem"
        Me.SituaciónDeActivoToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.SituaciónDeActivoToolStripMenuItem.Text = "&Situación de Activo"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(177, 6)
        '
        'CiudadesToolStripMenuItem
        '
        Me.CiudadesToolStripMenuItem.Name = "CiudadesToolStripMenuItem"
        Me.CiudadesToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.CiudadesToolStripMenuItem.Text = "Ci&udades"
        '
        'SucursalesToolStripMenuItem
        '
        Me.SucursalesToolStripMenuItem.Name = "SucursalesToolStripMenuItem"
        Me.SucursalesToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.SucursalesToolStripMenuItem.Text = "&Sucursales"
        '
        'DepartamentosToolStripMenuItem
        '
        Me.DepartamentosToolStripMenuItem.Name = "DepartamentosToolStripMenuItem"
        Me.DepartamentosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.DepartamentosToolStripMenuItem.Text = "&Departamentos"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(177, 6)
        '
        'UsuariosDeLaTomaToolStripMenuItem
        '
        Me.UsuariosDeLaTomaToolStripMenuItem.Name = "UsuariosDeLaTomaToolStripMenuItem"
        Me.UsuariosDeLaTomaToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.UsuariosDeLaTomaToolStripMenuItem.Text = "Usuarios de la &Toma"
        Me.UsuariosDeLaTomaToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(177, 6)
        Me.ToolStripSeparator4.Visible = False
        '
        'CambiarClaveToolStripMenuItem
        '
        Me.CambiarClaveToolStripMenuItem.Name = "CambiarClaveToolStripMenuItem"
        Me.CambiarClaveToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.CambiarClaveToolStripMenuItem.Text = "C&ambiar Clave"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.UsuariosToolStripMenuItem.Text = "&Usuarios"
        '
        'ActivosToolStripMenuItem
        '
        Me.ActivosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbrirToolStripMenuItem, Me.ToolStripSeparator5, Me.SubirDeExcelToolStripMenuItem, Me.BajarAExcelToolStripMenuItem})
        Me.ActivosToolStripMenuItem.Name = "ActivosToolStripMenuItem"
        Me.ActivosToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.ActivosToolStripMenuItem.Text = "&Activos"
        '
        'AbrirToolStripMenuItem
        '
        Me.AbrirToolStripMenuItem.Name = "AbrirToolStripMenuItem"
        Me.AbrirToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.AbrirToolStripMenuItem.Text = "&Abrir"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(143, 6)
        '
        'SubirDeExcelToolStripMenuItem
        '
        Me.SubirDeExcelToolStripMenuItem.Name = "SubirDeExcelToolStripMenuItem"
        Me.SubirDeExcelToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.SubirDeExcelToolStripMenuItem.Text = "&Subir de Excel"
        '
        'BajarAExcelToolStripMenuItem
        '
        Me.BajarAExcelToolStripMenuItem.Name = "BajarAExcelToolStripMenuItem"
        Me.BajarAExcelToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.BajarAExcelToolStripMenuItem.Text = "&Bajar a Excel"
        '
        'VentanasToolStripMenuItem
        '
        Me.VentanasToolStripMenuItem.Name = "VentanasToolStripMenuItem"
        Me.VentanasToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.VentanasToolStripMenuItem.Text = "&Ventanas"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(923, 415)
        Me.Controls.Add(Me.MenuStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Software para la Toma de Inventarios de Activos Fijos Carrera Torres & Asociados " & _
    "S.A.  2006 - 2017"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents EspecificacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActivosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentanasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents EmpresaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TipoDeActivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaseDeActivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SituaciónDeActivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CiudadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SucursalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepartamentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents UsuariosDeLaTomaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CambiarClaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbrirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SubirDeExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BajarAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
