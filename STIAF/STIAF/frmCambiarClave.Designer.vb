﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambiarClave
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtRedigitada = New System.Windows.Forms.TextBox()
        Me.TxtNueva = New System.Windows.Forms.TextBox()
        Me.TxtClaveActual = New System.Windows.Forms.TextBox()
        Me.lblClaveRedigitada = New System.Windows.Forms.Label()
        Me.lblClaveNueva = New System.Windows.Forms.Label()
        Me.lblClaveAnterior = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(222, 100)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(96, 30)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(120, 100)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(96, 30)
        Me.btnAceptar.TabIndex = 9
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtRedigitada)
        Me.GroupBox1.Controls.Add(Me.TxtNueva)
        Me.GroupBox1.Controls.Add(Me.TxtClaveActual)
        Me.GroupBox1.Controls.Add(Me.lblClaveRedigitada)
        Me.GroupBox1.Controls.Add(Me.lblClaveNueva)
        Me.GroupBox1.Controls.Add(Me.lblClaveAnterior)
        Me.GroupBox1.Location = New System.Drawing.Point(3, -4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(315, 101)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'TxtRedigitada
        '
        Me.TxtRedigitada.Location = New System.Drawing.Point(126, 66)
        Me.TxtRedigitada.Name = "TxtRedigitada"
        Me.TxtRedigitada.PasswordChar = Global.Microsoft.VisualBasic.ChrW(45)
        Me.TxtRedigitada.Size = New System.Drawing.Size(174, 20)
        Me.TxtRedigitada.TabIndex = 2
        '
        'TxtNueva
        '
        Me.TxtNueva.Location = New System.Drawing.Point(126, 40)
        Me.TxtNueva.Name = "TxtNueva"
        Me.TxtNueva.PasswordChar = Global.Microsoft.VisualBasic.ChrW(45)
        Me.TxtNueva.Size = New System.Drawing.Size(174, 20)
        Me.TxtNueva.TabIndex = 1
        '
        'TxtClaveActual
        '
        Me.TxtClaveActual.Location = New System.Drawing.Point(126, 15)
        Me.TxtClaveActual.Name = "TxtClaveActual"
        Me.TxtClaveActual.PasswordChar = Global.Microsoft.VisualBasic.ChrW(45)
        Me.TxtClaveActual.Size = New System.Drawing.Size(174, 20)
        Me.TxtClaveActual.TabIndex = 0
        '
        'lblClaveRedigitada
        '
        Me.lblClaveRedigitada.Location = New System.Drawing.Point(12, 70)
        Me.lblClaveRedigitada.Name = "lblClaveRedigitada"
        Me.lblClaveRedigitada.Size = New System.Drawing.Size(109, 13)
        Me.lblClaveRedigitada.TabIndex = 2
        Me.lblClaveRedigitada.Text = "Redigitar Contraseña:"
        '
        'lblClaveNueva
        '
        Me.lblClaveNueva.Location = New System.Drawing.Point(12, 43)
        Me.lblClaveNueva.Name = "lblClaveNueva"
        Me.lblClaveNueva.Size = New System.Drawing.Size(109, 13)
        Me.lblClaveNueva.TabIndex = 1
        Me.lblClaveNueva.Text = "Nueva Contraseña:"
        '
        'lblClaveAnterior
        '
        Me.lblClaveAnterior.Location = New System.Drawing.Point(12, 18)
        Me.lblClaveAnterior.Name = "lblClaveAnterior"
        Me.lblClaveAnterior.Size = New System.Drawing.Size(109, 13)
        Me.lblClaveAnterior.TabIndex = 0
        Me.lblClaveAnterior.Text = "Contraseña Actual:"
        '
        'frmCambiarClave
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(321, 132)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmCambiarClave"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambiar la Clave"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtRedigitada As System.Windows.Forms.TextBox
    Friend WithEvents TxtNueva As System.Windows.Forms.TextBox
    Friend WithEvents TxtClaveActual As System.Windows.Forms.TextBox
    Friend WithEvents lblClaveRedigitada As System.Windows.Forms.Label
    Friend WithEvents lblClaveNueva As System.Windows.Forms.Label
    Friend WithEvents lblClaveAnterior As System.Windows.Forms.Label
End Class
