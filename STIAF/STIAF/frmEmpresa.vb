﻿Public Class frmEmpresa

    Private Sub frmEmpresa_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPGetEmpresas()
        ' Estado de los botones de comando
        ' En todos los casos de niveles de seguridad
        btnModificar.Enabled = False     ' Modificar
        btnEliminar.Enabled = False     ' Eliminar
        If CInt(VGNivelSeg) <= 4 Then  ' Invitados y Usuarios de otros módulos
            btnIngresar.Enabled = False  ' Ingresar
        End If
    End Sub

    Private Sub FPGetEmpresas()
        ' Obtengo las empresas ingresadas en la base de datos
        Dim consulta As String = "SELECT * FROM empresa ORDER BY em_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "empresas")
        Dim NumRegistros As Byte = registro.Tables("empresas").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("No existen empresas...")
        Else
            ReDim VGCodEmpresa(NumRegistros)
            lstEmpresas.Items.Clear()
            For i = 1 To NumRegistros
                VGCodEmpresa(i - 1) = registro.Tables("empresas").Rows(i - 1).Item("cod_empresa")
                lstEmpresas.Items.Add(registro.Tables("empresas").Rows(i - 1).Item("em_nombre"))
            Next i
        End If
    End Sub

    Private Sub lstEmpresas_DoubleClick(sender As Object, e As EventArgs) Handles lstEmpresas.DoubleClick
        ' Primero obtengo el cod_empresa
        TxtCodigo.Text = VGCodEmpresa(lstEmpresas.SelectedIndex)
        FillFields()
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM empresa WHERE cod_empresa = " & Trim(TxtCodigo.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "empresa")
        Dim NumRegistros As Byte = registro.Tables("empresa").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar la empresa...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            TxtNombre.Text = registro.Tables("empresa").Rows(0).Item("em_nombre")
            TxtDireccion.Text = registro.Tables("empresa").Rows(0).Item("em_direccion")
            TxtTelefono.Text = registro.Tables("empresa").Rows(0).Item("em_telefono")
            TxtFax.Text = registro.Tables("empresa").Rows(0).Item("em_fax")
            TxtPrefijo.Text = registro.Tables("empresa").Rows(0).Item("em_prefijo")
            TxtEmail.Text = registro.Tables("empresa").Rows(0).Item("em_email")
        End If
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = False
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ' Procedimiento para modificar el nombre de las empresas
        VGStrSQL$ = "UPDATE empresa SET em_nombre = '"
        ' Campo que contiene el nombre
        VGStrSQL$ = VGStrSQL$ & Trim(TxtNombre.Text) & "',"
        ' Campo que contiene la dirección
        VGStrSQL$ = VGStrSQL$ & "em_direccion = '" & Trim(TxtDireccion.Text) & "',"
        ' Campo que contiene el teléfono
        VGStrSQL$ = VGStrSQL$ & "em_telefono = '" & Trim(TxtTelefono.Text) & "',"
        ' Campo que contiene el fax
        VGStrSQL$ = VGStrSQL$ & "em_fax = '" & Trim(TxtFax.Text) & "',"
        ' Campo que contiene el prefijo
        VGStrSQL$ = VGStrSQL$ & "em_prefijo = '" & Trim(TxtPrefijo.Text) & "',"
        ' Campo que contiene el email
        VGStrSQL$ = VGStrSQL$ & "em_email = '" & Trim(TxtEmail.Text) & "'"
        ' Cláusula WHERE
        VGStrSQL$ = VGStrSQL$ & " where cod_empresa = " & TxtCodigo.Text
        Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
        comando.ExecuteNonQuery()
        MsgBox("El registro ha sido actualizado", vbInformation, "STIAF")
        FPGetEmpresas()
    End Sub

    Private Sub FPLimpiar()
        ' Borrar el contenido de los campos en pantalla
        TxtCodigo.Text = ""
        TxtNombre.Text = ""
        TxtDireccion.Text = ""
        TxtTelefono.Text = ""
        TxtFax.Text = ""
        TxtPrefijo.Text = ""
        TxtEmail.Text = ""
        TxtNombre.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If Trim(TxtNombre.Text) = "" Then
            MsgBox("Debe digitar el dato para el nombre de la Empresa.", vbExclamation, "Error en el ingreso de datos")
            TxtNombre.Focus()
        ElseIf FPBuscarCampo("empresa", "em_nombre", Trim(TxtNombre.Text)) Then
            MsgBox("El nombre de la Empresa ya se encuentra ingresado " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtNombre.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla empresa
            VGStrSQL$ = "INSERT INTO empresa (em_nombre, em_direccion, em_telefono, em_fax,em_prefijo, em_email) VALUES ("
            ' Nombre de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtNombre.Text) & "',"
            ' Dirección de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtDireccion.Text) & "',"
            ' Teléfono de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtTelefono.Text) & "',"
            ' Fax de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtFax.Text) & "',"
            ' Prefijo de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtPrefijo.Text) & "',"
            ' Email del Contacto de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtEmail.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido insertado", vbInformation, "STIAF")
            ' Estado de los botones de comando
            If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
                btnIngresar.Enabled = False
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            VGStrSQL$ = "SELECT max(cod_empresa) AS secuencial FROM empresa"
            Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "maximo")
            Dim NumRegistros As Byte = registro.Tables("maximo").Rows.Count
            If NumRegistros = 0 Then
                MsgBox("Ocurrió un error al recuperar el máximo cod_empresa...")
            Else
                TxtCodigo.Text = CStr(registro.Tables("maximo").Rows(0).Item("secuencial"))
            End If

            ' Actualizo la lista de empresas
            FPGetEmpresas()
            FPLimpiar()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla empresa
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGStrSQL$ = "delete * from empresa where cod_empresa = " & TxtCodigo.Text
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido eliminado", vbInformation, "STIAF")
            FPLimpiar()
            FPGetEmpresas()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class