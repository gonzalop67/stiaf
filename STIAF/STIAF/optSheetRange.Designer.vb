﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class optSheetRange
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(optSheetRange))
        Me.butExit = New System.Windows.Forms.Button()
        Me.butDesAsociar = New System.Windows.Forms.Button()
        Me.butAsociar = New System.Windows.Forms.Button()
        Me.sprCampos = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lstTitulos = New System.Windows.Forms.ListBox()
        Me.lstCampos = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.butLoad = New System.Windows.Forms.Button()
        Me.Focos = New System.Windows.Forms.ProgressBar()
        Me.faRangeSample = New System.Windows.Forms.DataGridView()
        Me.lstSheetranges = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboEmpresa = New System.Windows.Forms.ComboBox()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.lblSheetPath = New System.Windows.Forms.Label()
        CType(Me.sprCampos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.faRangeSample, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butExit
        '
        Me.butExit.Location = New System.Drawing.Point(730, 327)
        Me.butExit.Name = "butExit"
        Me.butExit.Size = New System.Drawing.Size(94, 23)
        Me.butExit.TabIndex = 54
        Me.butExit.Text = "Cancelar"
        Me.butExit.UseVisualStyleBackColor = True
        '
        'butDesAsociar
        '
        Me.butDesAsociar.Location = New System.Drawing.Point(636, 327)
        Me.butDesAsociar.Name = "butDesAsociar"
        Me.butDesAsociar.Size = New System.Drawing.Size(94, 23)
        Me.butDesAsociar.TabIndex = 53
        Me.butDesAsociar.Text = "Des-Asociar"
        Me.butDesAsociar.UseVisualStyleBackColor = True
        '
        'butAsociar
        '
        Me.butAsociar.Location = New System.Drawing.Point(542, 327)
        Me.butAsociar.Name = "butAsociar"
        Me.butAsociar.Size = New System.Drawing.Size(94, 23)
        Me.butAsociar.TabIndex = 52
        Me.butAsociar.Text = "Asociar"
        Me.butAsociar.UseVisualStyleBackColor = True
        '
        'sprCampos
        '
        Me.sprCampos.AllowUserToAddRows = False
        Me.sprCampos.AllowUserToDeleteRows = False
        Me.sprCampos.AllowUserToOrderColumns = True
        Me.sprCampos.AllowUserToResizeColumns = False
        Me.sprCampos.AllowUserToResizeRows = False
        Me.sprCampos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.sprCampos.Location = New System.Drawing.Point(394, 195)
        Me.sprCampos.Name = "sprCampos"
        Me.sprCampos.Size = New System.Drawing.Size(430, 128)
        Me.sprCampos.TabIndex = 51
        '
        'Column1
        '
        Me.Column1.HeaderText = "Nombre del Campo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 200
        '
        'Column2
        '
        Me.Column2.HeaderText = "Asociado con"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 200
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Location = New System.Drawing.Point(394, 173)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(430, 19)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Campos asociados:"
        '
        'lstTitulos
        '
        Me.lstTitulos.FormattingEnabled = True
        Me.lstTitulos.Location = New System.Drawing.Point(612, 48)
        Me.lstTitulos.Name = "lstTitulos"
        Me.lstTitulos.Size = New System.Drawing.Size(212, 121)
        Me.lstTitulos.TabIndex = 49
        '
        'lstCampos
        '
        Me.lstCampos.FormattingEnabled = True
        Me.lstCampos.Location = New System.Drawing.Point(394, 48)
        Me.lstCampos.Name = "lstCampos"
        Me.lstCampos.Size = New System.Drawing.Size(212, 121)
        Me.lstCampos.TabIndex = 48
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Location = New System.Drawing.Point(612, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(212, 19)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Lista de Campos de Excel a subir:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Location = New System.Drawing.Point(394, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(212, 19)
        Me.Label3.TabIndex = 46
        Me.Label3.Text = "Lista de Campos del STIAF:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Location = New System.Drawing.Point(394, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(430, 19)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "ASOCIAR"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'butLoad
        '
        Me.butLoad.Location = New System.Drawing.Point(302, 327)
        Me.butLoad.Name = "butLoad"
        Me.butLoad.Size = New System.Drawing.Size(86, 23)
        Me.butLoad.TabIndex = 44
        Me.butLoad.Text = "Subir a la BDD"
        Me.butLoad.UseVisualStyleBackColor = True
        '
        'Focos
        '
        Me.Focos.Location = New System.Drawing.Point(5, 327)
        Me.Focos.Name = "Focos"
        Me.Focos.Size = New System.Drawing.Size(294, 23)
        Me.Focos.TabIndex = 43
        '
        'faRangeSample
        '
        Me.faRangeSample.AllowUserToAddRows = False
        Me.faRangeSample.AllowUserToDeleteRows = False
        Me.faRangeSample.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.faRangeSample.Location = New System.Drawing.Point(5, 173)
        Me.faRangeSample.Name = "faRangeSample"
        Me.faRangeSample.RowHeadersVisible = False
        Me.faRangeSample.Size = New System.Drawing.Size(383, 150)
        Me.faRangeSample.TabIndex = 42
        '
        'lstSheetranges
        '
        Me.lstSheetranges.FormattingEnabled = True
        Me.lstSheetranges.Location = New System.Drawing.Point(5, 72)
        Me.lstSheetranges.Name = "lstSheetranges"
        Me.lstSheetranges.Size = New System.Drawing.Size(383, 95)
        Me.lstSheetranges.TabIndex = 41
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(5, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(383, 19)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Lista de Hojas de Cálculo"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboEmpresa
        '
        Me.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresa.FormattingEnabled = True
        Me.cboEmpresa.Location = New System.Drawing.Point(56, 26)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(332, 21)
        Me.cboEmpresa.TabIndex = 39
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Location = New System.Drawing.Point(6, 28)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(51, 13)
        Me.lblEmpresa.TabIndex = 38
        Me.lblEmpresa.Text = "Empresa:"
        '
        'lblSheetPath
        '
        Me.lblSheetPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSheetPath.Location = New System.Drawing.Point(5, 4)
        Me.lblSheetPath.Name = "lblSheetPath"
        Me.lblSheetPath.Size = New System.Drawing.Size(383, 19)
        Me.lblSheetPath.TabIndex = 37
        Me.lblSheetPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'optSheetRange
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(828, 354)
        Me.Controls.Add(Me.butExit)
        Me.Controls.Add(Me.butDesAsociar)
        Me.Controls.Add(Me.butAsociar)
        Me.Controls.Add(Me.sprCampos)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lstTitulos)
        Me.Controls.Add(Me.lstCampos)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.butLoad)
        Me.Controls.Add(Me.Focos)
        Me.Controls.Add(Me.faRangeSample)
        Me.Controls.Add(Me.lstSheetranges)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboEmpresa)
        Me.Controls.Add(Me.lblEmpresa)
        Me.Controls.Add(Me.lblSheetPath)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "optSheetRange"
        Me.Text = "Obtención de Datos desde Excel"
        CType(Me.sprCampos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.faRangeSample, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butExit As System.Windows.Forms.Button
    Friend WithEvents butDesAsociar As System.Windows.Forms.Button
    Friend WithEvents butAsociar As System.Windows.Forms.Button
    Friend WithEvents sprCampos As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lstTitulos As System.Windows.Forms.ListBox
    Friend WithEvents lstCampos As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butLoad As System.Windows.Forms.Button
    Friend WithEvents Focos As System.Windows.Forms.ProgressBar
    Friend WithEvents faRangeSample As System.Windows.Forms.DataGridView
    Friend WithEvents lstSheetranges As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboEmpresa As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents lblSheetPath As System.Windows.Forms.Label
End Class
