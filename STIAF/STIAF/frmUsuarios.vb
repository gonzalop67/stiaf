﻿Public Class frmUsuarios

    Private Sub frmUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Consulta de todos los usuarios definidos
        FPQueryAll()
    End Sub

    Private Sub FPQueryAll()
        ' Procedimiento para mostrar todos los usuarios definidos
        Dim sql As String
        sql = "SELECT cod_usuario AS Código, us_name AS Login, us_fullname AS [Nombre Completo], us_alias AS Alias, us_modifica AS [Modificado por], ni_nombre AS [Nivel de acceso]" & _
                " FROM usuario, nivel_seguridad WHERE usuario.cod_nivseg = nivel_seguridad.cod_nivseg" & _
                " ORDER BY us_name"
        Dim da As New OleDb.OleDbDataAdapter(sql, cn)
        Dim ds As New DataSet
        da.Fill(ds)
        dtgUsuarios.DataSource = ds.Tables(0).DefaultView
        'Ancho de las columnas
        dtgUsuarios.Columns(0).Width = 50    'Código
        dtgUsuarios.Columns(1).Width = 90    'Login
        dtgUsuarios.Columns(2).Width = 150   'Nombre completo
        dtgUsuarios.Columns(3).Width = 90    'Alias
        dtgUsuarios.Columns(4).Width = 150   'Modificado por
        dtgUsuarios.Columns(5).Width = 150   'Nivel de acceso
    End Sub

    Private Sub btnAñadir_Click(sender As Object, e As EventArgs) Handles btnAñadir.Click
        frmAdmUsers.Text = "Nuevo Usuario"
        frmAdmUsers.ShowDialog()
        FPQueryAll()
    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        VGCodUsuarioSel = dtgUsuarios.CurrentRow.Cells(0).Value
        frmAdmUsers.Text = "Modificar Usuario"
        frmAdmUsers.ShowDialog()
        FPQueryAll()
    End Sub

    Private Sub dtgUsuarios_DoubleClick(sender As Object, e As EventArgs) Handles dtgUsuarios.DoubleClick
        VGCodUsuarioSel = dtgUsuarios.CurrentRow.Cells(0).Value
        frmAdmUsers.Text = "Modificar Usuario"
        frmAdmUsers.ShowDialog()
        FPQueryAll()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla usuario
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGCodUsuarioSel = dtgUsuarios.CurrentRow.Cells(0).Value
            VGStrSQL$ = "delete * from usuario where cod_usuario = " & VGCodUsuarioSel
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El usuario ha sido eliminado exitosamente", vbInformation, "STIAF")
            FPQueryAll()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class