﻿Module Miscelaneos

    Public Function FPReemplazarComillas(Cadena As String) As String
        ' Reemplaza las comillas simples por comillas dobles
        While InStr(1, Cadena, "'") > 0
            Cadena = Mid(Cadena, 1, InStr(1, Cadena, "'") - 1) & """" & Mid(Cadena, InStr(1, Cadena, "'") + 1)
        End While
        FPReemplazarComillas = Cadena
    End Function

    Public Function FPCambiarComaAPunto(Cadena As String) As String
        Dim pos1 As Integer
        ' Quitar algún punto que exista
        While InStr(1, Cadena, ".") > 0
            Cadena = Mid(Cadena, 1, InStr(1, Cadena, ".") - 1) & Mid(Cadena, InStr(1, Cadena, ".") + 1)
        End While
        ' Convierte las comas de la cadena ingresada a puntos
        pos1 = InStr(1, Cadena, ",")
        If pos1 > 0 Then
            Cadena = Mid(Cadena, 1, pos1 - 1) & "." & Mid(Cadena, pos1 + 1)
        End If
        FPCambiarComaAPunto = Cadena
    End Function

    Public Function FPBuscarCampo(tabla As String, campo As String, descripcion As String)
        VGStrSQL$ = "SELECT * FROM " + Trim(tabla) + " WHERE " + Trim(campo) + " = '" + descripcion + "'"
        Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "tabla")
        Dim NumRegistros As Byte = registro.Tables("tabla").Rows.Count
        FPBuscarCampo = IIf(NumRegistros = 0, False, True)
    End Function

    Public Sub GenerarSecuencial(gridView As DataGridView)
        If gridView IsNot Nothing Then
            For Each r As DataGridViewRow In gridView.Rows
                r.HeaderCell.Value = (r.Index + 1).ToString()
            Next
        End If
    End Sub

    Public Function autoCompletarTextBox(ByVal campoTexto As TextBox, ByVal nomCampo As String, ByVal nomTabla As String) As AutoCompleteStringCollection
        Dim de As New OleDb.OleDbDataAdapter("SELECT [" & nomCampo & "] AS campo FROM " & nomTabla, cn)
        Dim dt As New DataTable
        de.Fill(dt)

        Dim coleccion As New AutoCompleteStringCollection
        For Each row As DataRow In dt.Rows
            coleccion.Add(Convert.ToString(row("campo")))
        Next
        Return coleccion
    End Function

End Module
