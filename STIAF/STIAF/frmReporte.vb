﻿Imports Microsoft.Office.Core
Imports Microsoft.Office.Interop.Excel
Imports System.Data

Public Class frmReporte

    Dim sDetalle As String = ""

    Private Sub frmReporte_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim sCodEmpresa As String = basMain.VGCodEmpresaSel

        Dim sNomCampo As String = basMain.sNomCampo
        Dim strCriterio As String = basMain.strCriterio
        Dim sCampoBusqueda As String = ""
        Dim sNomCampoBusqueda As String = ""
        Dim sNomTabla As String = ""

        Select Case sNomCampo
            Case "Tipo de Activo"
                sCampoBusqueda = "descripcion"
                sNomTabla = "tipo_activo"
                sNomCampoBusqueda = "cod_tipo_activo"
            Case "Clase de Activo"
                sCampoBusqueda = "descripcion"
                sNomTabla = "clase_activo"
                sNomCampoBusqueda = "cod_clase_activo"
            Case "Usuario"
                sCampoBusqueda = "nombre"
                sNomTabla = "usuario_toma"
                sNomCampoBusqueda = "id_usuario"
            Case "Situacion Activo"
                sCampoBusqueda = "descripcion"
                sNomTabla = "situacion"
                sNomCampoBusqueda = "cod_situacion"
            Case "Departamento"
                sCampoBusqueda = "de_nombre"
                sNomTabla = "departamento"
                sNomCampoBusqueda = "cod_departamento"
            Case "Ciudad"
                sCampoBusqueda = "ci_nombre"
                sNomTabla = "ciudad"
                sNomCampoBusqueda = "cod_ciudad"
        End Select

        If sNomCampo = "Tipo de Activo" Or sNomCampo = "Clase de Activo" Or sNomCampo = "Usuario" Or sNomCampo = "Situacion Activo" Or sNomCampo = "Departamento" Or sNomCampo = "Ciudad" Then
            Dim consulta As String = "SELECT [" & sCampoBusqueda & "] as Detalle FROM " & sNomTabla & " WHERE [" & sNomCampoBusqueda & "] = " & Trim(strCriterio)
            Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "busqueda")
            sDetalle = registro.Tables("busqueda").Rows(0).Item("Detalle")
        Else
            sDetalle = strCriterio
        End If

        Me.Text = "Reporte de " & basMain.sNomCampo & " [" & sDetalle & "]"

        'Aqui va el codigo para la consulta que sera enviada al datagridview
        Dim query As String = "SELECT [Codigo Original],[Codigo Barras],[Descripcion 1],[Usuario],de_nombre AS Departamento,ci_nombre AS Ciudad,[Valor Adquisicion],descripcion AS Situacion,[Adicional 1]"
        query = query & " FROM activo a, departamento d, ciudad c, situacion s WHERE a.Departamento = d.cod_departamento AND a.Ciudad = c.cod_ciudad AND a.[Situacion Activo] = s.cod_situacion"
        If sNomCampo = "Tipo de Activo" Or sNomCampo = "Clase de Activo" Or sNomCampo = "Usuario" Or sNomCampo = "Situacion Activo" Or sNomCampo = "Ciudad" Or sNomCampo = "Departamento" Then
            query = query & " AND [" & Trim(sNomCampo) & "] = " & Trim(strCriterio)
        ElseIf sNomCampo = "Fecha Compra" Then
            query = query & " AND [" & Trim(sNomCampo) & "] = #" & Trim(strCriterio) & "#"
        Else
            query = query & " AND [" & Trim(sNomCampo) & "] = '" & Trim(strCriterio) & "'"
        End If
        query = query & " AND cod_empresa=" & Trim(sCodEmpresa)

        'Aqui va el enlace de la consulta con el datagridview
        Dim da As New OleDb.OleDbDataAdapter(query, cn)
        Dim ds As New DataSet
        da.Fill(ds, "activos")

        grdActivos.DataSource = ds.Tables(0).DefaultView

        'resize all columns' widths to fit the data
        For i = 0 To grdActivos.Columns.Count - 1
            grdActivos.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells)
        Next

        'Enumerar las filas del datagridview
        GenerarSecuencial(grdActivos)

        Dim total As Double = 0
        Dim fila As DataGridViewRow = New DataGridViewRow()

        For Each fila In grdActivos.Rows
            total += Convert.ToDouble(fila.Cells("Valor Adquisicion").Value)
        Next

        txtTotalValorCompra.Text = Convert.ToString(total)

        Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub cmdIrAExcel_Click(sender As Object, e As EventArgs) Handles cmdIrAExcel.Click

        'Creamos las variables
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet

        'Nombre del Usuario
        Dim sNomUsuario As String = sDetalle

        Dim sNewXlsFile As String
        Dim sXlsTemplate As String = My.Computer.FileSystem.CurrentDirectory & "\PlantillaBPU.xls"

        If MsgBox("Este proceso puede durar varios minutos. ¿Está seguro que desea continuar?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Aquí bajo los datos de access a excel
            If Me.grdActivos.Rows.Count > 0 Then
                ' Reporte especial solo para bienes por usuario
                If sNomCampo = "Usuario" Then
                    'Nombre del Usuario
                    sNewXlsFile = My.Computer.FileSystem.CurrentDirectory & "\Reporte Bienes Por Usuario (" & Trim(sNomUsuario) & ").xls"
                    If Dir(sNewXlsFile) <> "" Then Kill(sNewXlsFile)
                    exLibro = exApp.Workbooks.Open(sXlsTemplate)
                    ' Hace que Excel se vea
                    exApp.Visible = True
                    exHoja = exApp.ActiveSheet
                    exLibro.SaveAs(sNewXlsFile)

                    'Cabecera para indicar los datos de la empresa
                    'Obtengo los datos de la empresa
                    Sql = "SELECT em_nombre FROM empresa WHERE cod_empresa = " & Trim(VGCodEmpresaSel)

                    Dim adaptador As New OleDb.OleDbDataAdapter(Sql, cn)
                    Dim registro As New DataSet
                    adaptador.Fill(registro, "empresa")
                    exHoja.Cells(11, 2).Formula = registro.Tables("empresa").Rows(0).Item("em_nombre")

                    'Nombre del Usuario
                    exHoja.Cells(12, 2).Formula = sNomUsuario
                    'Fecha actual
                    exHoja.Cells(12, 10).Formula = Now.ToString("MMMM dd, yyyy")

                    'Recorrer el spread para pasar a Excel
                    For fila = 0 To grdActivos.Rows.Count - 1
                        ' Código Original
                        exHoja.Cells(fila + 15, 1).Formula = grdActivos.Item(0, fila).Value
                        ' Código de Barras
                        exHoja.Cells(fila + 15, 3).Formula = grdActivos.Item(1, fila).Value
                        ' Descripción 1
                        exHoja.Cells(fila + 15, 6).Formula = grdActivos.Item(2, fila).Value
                        ' Estado
                        exHoja.Cells(fila + 15, 8).Formula = grdActivos.Item(7, fila).Value
                        ' Adicional 1
                        exHoja.Cells(fila + 15, 10).Formula = grdActivos.Item(8, fila).Value
                    Next
                Else
                    'Aquí paso los datos del grid a Excel
                    'Añadimos el Libro al programa, y la hoja al libro

                    exLibro = exApp.Workbooks.Add
                    'exHoja = exLibro.Worksheets.Add()
                    exHoja = exApp.ActiveSheet

                    ' ¿Cuantas columnas y cuantas filas?

                    Dim NCol As Integer = grdActivos.ColumnCount
                    Dim NRow As Integer = grdActivos.RowCount

                    'Aqui recorremos todas las filas, y por cada fila todas las columnas
                    'y vamos escribiendo.

                    For i As Integer = 1 To NCol
                        exHoja.Cells.Item(1, i) = grdActivos.Columns(i - 1).Name.ToString
                    Next

                    For Fila As Integer = 0 To NRow - 1
                        For Col As Integer = 0 To NCol - 1
                            exHoja.Cells.Item(Fila + 2, Col + 1) = grdActivos.Item(Col, Fila).Value
                        Next
                    Next

                    'Titulo en negrita, Alineado al centro y que el tamaño de la columna

                    'se ajuste al texto

                    exHoja.Rows.Item(1).Font.Bold = 1
                    exHoja.Rows.Item(1).HorizontalAlignment = 3
                    exHoja.Columns.AutoFit()

                    'Aplicación visible

                    exApp.Application.Visible = True
                End If

            End If

            exApp = Nothing
            exLibro = Nothing
            exHoja = Nothing

            Cursor = System.Windows.Forms.Cursors.Default
        End If

    End Sub
End Class