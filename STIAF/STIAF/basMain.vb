﻿Module basMain
    Public cs As String
    Public cn As New OleDb.OleDbConnection()

    Public VGPasswd As String
    Public VGUsuario As String
    Public VGCodUsuario As Long
    Public VGCodActivoSel As Long

    ' Variables globales
    Public VGNivelSeg As Integer
    Public VGStrSQL, Sql As String
    Public VGTituloSoftware As String = "Software para la Toma de Inventario de Activos Fijos"
    Public VGCodNivSeg()
    Public VGCodEmpresa()
    Public VGCodTipoActivo()
    Public VGCodClaseActivo()
    Public VGCodUsuarioToma()
    Public VGCodSituacionActivo()
    Public VGCodCiudad()
    Public VGCodSucursal()
    Public VGCodDepartamento()

    Public VGCodUsuarioSel
    Public VGCodEmpresaSel
    Public sNomCampo As String
    Public strCriterio As String

    Public Contador As Long = 0 ' Variable utilizada para el efecto del progressbar

    ' Variables para usar con excel
    Public sNameFile As String

    Public Sub Conectar()
        Dim sPath_Access As String
        ' -- Ruta del Archivo ACCDB
        sPath_Access = Application.StartupPath & "\activos.accdb"
        ' MessageBox.Show(sPath)
        cs = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & _
             sPath_Access & ";Persist Security Info=False;"
        Try
            cn.ConnectionString = cs
            cn.Open()

            ' Si no salta algún error la conexión fue exitosa
        Catch ex As Exception
            ' Existe un error
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Module
