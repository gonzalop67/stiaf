﻿Public Class frmSitActivo

    Private Sub frmSitActivo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPGetSituacionActivos()
        ' Estado de los botones de comando
        ' En todos los casos de niveles de seguridad
        btnModificar.Enabled = False     ' Modificar
        btnEliminar.Enabled = False     ' Eliminar
        If CInt(VGNivelSeg) <= 4 Then  ' Invitados y Usuarios de otros módulos
            btnIngresar.Enabled = False  ' Ingresar
        End If
    End Sub

    Private Sub FPGetSituacionActivos()
        ' Obtengo los tipos de activos ingresados en la base de datos
        Dim consulta As String = "SELECT * FROM situacion ORDER BY cod_situacion"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "situacion_activo")
        Dim NumRegistros As Byte = registro.Tables("situacion_activo").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("No existen situaciones de activos...")
        Else
            ReDim VGCodSituacionActivo(NumRegistros)
            lstSituacionActivo.Items.Clear()
            For i = 1 To NumRegistros
                VGCodSituacionActivo(i - 1) = registro.Tables("situacion_activo").Rows(i - 1).Item("cod_situacion")
                lstSituacionActivo.Items.Add(registro.Tables("situacion_activo").Rows(i - 1).Item("descripcion"))
            Next i
        End If
    End Sub

    Private Sub lstSituacionActivo_DoubleClick(sender As Object, e As EventArgs) Handles lstSituacionActivo.DoubleClick
        ' Primero obtengo el cod_empresa
        TxtCodigo.Text = VGCodSituacionActivo(lstSituacionActivo.SelectedIndex)
        FillFields()
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM situacion WHERE cod_situacion = " & Trim(TxtCodigo.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "situacion_activo")
        Dim NumRegistros As Byte = registro.Tables("situacion_activo").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar la situación del activo...")
        Else
            TxtDescripcion.Text = registro.Tables("situacion_activo").Rows(0).Item("descripcion")
        End If
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = False
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
        TxtDescripcion.Focus()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ' Procedimiento para modificar la descripción del tipo de activo
        VGStrSQL$ = "UPDATE situacion SET descripcion = '"
        ' Campo que contiene el nombre
        VGStrSQL$ = VGStrSQL$ & Trim(TxtDescripcion.Text) & "'"
        ' Cláusula WHERE
        VGStrSQL$ = VGStrSQL$ & " where cod_situacion = " & TxtCodigo.Text
        Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
        comando.ExecuteNonQuery()
        MsgBox("El registro ha sido actualizado", vbInformation, "STIAF")
        FPGetSituacionActivos()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub FPLimpiar()
        ' Borrar el contenido de los campos en pantalla
        TxtCodigo.Text = ""
        TxtDescripcion.Text = ""
        TxtDescripcion.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla tipo_activo
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGStrSQL$ = "delete * from situacion where cod_situacion = " & TxtCodigo.Text
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido eliminado", vbInformation, "STIAF")
            FPLimpiar()
            FPGetSituacionActivos()
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If Trim(TxtDescripcion.Text) = "" Then
            MsgBox("Debe digitar el dato para la descripción de la Situación del Activo.", vbExclamation, "Error en el ingreso de datos")
            TxtDescripcion.Focus()
        ElseIf FPBuscarCampo("situacion", "descripcion", Trim(TxtDescripcion.Text)) Then
            MsgBox("La descripción de la Situación del Activo ya se encuentra ingresada " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtDescripcion.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla empresa
            VGStrSQL$ = "INSERT INTO situacion (descripcion) VALUES ("
            ' Nombre de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtDescripcion.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido insertado", vbInformation, "STIAF")
            ' Estado de los botones de comando
            If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
                btnIngresar.Enabled = False
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            VGStrSQL$ = "SELECT max(cod_situacion) AS secuencial FROM situacion"
            Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "maximo")
            Dim NumRegistros As Byte = registro.Tables("maximo").Rows.Count
            If NumRegistros = 0 Then
                MsgBox("Ocurrió un error al recuperar el máximo cod_tipo_activo...")
            Else
                TxtCodigo.Text = CStr(registro.Tables("maximo").Rows(0).Item("secuencial"))
            End If

            ' Actualizo la lista de empresas
            FPGetSituacionActivos()
            FPLimpiar()
        End If
    End Sub
End Class