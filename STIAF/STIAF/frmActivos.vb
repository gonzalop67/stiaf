﻿Public Class frmActivos

    Dim VLCodEmpresa, VLCodTipoActivo, VLCodCiudad, VLCodSucursal As Integer

    Dim sCodUsuario As String   ' Variable utilizada para almacenar el id_usuario asociado al activo

    ' Variables necesarias para realizar la paginación
    Dim PgSize As Integer = 10
    Dim CurrentPageIndex As Integer = 1
    Dim TotalPage = 0

    ' Variables para el autocompletado
    ' La cadena de selección
    ' los datos que traeremos de la base de datos.
    Private seleccion As String = "SELECT DISTINCT [Descripcion 1] AS descripcion FROM activo"
    Private selection As String = "SELECT DISTINCT [Usuario] AS nombre FROM activo"

    ' Variables necesarias para el botón de búsqueda
    Dim sNomCampo, strCriterio As String

    ' El adapatador para obtener los datos
    Private da As OleDb.OleDbDataAdapter
    ' El DataTable lo necesitamos a nivel del formulario
    Private dt As DataTable
    Private dtb As DataTable
    ' Para evitar re-entradas en el código
    Private iniciando As Boolean = True

    Private Sub frmActivos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Cursor = System.Windows.Forms.Cursors.WaitCursor

        'Código para el autocompletado
        da = New OleDb.OleDbDataAdapter(seleccion, cn)
        dt = New DataTable
        da.Fill(dt)

        'Código para el autocompletado
        da = New OleDb.OleDbDataAdapter(selection, cn)
        dtb = New DataTable
        da.Fill(dtb)

        Me.txtDescripcion.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtDescripcion, "Descripcion 1", "activo")

        iniciando = False

        FillAutoComplete() ' Código necesario para poder realizar el autocompletado

        ' Tipos de Bajas
        cboTipoDeBaja.Items.Clear()
        cboTipoDeBaja.Items.Add("No Asignados")
        cboTipoDeBaja.Items.Add("Ingresados por el Usuario")
        cboTipoDeBaja.Items.Add("Modificados por el Usuario")
        cboTipoDeBaja.Items.Add("Toda la Base de Datos")

        ' Aquí obtengo las empresas definidas en la base de datos
        Dim consulta As String = "SELECT * FROM empresa ORDER BY em_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "empresas")
        Dim NumRegistros As Byte = registro.Tables("empresas").Rows.Count
        ReDim VGCodEmpresa(NumRegistros)
        cboEmpresa.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 0 To NumRegistros - 1
                cboEmpresa.Items.Add(registro.Tables("empresas").Rows(i).Item("em_nombre"))
                VGCodEmpresa(i) = registro.Tables("empresas").Rows(i).Item("cod_empresa")
            Next i
            cboEmpresa.SelectedIndex = 0
        End If

        ' Aquí obtengo los tipos de activo definidos en la base de datos
        consulta = "SELECT * FROM tipo_activo ORDER BY descripcion"
        adaptador = New OleDb.OleDbDataAdapter(consulta, cn)
        adaptador.Fill(registro, "tipos_activo")
        NumRegistros = registro.Tables("tipos_activo").Rows.Count
        ReDim VGCodTipoActivo(NumRegistros)
        cboTipoActivo.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 0 To NumRegistros - 1
                cboTipoActivo.Items.Add(registro.Tables("tipos_activo").Rows(i).Item("descripcion"))
                VGCodTipoActivo(i) = registro.Tables("tipos_activo").Rows(i).Item("cod_tipo_activo")
            Next i
            cboTipoActivo.SelectedIndex = 0
        End If

        ' Aquí obtengo las ciudades definidas en la base de datos
        consulta = "SELECT * FROM ciudad ORDER BY ci_nombre"
        adaptador = New OleDb.OleDbDataAdapter(consulta, cn)
        adaptador.Fill(registro, "ciudad")
        NumRegistros = registro.Tables("ciudad").Rows.Count
        ReDim VGCodCiudad(NumRegistros)
        cboCiudad.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 1 To NumRegistros
                VGCodCiudad(i - 1) = registro.Tables("ciudad").Rows(i - 1).Item("cod_ciudad")
                cboCiudad.Items.Add(registro.Tables("ciudad").Rows(i - 1).Item("ci_nombre"))
            Next i
            cboCiudad.SelectedIndex = 0
        End If

        ' Aquí obtengo las situaciones de activo definidas en la base de datos
        consulta = "SELECT * FROM situacion ORDER BY cod_situacion"
        adaptador = New OleDb.OleDbDataAdapter(consulta, cn)
        adaptador.Fill(registro, "situacion")
        NumRegistros = registro.Tables("situacion").Rows.Count
        ReDim VGCodSituacionActivo(NumRegistros)
        cboSituacion.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 1 To NumRegistros
                VGCodSituacionActivo(i - 1) = registro.Tables("situacion").Rows(i - 1).Item("cod_situacion")
                cboSituacion.Items.Add(registro.Tables("situacion").Rows(i - 1).Item("descripcion"))
            Next i
            cboSituacion.SelectedIndex = 0
        End If

        ToolTip1.SetToolTip(btnFirstRecord, "Ir al primer activo")
        ToolTip1.SetToolTip(btnPrevRecord, "Ir al anterior activo")
        ToolTip1.SetToolTip(btnNextRecord, "Ir al siguiente activo")
        ToolTip1.SetToolTip(btnLastRecord, "Ir al último activo")

        ToolTip1.SetToolTip(btnFirstActivo, "Ir a la primera página")
        ToolTip1.SetToolTip(btnPrevActivo, "Ir a la página anterior")
        ToolTip1.SetToolTip(btnNextActivo, "Ir a la siguiente página")
        ToolTip1.SetToolTip(btnLastActivo, "Ir a la última página")

        CalcularTotales()

        grdActivos.DataSource = GetCurrentRecords(CurrentPageIndex, "").Tables(0).DefaultView

        'resize all columns' widths to fit the data
        For i = 0 To grdActivos.Columns.Count - 1
            grdActivos.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells)
        Next

        Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub CalculateTotalPages()
        Dim da As New OleDb.OleDbDataAdapter("SELECT COUNT(*) FROM activo WHERE cod_empresa = " & VGCodEmpresa(cboEmpresa.SelectedIndex), cn)
        Dim ds As New DataSet
        da.Fill(ds, "activos")
        Dim rowCount As Integer = ds.Tables(0).Rows(0).Item(0)
        TotalPage = rowCount / PgSize
        ' if any row left after calculated pages, add one more page 
        If (rowCount Mod PgSize > 0) Then TotalPage += 1
    End Sub

    Private Function GetCurrentRecords(ByVal page As Integer, ByVal criterio As String) As DataSet

        Sql = "SELECT activo.cod_activo, " & _
              "activo.[Codigo Original], " & _
              "activo.[Codigo Barras], " & _
              "tipo_activo.descripcion AS [Tipo de Activo], " & _
              "clase_activo.descripcion AS [Clase de Activo], " & _
              "activo.[Descripcion 1], " & _
              "activo.Marca, " & _
              "activo.Modelo, " & _
              "activo.[Serie / # Motor], " & _
              "activo.[# Chasis], " & _
              "activo.[# Placa], " & _
              "activo.Color, " & _
              "activo.Usuario, " & _
              "activo.[Cod Usuario], " & _
              "activo.Cargo, " & _
              "activo.[Area Fisica], " & _
              "activo.[Fec Ubic Activo], " & _
              "ciudad.ci_nombre AS Ciudad, " & _
              "sucursal.su_nombre AS Sucursal, " & _
              "departamento.de_nombre AS Departamento, " & _
              "situacion.descripcion AS Situacion, " & _
              "activo.[Fecha Compra], " & _
              "activo.[Valor Adquisicion], " & _
              "activo.[Valor Libros], " & _
              "activo.[Ingresado por el usuario], " & _
              "activo.[Modificado por el usuario], " & _
              "activo.Asignado, " & _
              "activo.[Periodo Depreciacion], " & _
              "activo.[Vida Util], " & _
              "activo.[Deprec/Men/Anual], " & _
              "activo.[Adicional 1], " & _
              "activo.[Adicional 2], " & _
              "activo.[Adicional 3], " & _
              "activo.[Adicional 4], " & _
              "activo.[Adicional 5], " & _
              "activo.[Adicional 6], " & _
              "activo.[Adicional 7], " & _
              "activo.[Adicional 8] " & _
              "FROM tipo_activo INNER JOIN (" & _
              "sucursal INNER JOIN (situacion INNER JOIN (departamento INNER JOIN (clase_activo INNER JOIN (ciudad INNER JOIN activo ON ciudad.cod_ciudad = activo.Ciudad) ON clase_activo.cod_clase_activo = activo.[Clase de Activo]) ON departamento.cod_departamento = activo.Departamento) ON situacion.cod_situacion = activo.[Situacion Activo]) ON (sucursal.cod_sucursal = activo.Sucursal) AND (ciudad.cod_ciudad = sucursal.cod_ciudad) AND (sucursal.cod_sucursal = departamento.cod_sucursal)) ON (tipo_activo.cod_tipo_activo = activo.[Tipo de Activo]) AND (tipo_activo.cod_tipo_activo = clase_activo.cod_tipo_activo) " & _
              "WHERE cod_empresa = " & VGCodEmpresa(cboEmpresa.SelectedIndex)

        If criterio <> "" Then
            Sql = Sql & " AND " & criterio
        End If

        Dim da As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds As New DataSet
        da.Fill(ds, "activos")

        Dim NumRegistros As Integer = ds.Tables("activos").Rows.Count
        lblTotalRegistros.Text = "Total de Registros Encontrados: " & CStr(NumRegistros)

        GetCurrentRecords = ds
    End Function

    Private Sub CalcularTotales()
        ' Calculo el total de registros
        Sql = "SELECT COUNT(*) AS [Total de Registros] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Dim da1 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds1 As New DataSet
        da1.Fill(ds1, "totales1")
        grdTotales1.DataSource = ds1.Tables(0).DefaultView
        ' Calculo la suma del valor de adquisicion
        Sql = "SELECT ROUND(SUM([Valor Adquisicion]),2) AS [Total Valor Adquisicion] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Dim da2 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds2 As New DataSet
        da2.Fill(ds2, "totales2")
        grdTotales2.DataSource = ds2.Tables(0).DefaultView
        ' Calculo el total de registros asignados
        Sql = "SELECT COUNT(*) AS [Total Registros Asignados] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Sql = Sql & " AND [Asignado]=TRUE"
        Dim da3 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds3 As New DataSet
        da3.Fill(ds3, "totales3")
        grdTotales3.DataSource = ds3.Tables(0).DefaultView
        ' Calculo la suma del valor de adquisicion de los activos asignados
        Sql = "SELECT SUM([Valor Adquisicion]) AS [Valor Adquisicion Asignados] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Sql = Sql & " AND [Asignado]=TRUE"
        Dim da4 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds4 As New DataSet
        da4.Fill(ds4, "totales4")
        grdTotales4.DataSource = ds4.Tables(0).DefaultView
        ' Calculo el total de registros no asignados
        Sql = "SELECT COUNT(*) AS [Total Registros No Asignados] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Sql = Sql & " AND [Asignado]=FALSE"
        Dim da5 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds5 As New DataSet
        da5.Fill(ds5, "totales5")
        grdTotales5.DataSource = ds5.Tables(0).DefaultView
        ' Calculo la suma del valor de adquisicion de los activos no asignados
        Sql = "SELECT SUM([Valor Adquisicion]) AS [Valor Adquisicion No Asignados] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Sql = Sql & " AND [Asignado]=FALSE"
        Dim da6 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds6 As New DataSet
        da6.Fill(ds6, "totales6")
        grdTotales6.DataSource = ds6.Tables(0).DefaultView
        ' Calculo el total de registros ingresados por el usuario
        Sql = "SELECT COUNT(*) AS [Registros Ingresados por el Usuario] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Sql = Sql & " AND [Ingresado por el usuario]=TRUE"
        Dim da7 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds7 As New DataSet
        da7.Fill(ds7, "totales7")
        grdTotales7.DataSource = ds7.Tables(0).DefaultView
        ' Calculo la suma del valor de adquisicion de los activos ingresados por el usuario
        Sql = "SELECT SUM([Valor Adquisicion]) AS [Valor Adquisicion Ingresados por el Usuario] FROM activo WHERE cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))
        Sql = Sql & " AND [Ingresado por el usuario]=TRUE"
        Dim da8 As New OleDb.OleDbDataAdapter(Sql, cn)
        Dim ds8 As New DataSet
        da8.Fill(ds8, "totales8")
        grdTotales8.DataSource = ds8.Tables(0).DefaultView
    End Sub

    Private Sub FillAutoComplete()
        ' Esto es para la función de autocompletado
        Me.txtCodigoOriginal.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtCodigoOriginal, "Codigo Original", "activo")
        Me.txtCodigoBarras.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtCodigoBarras, "Codigo Barras", "activo")

        Me.txtMarca.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtMarca, "Marca", "activo")
        Me.txtModelo.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtModelo, "Modelo", "activo")
        Me.txtNroMotor.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtNroMotor, "Serie / # Motor", "activo")
        Me.txtChasis.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtChasis, "# Chasis", "activo")
        Me.txtPlaca.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtPlaca, "# Placa", "activo")
        Me.txtColor.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtColor, "Color", "activo")

        Me.txtColor.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtColor, "Usuario", "activo")
        Me.txtColor.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtColor, "Cod Usuario", "activo")

        Me.txtCargo.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtCargo, "Cargo", "activo")
        Me.txtAreaFisica.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAreaFisica, "Area Fisica", "activo")
        Me.txtValorAdquisicion.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtValorAdquisicion, "Valor Adquisicion", "activo")
        Me.txtValorLibros.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtValorLibros, "Valor Libros", "activo")
        Me.txtPeriodoDepreciacion.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtPeriodoDepreciacion, "Periodo Depreciacion", "activo")
        Me.txtVidaUtil.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtVidaUtil, "Vida Util", "activo")
        Me.txtDepreciacion.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtDepreciacion, "Deprec/Men/Anual", "activo")
        Me.txtAdicional1.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional1, "Adicional 1", "activo")
        Me.txtAdicional2.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional2, "Adicional 2", "activo")
        Me.txtAdicional3.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional3, "Adicional 3", "activo")
        Me.txtAdicional4.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional4, "Adicional 4", "activo")
        Me.txtAdicional5.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional5, "Adicional 5", "activo")
        Me.txtAdicional6.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional6, "Adicional 6", "activo")
        Me.txtAdicional7.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional7, "Adicional 7", "activo")
        Me.txtAdicional8.AutoCompleteCustomSource = autoCompletarTextBox(Me.txtAdicional8, "Adicional 8", "activo")
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cboTipoActivo_GotFocus(sender As Object, e As EventArgs) Handles cboTipoActivo.GotFocus
        sNomCampo = "Tipo de Activo"
    End Sub

    Private Sub cboTipoActivo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoActivo.SelectedIndexChanged
        FPGetClasesActivo()
    End Sub

    Private Sub FPGetClasesActivo()
        Dim consulta As String = "SELECT * FROM clase_activo WHERE cod_tipo_activo = " & VGCodTipoActivo(cboTipoActivo.SelectedIndex) & " ORDER BY descripcion"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "clase_activo")
        Dim NumRegistros As Byte = registro.Tables("clase_activo").Rows.Count
        If NumRegistros = 0 Then
            MsgBox("No existen Clases de Activo asociadas al Tipo de Activo seleccionado...")
        Else
            ' mapeo los registros encontrados
            ReDim VGCodClaseActivo(NumRegistros)
            cboClaseActivo.Items.Clear()
            For i = 1 To NumRegistros
                VGCodClaseActivo(i - 1) = registro.Tables("clase_activo").Rows(i - 1).Item("cod_clase_activo")
                cboClaseActivo.Items.Add(registro.Tables("clase_activo").Rows(i - 1).Item("descripcion"))
            Next i
        End If
        cboClaseActivo.SelectedIndex = 0
    End Sub

    Private Sub cboCiudad_GotFocus(sender As Object, e As EventArgs) Handles cboCiudad.GotFocus
        sNomCampo = "Ciudad"
    End Sub

    Private Sub cboCiudad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCiudad.SelectedIndexChanged
        FPGetSucursales()
    End Sub

    Private Sub FPGetSucursales()
        Dim consulta As String = "SELECT * FROM sucursal WHERE cod_ciudad = " & VGCodCiudad(cboCiudad.SelectedIndex) & " ORDER BY su_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "sucursal")
        Dim NumRegistros As Byte = registro.Tables("sucursal").Rows.Count
        If NumRegistros = 0 Then
            MsgBox("No existen Sucursales asociadas a la Ciudad seleccionada...")
        Else
            ' mapeo los registros encontrados
            ReDim VGCodSucursal(NumRegistros)
            cboSucursal.Items.Clear()
            For i = 1 To NumRegistros
                VGCodSucursal(i - 1) = registro.Tables("sucursal").Rows(i - 1).Item("cod_sucursal")
                cboSucursal.Items.Add(registro.Tables("sucursal").Rows(i - 1).Item("su_nombre"))
            Next i
        End If
        cboSucursal.SelectedIndex = 0
    End Sub

    Private Sub cboSucursal_GotFocus(sender As Object, e As EventArgs) Handles cboSucursal.GotFocus
        sNomCampo = "Sucursal"
    End Sub

    Private Sub cboSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSucursal.SelectedIndexChanged
        FPGetDepartamentos()
    End Sub

    Private Sub FPGetDepartamentos()
        Dim consulta As String = "SELECT * FROM departamento WHERE cod_sucursal = " & VGCodSucursal(cboSucursal.SelectedIndex) & " ORDER BY de_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "departamento")
        Dim NumRegistros As Byte = registro.Tables("departamento").Rows.Count
        If NumRegistros = 0 Then
            MsgBox("No existen Departamentos asociados a la Sucursal seleccionada...")
        Else
            ' mapeo los registros encontrados
            ReDim VGCodDepartamento(NumRegistros)
            cboDepartamento.Items.Clear()
            For i = 1 To NumRegistros
                VGCodDepartamento(i - 1) = registro.Tables("departamento").Rows(i - 1).Item("cod_departamento")
                cboDepartamento.Items.Add(registro.Tables("departamento").Rows(i - 1).Item("de_nombre"))
            Next i
        End If
        cboDepartamento.SelectedIndex = 0
    End Sub

    Private Sub cboDepartamento_GotFocus(sender As Object, e As EventArgs) Handles cboDepartamento.GotFocus
        sNomCampo = "Departamento"
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If lblCodActivo.Text = 0 Then
            ' Es un registro nuevo
            FPIngresar()
        Else
            ' Es un registro ingresado
            FPModificar()
        End If
    End Sub

    Private Sub FPIngresar()
        ' Procedimiento para ingresar un nuevo registro en la tabla activo
        If Trim(txtCodigoOriginal.Text) <> "" Then
            If FPBuscarCampo("activo", "[Codigo Original]", Trim(txtCodigoOriginal.Text)) Then
                MsgBox("Ya existe el valor de Codigo Original " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
                txtCodigoOriginal.Focus()
            End If
        ElseIf Trim(txtCodigoBarras.Text) = "" Then
            MsgBox("El campo Codigo Barras es obligatorio.")
            chkIngUsuario.Checked = False
            txtCodigoBarras.Focus()
            ' Comprobar que no se repita el codigo
        ElseIf FPBuscarCampo("activo", "[Codigo Barras]", Trim(txtCodigoBarras.Text)) Then
            MsgBox("El Codigo Barras ya se encuentra ingresado.")
            chkIngUsuario.Checked = False
            txtCodigoBarras.Focus()
        ElseIf Trim(txtDescripcion.Text) = "" Then
            MsgBox("El campo Descripcion es obligatorio.")
            chkIngUsuario.Checked = False
            txtDescripcion.Focus()
        ElseIf Trim(txtMarca.Text) = "" Then
            MsgBox("El campo Marca es obligatorio.")
            chkIngUsuario.Checked = False
            txtMarca.Focus()
        ElseIf Trim(txtModelo.Text) = "" Then
            MsgBox("El campo Modelo es obligatorio.")
            chkIngUsuario.Checked = False
            txtModelo.Focus()
        ElseIf Trim(txtNroMotor.Text) = "" Then
            MsgBox("El campo Serie / #Motor es obligatorio.")
            chkIngUsuario.Checked = False
            txtNroMotor.Focus()
        ElseIf Trim(txtUsuario.Text) = "" Then
            MsgBox("El campo Usuario es obligatorio.")
            chkIngUsuario.Checked = False
            txtUsuario.Focus()
        ElseIf Trim(txtAreaFisica.Text) = "" Then
            MsgBox("El campo Area Fisica es obligatorio.")
            chkIngUsuario.Checked = False
            txtAreaFisica.Focus()
        Else
            VGStrSQL$ =
                "insert into activo (" & _
                        "cod_empresa, " & _
                        "[Codigo Original], " & _
                        "[Codigo Barras], " & _
                        "[Tipo de Activo], " & _
                        "[Clase de Activo], " & _
                        "[Descripcion 1], " & _
                        "Marca, " & _
                        "Modelo, " & _
                        "[Serie / # Motor], " & _
                        "[# Chasis], " & _
                        "[# Placa], " & _
                        "Color, " & _
                        "Usuario, " & _
                        "[Cod Usuario], " & _
                        "Cargo, " & _
                        "[Area Fisica], " & _
                        "[Fec Ubic Activo], " & _
                        "Ciudad, " & _
                        "Sucursal, " & _
                        "Departamento, " & _
                        "[Situacion Activo], " & _
                        "[Fecha Compra], " & _
                        "[Valor Adquisicion], " & _
                        "[Valor Libros], " & _
                        "[Ingresado por el usuario], " & _
                        "[Modificado por el usuario], " & _
                        "Asignado, " & _
                        "[Periodo Depreciacion], " & _
                        "[Vida Util], " & _
                        "[Deprec/Men/Anual], " & _
                        "[Adicional 1], " & _
                        "[Adicional 2], " & _
                        "[Adicional 3], " & _
                        "[Adicional 4], " & _
                        "[Adicional 5], " & _
                        "[Adicional 6], " & _
                        "[Adicional 7], " & _
                        "[Adicional 8], " & _
                        "[Fecha de Modificacion])" & _
                        " VALUES("
            'cod_empresa
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex)) & ", "
            'Codigo Original
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtCodigoOriginal.Text) & "', "
            'Codigo Barras
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtCodigoBarras.Text) & "', "
            'Tipo de Activo
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodTipoActivo(cboTipoActivo.SelectedIndex)) & ", "
            'Clase de Activo
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodClaseActivo(cboClaseActivo.SelectedIndex)) & ", "
            'Descripcion
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtDescripcion.Text) & "', "
            'Marca
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtMarca.Text) & "', "
            'Modelo
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtModelo.Text) & "', "
            'Serie / #Motor
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtNroMotor.Text) & "', "
            '#Chasis
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtChasis.Text) & "', "
            'Placa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtPlaca.Text) & "', "
            'Color
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtColor.Text) & "', "
            'Usuario
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtUsuario.Text) & "', "
            'Cod Usuario
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtCodUsuario.Text) & "', "
            'Cargo
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtCargo.Text) & "', "
            'Area Fisica
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(txtAreaFisica.Text) & "', "
            'Fec Ubic Activo
            VGStrSQL$ = VGStrSQL$ & "#" & Trim(txtFecUbicActivo.Text) & "#, "
            'Ciudad
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodCiudad(cboCiudad.SelectedIndex)) & ", "
            'Sucursal
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodSucursal(cboSucursal.SelectedIndex)) & ", "
            'Departamento
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodDepartamento(cboDepartamento.SelectedIndex)) & ", "
            'Situacion de Activo
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodSituacionActivo(cboSituacion.SelectedIndex)) & ", "
            'Fecha de Compra 
            VGStrSQL$ = VGStrSQL$ & "#" & Trim(txtFechaCompra.Text) & "#, "
            'Valor Adquisicion
            If Trim(txtValorAdquisicion.Text) = "" Then
                VGStrSQL$ = VGStrSQL$ & "0, '"
            Else
                VGStrSQL$ = VGStrSQL$ & Trim(FPCambiarComaAPunto(txtValorAdquisicion.Text)) & ", '"
            End If
            'Valor Libros
            VGStrSQL$ = VGStrSQL$ & Trim(FPCambiarComaAPunto(txtValorLibros.Text)) & "'"
            'Ingresado por el usuario
            VGStrSQL$ = VGStrSQL$ & ", true"
            'Modificado por el usuario
            VGStrSQL$ = VGStrSQL$ & ", false"
            'Asignado
            VGStrSQL$ = VGStrSQL$ & ", false"
            ' Periodo Depreciacion
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtPeriodoDepreciacion.Text)) & "'"
            ' Vida Util
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtVidaUtil.Text)) & "'"
            ' Deprec/Men/Anual
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtDepreciacion.Text)) & "'"
            ' Adicional 1
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional1.Text)) & "'"
            ' Adicional 2
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional2.Text)) & "'"
            ' Adicional 3
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional3.Text)) & "'"
            ' Adicional 4
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional4.Text)) & "'"
            ' Adicional 5
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional5.Text)) & "'"
            ' Adicional 6
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional6.Text)) & "'"
            ' Adicional 7
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional7.Text)) & "'"
            ' Adicional 8
            VGStrSQL$ = VGStrSQL$ & ",'" & Trim(FPCambiarComaAPunto(txtAdicional8.Text)) & "'"
            ' Fecha de modificación
            VGStrSQL$ = VGStrSQL$ & ",#" & Trim(Format(Today, "dd/MM/yyyy")) & "#)"

            Dim comando As New OleDb.OleDbCommand(VGStrSQL$, cn)
            comando.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...

            MsgBox("Registro insertado exitosamente.", vbInformation, "Ingreso de Activos")

            FPLimpiar()

            'Código para el autocompletado
            da = New OleDb.OleDbDataAdapter(seleccion, cn)
            dt = New DataTable
            da.Fill(dt)

            FillAutoComplete() ' Código necesario para poder realizar el autocompletado

            CalcularTotales()

            grdActivos.DataSource = GetCurrentRecords(CurrentPageIndex, "").Tables(0).DefaultView

            'resize all columns' widths to fit the data
            For i = 0 To grdActivos.Columns.Count - 1
                grdActivos.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells)
            Next

        End If

    End Sub

    Private Sub FPModificar()
        ' Procedimiento para actualizar un registro existente en la tabla activo
        If Trim(txtCodigoBarras.Text) = "" Then
            MsgBox("El campo Codigo Barras es obligatorio.")
            chkModUsuario.Checked = False
            txtCodigoBarras.Focus()
        ElseIf Trim(txtDescripcion.Text) = "" Then
            MsgBox("El campo Descripcion es obligatorio.")
            chkModUsuario.Checked = False
            txtDescripcion.Focus()
        ElseIf Trim(txtMarca.Text) = "" Then
            MsgBox("El campo Marca es obligatorio.")
            chkIngUsuario.Checked = False
            txtMarca.Focus()
        ElseIf Trim(txtModelo.Text) = "" Then
            MsgBox("El campo Modelo es obligatorio.")
            chkIngUsuario.Checked = False
            txtModelo.Focus()
        ElseIf Trim(txtNroMotor.Text) = "" Then
            MsgBox("El campo Serie / #Motor es obligatorio.")
            chkIngUsuario.Checked = False
            txtNroMotor.Focus()
        ElseIf Trim(txtUsuario.Text) = "" Then
            MsgBox("El campo Usuario es obligatorio.")
            chkIngUsuario.Checked = False
            txtUsuario.Focus()
        ElseIf Trim(txtAreaFisica.Text) = "" Then
            MsgBox("El campo Area Fisica es obligatorio.")
            chkModUsuario.Checked = False
            txtAreaFisica.Focus()
        Else
            VGStrSQL$ = "update activo set "
            'Codigo Original
            VGStrSQL$ = VGStrSQL$ & "[Codigo Original] = '" & Trim(txtCodigoOriginal.Text) & "', "
            'Codigo Barras
            VGStrSQL$ = VGStrSQL$ & "[Codigo Barras] = '" & Trim(txtCodigoBarras.Text) & "', "
            'Tipo de Activo
            VGStrSQL$ = VGStrSQL$ & "[Tipo de Activo] = " & Trim(VGCodTipoActivo(cboTipoActivo.SelectedIndex)) & ", "
            'Clase de Activo
            VGStrSQL$ = VGStrSQL$ & "[Clase de Activo] = " & Trim(VGCodClaseActivo(cboClaseActivo.SelectedIndex)) & ", "
            'Descripcion
            VGStrSQL$ = VGStrSQL$ & "[Descripcion 1] = '" & Trim(txtDescripcion.Text) & "', "
            'Marca
            VGStrSQL$ = VGStrSQL$ & "[Marca] = '" & Trim(txtMarca.Text) & "', "
            'Modelo
            VGStrSQL$ = VGStrSQL$ & "[Modelo] = '" & Trim(txtModelo.Text) & "', "
            'Serie / # Motor
            VGStrSQL$ = VGStrSQL$ & "[Serie / # Motor] = '" & Trim(txtNroMotor.Text) & "', "
            '# Chasis
            VGStrSQL$ = VGStrSQL$ & "[# Chasis] = '" & Trim(txtChasis.Text) & "', "
            '# Placa
            VGStrSQL$ = VGStrSQL$ & "[# Placa] = '" & Trim(txtPlaca.Text) & "', "
            'Color
            VGStrSQL$ = VGStrSQL$ & "[Color] = '" & Trim(txtColor.Text) & "', "
            'Usuario
            VGStrSQL$ = VGStrSQL$ & "Usuario = '" & Trim(txtUsuario.Text) & "', "
            'Cod Usuario
            VGStrSQL$ = VGStrSQL$ & "[Cod Usuario] = '" & Trim(txtCodUsuario.Text) & "', "
            'Cargo
            VGStrSQL$ = VGStrSQL$ & "[Cargo] = '" & Trim(txtCargo.Text) & "', "
            'Area Fisica
            VGStrSQL$ = VGStrSQL$ & "[Area Fisica] = '" & Trim(txtAreaFisica.Text) & "', "
            'Fec Ubic Activo
            VGStrSQL$ = VGStrSQL$ & "[Fec Ubic Activo] = #" & Trim(txtFecUbicActivo.Text) & "#, "
            'Ciudad
            VGStrSQL$ = VGStrSQL$ & "[Ciudad] = " & Trim(VGCodCiudad(cboCiudad.SelectedIndex)) & ", "
            'Sucursal
            VGStrSQL$ = VGStrSQL$ & "[Sucursal] = " & Trim(VGCodSucursal(cboSucursal.SelectedIndex)) & ", "
            'Departamento
            VGStrSQL$ = VGStrSQL$ & "[Departamento] = " & Trim(VGCodDepartamento(cboDepartamento.SelectedIndex)) & ", "
            'Situacion Activo
            VGStrSQL$ = VGStrSQL$ & "[Situacion Activo] = " & Trim(VGCodSituacionActivo(cboSituacion.SelectedIndex)) & ", "
            'Fecha Compra 
            VGStrSQL$ = VGStrSQL$ & "[Fecha Compra] = #" & Trim(txtFechaCompra.Text) & "#, "
            'Valor Adquisicion
            If Trim(txtValorAdquisicion.Text) = "" Then
                VGStrSQL$ = VGStrSQL$ & "[Valor Adquisicion] = 0, "
            Else
                VGStrSQL$ = VGStrSQL$ & "[Valor Adquisicion] = " & Trim(FPCambiarComaAPunto(txtValorAdquisicion.Text)) & ", "
            End If
            'Valor Libros
            VGStrSQL$ = VGStrSQL$ & "[Valor Libros] = '" & Trim(FPCambiarComaAPunto(txtValorLibros.Text)) & "', "
            'Modificado por el usuario
            VGStrSQL$ = VGStrSQL$ & "[Modificado por el usuario] = true, "
            'Asignado
            VGStrSQL$ = VGStrSQL$ & "[Asignado] =  true, "
            ' Periodo Depreciacion
            VGStrSQL$ = VGStrSQL$ & "[Periodo Depreciacion] = '" & Trim(FPCambiarComaAPunto(txtPeriodoDepreciacion.Text)) & "', "
            ' Vida Util
            VGStrSQL$ = VGStrSQL$ & "[Vida Util] = '" & Trim(FPCambiarComaAPunto(txtVidaUtil.Text)) & "', "
            ' Deprec/Men/Anual
            VGStrSQL$ = VGStrSQL$ & "[Deprec/Men/Anual] = '" & Trim(FPCambiarComaAPunto(txtDepreciacion.Text)) & "', "
            ' Adicional 1
            VGStrSQL$ = VGStrSQL$ & "[Adicional 1] = '" & Trim(FPCambiarComaAPunto(txtAdicional1.Text)) & "', "
            ' Adicional 2
            VGStrSQL$ = VGStrSQL$ & "[Adicional 2] = '" & Trim(FPCambiarComaAPunto(txtAdicional2.Text)) & "', "
            ' Adicional 3
            VGStrSQL$ = VGStrSQL$ & "[Adicional 3] = '" & Trim(FPCambiarComaAPunto(txtAdicional3.Text)) & "', "
            ' Adicional 4
            VGStrSQL$ = VGStrSQL$ & "[Adicional 4] = '" & Trim(FPCambiarComaAPunto(txtAdicional4.Text)) & "', "
            ' Adicional 5
            VGStrSQL$ = VGStrSQL$ & "[Adicional 5] = '" & Trim(FPCambiarComaAPunto(txtAdicional5.Text)) & "', "
            ' Adicional 6
            VGStrSQL$ = VGStrSQL$ & "[Adicional 6] = '" & Trim(FPCambiarComaAPunto(txtAdicional6.Text)) & "', "
            ' Adicional 7
            VGStrSQL$ = VGStrSQL$ & "[Adicional 7] = '" & Trim(FPCambiarComaAPunto(txtAdicional7.Text)) & "', "
            ' Adicional 8
            VGStrSQL$ = VGStrSQL$ & "[Adicional 8] = '" & Trim(FPCambiarComaAPunto(txtAdicional8.Text)) & "', "
            ' Fecha de modificación
            VGStrSQL$ = VGStrSQL$ & "[Fecha de Modificacion] = #" & Trim(Format(Today, "dd/MM/yyyy")) & "#"

            'cod_activo
            VGStrSQL$ = VGStrSQL$ & " WHERE cod_activo = " & Trim(Me.lblCodActivo.Text)
            'cod_empresa
            VGStrSQL$ = VGStrSQL$ & " AND cod_empresa = " & Trim(VGCodEmpresa(cboEmpresa.SelectedIndex))

            Dim comando As New OleDb.OleDbCommand(VGStrSQL$, cn)
            comando.ExecuteNonQuery() 'Si no hay errores se ejecutará la consulta...

            MsgBox("Registro actualizado exitosamente.", vbInformation, "Actualización de Activos")

            FPLimpiar()

            'Código para el autocompletado
            da = New OleDb.OleDbDataAdapter(seleccion, cn)
            dt = New DataTable
            da.Fill(dt)

            FillAutoComplete() ' Código necesario para poder realizar el autocompletado

            CalcularTotales()

            grdActivos.DataSource = GetCurrentRecords(CurrentPageIndex, "").Tables(0).DefaultView

            'resize all columns' widths to fit the data
            For i = 0 To grdActivos.Columns.Count - 1
                grdActivos.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells)
            Next

        End If

    End Sub

    Private Sub grdActivos_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles grdActivos.DataBindingComplete
        'set color to the rows acording a criteria
        For i = 0 To grdActivos.Rows.Count - 1
            If Not grdActivos.Rows(i).DataBoundItem("Asignado") Then
                grdActivos.Rows(i).DefaultCellStyle.ForeColor = System.Drawing.Color.Red     'Rojo
            End If
            If grdActivos.Rows(i).DataBoundItem("Ingresado por el usuario") Then
                grdActivos.Rows(i).DefaultCellStyle.ForeColor = System.Drawing.Color.Black   'Negro
            End If
            If grdActivos.Rows(i).DataBoundItem("Modificado por el usuario") Then
                grdActivos.Rows(i).DefaultCellStyle.ForeColor = System.Drawing.Color.Blue    'Azul
            End If
        Next
        'Mostrar el número de filas de un DataGridView..
        If grdActivos.Rows.Count > 0 Then
            For r As Integer = 0 To grdActivos.Rows.Count - 1
                Me.grdActivos.Rows(r).HeaderCell.Value = (r + 1).ToString()
            Next
        End If
        'ocultar la columna que contiene el código del activo
        Me.grdActivos.Columns("cod_activo").Visible = False
    End Sub

    Private Sub grdActivos_DoubleClick(sender As Object, e As EventArgs) Handles grdActivos.DoubleClick
        VGCodActivoSel = grdActivos.CurrentRow.Cells(0).Value
        lblCodActivo.Text = VGCodActivoSel
        FillFields(VGCodActivoSel)
        chkModUsuario.Enabled = True
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ' Procedimiento para buscar y desplegar los registros asociados
        If cboEmpresa.Text <> "" Then
            lstResultado.Visible = False
            If sNomCampo = "Codigo Original" Or _
                   sNomCampo = "Codigo Barras" Or _
                   sNomCampo = "Tipo de Activo" Or _
                   sNomCampo = "Clase de Activo" Or _
                   sNomCampo = "Descripcion 1" Or _
                   sNomCampo = "Marca" Or _
                   sNomCampo = "Modelo" Or _
                   sNomCampo = "Serie / # Motor" Or _
                   sNomCampo = "# Chasis" Or _
                   sNomCampo = "# Placa" Or _
                   sNomCampo = "Color" Or _
                   sNomCampo = "Usuario" Or _
                   sNomCampo = "Cod Usuario" Or _
                   sNomCampo = "Area Fisica" Or _
                   sNomCampo = "Departamento" Or _
                   sNomCampo = "Ciudad" Or _
                   sNomCampo = "Fecha Compra" Or _
                   sNomCampo = "Adicional 1" Or _
                   sNomCampo = "Adicional 2" Or _
                   sNomCampo = "Adicional 3" Or _
                   sNomCampo = "Adicional 4" Or _
                   sNomCampo = "Adicional 5" Or _
                   sNomCampo = "Adicional 6" Or _
                   sNomCampo = "Adicional 7" Or _
                   sNomCampo = "Adicional 8" Then
                Select Case sNomCampo
                    Case "Codigo Original"
                        strCriterio = txtCodigoOriginal.Text
                    Case "Codigo Barras"
                        strCriterio = txtCodigoBarras.Text
                    Case "Tipo de Activo"
                        strCriterio = CStr(VGCodTipoActivo(cboTipoActivo.SelectedIndex))
                    Case "Clase de Activo"
                        strCriterio = CStr(VGCodClaseActivo(cboClaseActivo.SelectedIndex))
                    Case "Descripcion 1"
                        strCriterio = txtDescripcion.Text
                    Case "Marca"
                        strCriterio = txtMarca.Text
                    Case "Modelo"
                        strCriterio = txtModelo.Text
                    Case "Serie / # Motor"
                        strCriterio = txtNroMotor.Text
                    Case "# Chasis"
                        strCriterio = txtChasis.Text
                    Case "# Placa"
                        strCriterio = txtPlaca.Text
                    Case "Color"
                        strCriterio = txtColor.Text
                    Case "Usuario"
                        strCriterio = txtUsuario.Text
                    Case "Cod Usuario"
                        strCriterio = txtCodUsuario.Text
                    Case "Area Fisica"
                        strCriterio = txtAreaFisica.Text
                    Case "Departamento"
                        strCriterio = CStr(VGCodDepartamento(cboDepartamento.SelectedIndex))
                    Case "Ciudad"
                        strCriterio = CStr(VGCodCiudad(cboCiudad.SelectedIndex))
                    Case "Fecha Compra"
                        strCriterio = txtFechaCompra.Text
                    Case "Adicional 1"
                        strCriterio = txtAdicional1.Text
                    Case "Adicional 2"
                        strCriterio = txtAdicional2.Text
                    Case "Adicional 3"
                        strCriterio = txtAdicional3.Text
                    Case "Adicional 4"
                        strCriterio = txtAdicional4.Text
                    Case "Adicional 5"
                        strCriterio = txtAdicional5.Text
                    Case "Adicional 6"
                        strCriterio = txtAdicional6.Text
                    Case "Adicional 7"
                        strCriterio = txtAdicional7.Text
                    Case "Adicional 8"
                        strCriterio = txtAdicional8.Text
                End Select
                If strCriterio <> "" Then
                    ' Se procede a la búsqueda
                    If sNomCampo = "Tipo de Activo" Or sNomCampo = "Clase de Activo" Or sNomCampo = "Departamento" Or sNomCampo = "Ciudad" Then
                        strCriterio = "[" & Trim(sNomCampo) & "] = " & Trim(strCriterio)
                    Else
                        strCriterio = "[" & Trim(sNomCampo) & "] LIKE '%" & Trim(strCriterio) & "%'"
                    End If
                    Cursor = System.Windows.Forms.Cursors.WaitCursor
                    CurrentPageIndex = 1
                    grdActivos.DataSource = GetCurrentRecords(CurrentPageIndex, strCriterio).Tables(0).DefaultView

                    'resize all columns' widths to fit the data
                    For i = 0 To grdActivos.Columns.Count - 1
                        grdActivos.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells)
                    Next

                    Cursor = System.Windows.Forms.Cursors.Default
                End If
            End If
        End If
    End Sub

    Private Sub txtCodigoOriginal_GotFocus(sender As Object, e As EventArgs) Handles txtCodigoOriginal.GotFocus
        sNomCampo = "Codigo Original"
    End Sub

    Private Sub txtCodigoBarras_GotFocus(sender As Object, e As EventArgs) Handles txtCodigoBarras.GotFocus
        sNomCampo = "Codigo Barras"
    End Sub

    Private Sub cboClaseActivo_GotFocus(sender As Object, e As EventArgs) Handles cboClaseActivo.GotFocus
        sNomCampo = "Clase de Activo"
    End Sub

    Private Sub txtDescripcion_GotFocus(sender As Object, e As EventArgs) Handles txtDescripcion.GotFocus
        sNomCampo = "Descripcion 1"
    End Sub

    Private Sub txtMarca_GotFocus(sender As Object, e As EventArgs) Handles txtMarca.GotFocus
        sNomCampo = "Marca"
    End Sub

    Private Sub txtModelo_GotFocus(sender As Object, e As EventArgs) Handles txtModelo.GotFocus
        sNomCampo = "Modelo"
    End Sub

    Private Sub txtNroMotor_GotFocus(sender As Object, e As EventArgs) Handles txtNroMotor.GotFocus
        sNomCampo = "Serie / # Motor"
    End Sub

    Private Sub txtChasis_GotFocus(sender As Object, e As EventArgs) Handles txtChasis.GotFocus
        sNomCampo = "# Chasis"
    End Sub

    Private Sub txtPlaca_GotFocus(sender As Object, e As EventArgs) Handles txtPlaca.GotFocus
        sNomCampo = "# Placa"
    End Sub

    Private Sub txtColor_DragOver(sender As Object, e As DragEventArgs) Handles txtColor.DragOver
        sNomCampo = "Color"
    End Sub

    Private Sub txtUsuario_GotFocus(sender As Object, e As EventArgs) Handles txtUsuario.GotFocus
        sNomCampo = "Usuario"
    End Sub

    Private Sub txtCodUsuario_GotFocus(sender As Object, e As EventArgs) Handles txtCodUsuario.GotFocus
        sNomCampo = "Cod Usuario"
    End Sub

    Private Sub txtAreaFisica_GotFocus(sender As Object, e As EventArgs) Handles txtAreaFisica.GotFocus
        sNomCampo = "Area Fisica"
    End Sub

    Private Sub txtFechaCompra_GotFocus(sender As Object, e As EventArgs) Handles txtFechaCompra.GotFocus
        sNomCampo = "Fecha Compra"
    End Sub

    Private Sub txtAdicional1_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional1.GotFocus
        sNomCampo = "Adicional 1"
    End Sub

    Private Sub txtAdicional2_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional2.GotFocus
        sNomCampo = "Adicional 2"
    End Sub

    Private Sub txtAdicional3_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional3.GotFocus
        sNomCampo = "Adicional 3"
    End Sub

    Private Sub txtAdicional4_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional4.GotFocus
        sNomCampo = "Adicional 4"
    End Sub

    Private Sub txtAdicional5_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional5.GotFocus
        sNomCampo = "Adicional 5"
    End Sub

    Private Sub txtAdicional6_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional6.GotFocus
        sNomCampo = "Adicional 6"
    End Sub

    Private Sub txtAdicional7_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional7.GotFocus
        sNomCampo = "Adicional 7"
    End Sub

    Private Sub txtAdicional8_GotFocus(sender As Object, e As EventArgs) Handles txtAdicional8.GotFocus
        sNomCampo = "Adicional 8"
    End Sub

    Private Sub btnBackup_Click(sender As Object, e As EventArgs) Handles btnBackup.Click
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.Title = "Respaldar la Base de Datos"
        OpenFileDialog.InitialDirectory = Application.StartupPath
        OpenFileDialog.Filter = "Microsoft Access 2003 (*.mdb)|*.mdb|Microsoft Access 2007 (*.accdb)|*.accdb|Todos los archivos (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim extension As String = System.IO.Path.GetExtension(OpenFileDialog.FileName)
            If extension = ".mdb" Or extension = ".accdb" Then
                Dim nombreOriginal As String = System.IO.Path.GetFileName(OpenFileDialog.FileName)
                Dim fecha As String = DateString
                Dim aleatorio As Integer = CInt(Int((999999 * Rnd()) + 1))
                Dim nombreFinal As String = aleatorio & "-" & fecha & "-" & nombreOriginal
                System.IO.File.Copy(OpenFileDialog.FileName, nombreFinal)
                MsgBox("Nombre del archivo generado: " & nombreFinal)
            Else
                MsgBox("El formato es incorrecto")
            End If
        Else
            MsgBox("Ud. no ha seleccionado ningún archivo...")
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub btnReporte_Click(sender As Object, e As EventArgs) Handles btnReporte.Click
        If cboEmpresa.Text <> "" Then
            If sNomCampo = "Descripcion 1" Or _
                   sNomCampo = "Tipo de Activo" Or _
                   sNomCampo = "Clase de Activo" Or _
                   sNomCampo = "Marca" Or _
                   sNomCampo = "Usuario" Or _
                   sNomCampo = "Area Fisica" Or _
                   sNomCampo = "Departamento" Or _
                   sNomCampo = "Ciudad" Or _
                   sNomCampo = "Situacion Activo" Or _
                   sNomCampo = "Adicional 1" Then
                Select Case sNomCampo
                    Case "Tipo de Activo"
                        strCriterio = CStr(VGCodTipoActivo(cboTipoActivo.SelectedIndex))
                    Case "Clase de Activo"
                        strCriterio = CStr(VGCodClaseActivo(cboClaseActivo.SelectedIndex))
                    Case "Descripcion 1"
                        strCriterio = txtDescripcion.Text
                    Case "Marca"
                        strCriterio = txtMarca.Text
                    Case "Usuario"
                        strCriterio = CStr(sCodUsuario)
                    Case "Area Fisica"
                        strCriterio = txtAreaFisica.Text
                    Case "Departamento"
                        strCriterio = CStr(VGCodDepartamento(cboDepartamento.SelectedIndex))
                    Case "Ciudad"
                        strCriterio = CStr(VGCodCiudad(cboCiudad.SelectedIndex))
                    Case "Situacion Activo"
                        strCriterio = CStr(VGCodSituacionActivo(cboSituacion.SelectedIndex))
                    Case "Adicional 1"
                        strCriterio = txtAdicional1.Text
                End Select

                If Trim(strCriterio) <> "" Then
                    basMain.sNomCampo = sNomCampo
                    basMain.strCriterio = strCriterio
                    basMain.VGCodEmpresaSel = CStr(VGCodEmpresa(cboEmpresa.SelectedIndex))
                    frmReporte.ShowDialog()
                Else
                    MsgBox("El criterio de búsqueda no puede ser nulo.")
                End If

            End If
        End If
    End Sub

    Private Sub btnNotas_Click(sender As Object, e As EventArgs) Handles btnNotas.Click
        If cboEmpresa.Text <> "" Then
            basMain.VGCodEmpresaSel = CStr(VGCodEmpresa(cboEmpresa.SelectedIndex))
            frmNotas.ShowDialog()
        Else
            MsgBox("Debe escoger una empresa.")
            cboEmpresa.Focus()
        End If
    End Sub

    Private Sub btnBajar_Click(sender As Object, e As EventArgs) Handles btnBajar.Click
        'Creamos las variables
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet

        If cboEmpresa.Text <> "" And cboTipoDeBaja.Text <> "" Then

            Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Primero seteo la cláusula WHERE de acuerdo al tipo de bajada
            Dim sWhere As String = ""

            Select Case cboTipoDeBaja.SelectedIndex
                Case 0 ' No Asignados
                    sWhere = " WHERE Asignado = False "
                Case 1 ' Ingresados por el Usuario
                    sWhere = " WHERE [Ingresado por el Usuario] = True "
                Case 2 ' Modificados por el Usuario
                    sWhere = " WHERE [Modificado por el Usuario] = True "
            End Select

            'Añadimos el Libro al programa, y la hoja al libro
            exLibro = exApp.Workbooks.Add
            exHoja = exLibro.Worksheets.Add()

            exHoja.Columns("C").NumberFormat = "@"
            exHoja.Columns("D").NumberFormat = "@"

            'Recupero los nombres de los campos de la tabla "activo"
            Sql = "SELECT * FROM activo WHERE 1 = 2"
            Dim adaptador = New OleDb.OleDbDataAdapter(Sql, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "activo")

            Dim col As Integer = 0 ' Indice de la columna a desplegar
            Dim sNomColumna As String = ""

            For i = 0 To registro.Tables("activo").Columns.Count - 1
                sNomColumna = registro.Tables("activo").Columns(i).ColumnName
                col = col + 1
                exLibro.Sheets(1).cells(1, col) = sNomColumna
            Next

            'Consulta de todos los activos registrados
            Sql = "SELECT * FROM activo" & sWhere
            Dim da = New OleDb.OleDbDataAdapter(Sql, cn)
            Dim dt As New DataSet
            da.Fill(dt, "activo")

            Dim sValorCelda As String = ""

            If dt.Tables(0).Rows.Count > 0 Then

                Focos.Maximum = dt.Tables(0).Rows.Count

                'Aqui pasamos los datos de la tabla al libro excel
                For fila = 0 To dt.Tables(0).Rows.Count - 1
                    col = 0
                    For j = 0 To dt.Tables(0).Columns.Count - 1
                        sNomColumna = dt.Tables(0).Columns(j).ColumnName
                        col = col + 1
                        sValorCelda = dt.Tables(0).Rows(fila).Item(sNomColumna).ToString
                        Select Case UCase(sNomColumna)
                            Case "TIPO DE ACTIVO"
                                If sValorCelda <> "" Then
                                    Sql = "SELECT * FROM tipo_activo WHERE cod_tipo_activo = " + sValorCelda
                                    Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim dsT As New DataSet
                                    daT.Fill(dsT, "tabla")
                                    If dsT.Tables("tabla").Rows.Count > 0 Then
                                        exLibro.Sheets(1).cells(fila + 2, col) = dsT.Tables(0).Rows(0).Item("descripcion").ToString
                                    End If
                                    daT.Dispose()
                                    dsT.Dispose()
                                End If
                            Case "CLASE DE ACTIVO"
                                If sValorCelda <> "" Then
                                    Sql = "SELECT * FROM clase_activo WHERE cod_clase_activo = " + sValorCelda
                                    Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim dsT As New DataSet
                                    daT.Fill(dsT, "tabla")
                                    If dsT.Tables("tabla").Rows.Count > 0 Then
                                        exLibro.Sheets(1).cells(fila + 2, col) = dsT.Tables(0).Rows(0).Item("descripcion").ToString
                                    End If
                                    daT.Dispose()
                                    dsT.Dispose()
                                End If
                            Case "CIUDAD"
                                If sValorCelda <> "" Then
                                    Sql = "SELECT * FROM ciudad WHERE cod_ciudad = " + sValorCelda
                                    Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim dsT As New DataSet
                                    daT.Fill(dsT, "tabla")
                                    If dsT.Tables("tabla").Rows.Count > 0 Then
                                        exLibro.Sheets(1).cells(fila + 2, col) = dsT.Tables(0).Rows(0).Item("ci_nombre").ToString
                                    End If
                                    daT.Dispose()
                                    dsT.Dispose()
                                End If
                            Case "SUCURSAL"
                                If sValorCelda <> "" Then
                                    Sql = "SELECT * FROM sucursal WHERE cod_sucursal = " + sValorCelda
                                    Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim dsT As New DataSet
                                    daT.Fill(dsT, "tabla")
                                    If dsT.Tables("tabla").Rows.Count > 0 Then
                                        exLibro.Sheets(1).cells(fila + 2, col) = dsT.Tables(0).Rows(0).Item("su_nombre").ToString
                                    End If
                                    daT.Dispose()
                                    dsT.Dispose()
                                End If
                            Case "DEPARTAMENTO"
                                If sValorCelda <> "" Then
                                    Sql = "SELECT * FROM departamento WHERE cod_departamento = " + sValorCelda
                                    Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim dsT As New DataSet
                                    daT.Fill(dsT, "tabla")
                                    If dsT.Tables("tabla").Rows.Count > 0 Then
                                        exLibro.Sheets(1).cells(fila + 2, col) = dsT.Tables(0).Rows(0).Item("de_nombre").ToString
                                    End If
                                    daT.Dispose()
                                    dsT.Dispose()
                                End If
                            Case "SITUACION ACTIVO"
                                If sValorCelda <> "" Then
                                    Sql = "SELECT * FROM situacion WHERE cod_situacion = " + sValorCelda
                                    Dim daT As New OleDb.OleDbDataAdapter(Sql, cn)
                                    Dim dsT As New DataSet
                                    daT.Fill(dsT, "tabla")
                                    If dsT.Tables("tabla").Rows.Count > 0 Then
                                        exLibro.Sheets(1).cells(fila + 2, col) = dsT.Tables(0).Rows(0).Item("descripcion").ToString
                                    End If
                                    daT.Dispose()
                                    dsT.Dispose()
                                End If
                            Case Else
                                exLibro.Sheets(1).cells(fila + 2, col) = sValorCelda
                        End Select
                        If Not dt.Tables(0).Rows(fila).Item("Asignado") Then exApp.Cells(fila + 2, col).Font.Color = RGB(255, 0, 0)
                        If dt.Tables(0).Rows(fila).Item("Ingresado por el usuario") Then exApp.Cells(fila + 2, col).Font.Color = RGB(0, 0, 0)
                        If dt.Tables(0).Rows(fila).Item("Modificado por el usuario") Then exApp.Cells(fila + 2, col).Font.Color = RGB(0, 0, 255)

                    Next

                    dt.Dispose()

                    'Actualizo el "avance" del progressbar
                    Contador = Contador + 1
                    If Contador < Focos.Maximum Then
                        Focos.Value = Contador
                        lblPorcentaje.Text = CInt(Contador / Focos.Maximum * 100) & " %"
                    End If

                Next

                MsgBox("Total de Registros Bajados: " & Contador)

                Focos.Value = 0
                lblPorcentaje.Text = "0 %"

            End If

            exApp.Visible = True

            Cursor = System.Windows.Forms.Cursors.Default
        End If

        exApp = Nothing
        exLibro = Nothing
        exHoja = Nothing

    End Sub

    Private Sub txtCodigoOriginal_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigoOriginal.KeyDown
        If e.KeyCode = Keys.Enter Then
            ' Primero obtengo el cod_activo
            Sql = "select cod_activo from activo where [" & sNomCampo & "] = '" & Trim(Me.txtCodigoOriginal.Text) & "'"
            Dim da = New OleDb.OleDbDataAdapter(Sql, cn)
            Dim dt As New DataSet
            da.Fill(dt, "activo")
            Dim cod_activo As String = dt.Tables(0).Rows(0).Item("cod_activo")
            lblCodActivo.Text = Convert.ToInt64(cod_activo)
            FillFields(Convert.ToInt64(cod_activo))
            chkIngUsuario.Enabled = False
        End If
    End Sub

    Private Sub txtCodigoBarras_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigoBarras.KeyDown
        If e.KeyCode = Keys.Enter Then
            ' Primero obtengo el cod_activo
            Sql = "select cod_activo from activo where [" & sNomCampo & "] = '" & Trim(Me.txtCodigoBarras.Text) & "'"
            Dim da = New OleDb.OleDbDataAdapter(Sql, cn)
            Dim dt As New DataSet
            da.Fill(dt, "activo")
            Dim cod_activo As String = dt.Tables(0).Rows(0).Item("cod_activo")
            lblCodActivo.Text = Convert.ToInt64(cod_activo)
            FillFields(Convert.ToInt64(cod_activo))
            chkIngUsuario.Enabled = False
        End If
    End Sub

    Private Sub chkIngUsuario_CheckedChanged(sender As Object, e As EventArgs) Handles chkIngUsuario.CheckedChanged
        If chkIngUsuario.Checked Then FPIngresar()
    End Sub

    Private Sub chkModUsuario_CheckedChanged(sender As Object, e As EventArgs) Handles chkModUsuario.CheckedChanged
        If chkModUsuario.Checked Then FPModificar()
    End Sub

    Private Sub txtDescripcion_KeyUp(sender As Object, e As KeyEventArgs) Handles txtDescripcion.KeyUp
        If e.KeyCode = Keys.Escape Then
            ' Oculto la lista de resultados
            lstResultado.Visible = False
            txtDescripcion.Focus()
        ElseIf e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down Then
            lstResultado.SelectedIndex = lstResultado.SelectedIndex + 1
            lstResultado.Focus()
        End If
    End Sub

    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged
        Dim x As Integer = Me.txtDescripcion.Location.X
        Dim y As Integer = Me.txtDescripcion.Location.Y + Me.txtDescripcion.Height
        Dim width As Integer = Me.txtDescripcion.Width

        lstResultado.SetBounds(x, y, width, Height)

        lstResultado.Items.Clear()

        If iniciando Then Exit Sub

        ' Buscar en el DataTable usando el método Select
        ' que es como un filtro WHERE en una cadena de selección.

        ' El resultado se devuelve como un array de tipo DataRow
        Dim filas() As DataRow

        ' Si solo quieres mostrar los que empiecen por lo escrito.
        ' Al escribir "s" se buscarán los que empiecen por esa letra.
        filas = dt.Select("descripcion LIKE '" & txtDescripcion.Text & "%'")

        If Len(Trim(txtDescripcion.Text)) <= 0 Then
            lstResultado.Items.Clear()
            lstResultado.Visible = False
        Else
            ' Si hay datos, mostrar los apellidos
            If filas.Length > 0 Then

                ' Recorrer cada fila y mostrar los apellidos
                For Each dr As DataRow In filas

                    Me.lstResultado.Items.Add(dr("descripcion").ToString())

                Next
            End If
            With lstResultado
                If .Items.Count > 0 Then
                    If .Items.Count > 10 Then
                        .Height = 100
                    ElseIf .Items.Count > 3 Then
                        .Height = 60
                    Else
                        .Height = .Items.Count * 20
                    End If
                    .Visible = True
                    .BringToFront()
                Else
                    .Visible = False
                End If
            End With
        End If
    End Sub

    Private Sub lstResultado_DoubleClick(sender As Object, e As EventArgs) Handles lstResultado.DoubleClick
        ' Completo el texto de la caja de texto
        iniciando = True
        txtDescripcion.Text = lstResultado.Text
        ' Oculto la lista de resultados
        lstResultado.Visible = False
        txtDescripcion.Focus()
        iniciando = False
    End Sub

    Private Sub lstResultado_KeyDown(sender As Object, e As KeyEventArgs) Handles lstResultado.KeyDown
        If e.KeyCode = Keys.Enter Then
            ' Completo el texto de la caja de texto
            iniciando = True
            txtDescripcion.Text = lstResultado.Text
            ' Oculto la lista de resultados
            lstResultado.Visible = False
            txtDescripcion.Focus()
            iniciando = False
        ElseIf e.KeyCode = Keys.Escape Then
            'Oculto el listbox y regreso el cursor al campo descripcion
            iniciando = False
            ' Oculto la lista de resultados
            lstResultado.Visible = False
            txtDescripcion.Focus()
        End If
    End Sub

    Private Sub txtUsuario_KeyUp(sender As Object, e As KeyEventArgs) Handles txtUsuario.KeyUp
        If e.KeyCode = Keys.Escape Then
            ' Oculto la lista de resultados
            lstResponse.Visible = False
            txtUsuario.Focus()
        ElseIf e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down Then
            lstResponse.SelectedIndex = lstResponse.SelectedIndex + 1
            lstResponse.Focus()
        End If
    End Sub

    Private Sub txtUsuario_TextChanged(sender As Object, e As EventArgs) Handles txtUsuario.TextChanged
        Dim x As Integer = Me.txtUsuario.Location.X
        Dim y As Integer = Me.txtUsuario.Location.Y + Me.txtUsuario.Height
        Dim width As Integer = Me.txtUsuario.Width

        lstResponse.SetBounds(x, y, width, Height)

        lstResponse.Items.Clear()

        If iniciando Then Exit Sub

        ' Buscar en el DataTable usando el método Select
        ' que es como un filtro WHERE en una cadena de selección.

        ' El resultado se devuelve como un array de tipo DataRow
        Dim filas() As DataRow

        ' Si solo quieres mostrar los que empiecen por lo escrito.
        ' Al escribir "s" se buscarán los que empiecen por esa letra.
        filas = dtb.Select("nombre LIKE '" & txtUsuario.Text & "%'")

        If Len(Trim(txtUsuario.Text)) <= 0 Then
            lstResponse.Items.Clear()
            lstResponse.Visible = False
        Else
            ' Si hay datos, mostrar los apellidos
            If filas.Length > 0 Then

                ' Recorrer cada fila y mostrar los nombres
                For Each dr As DataRow In filas
                    Me.lstResponse.Items.Add(dr("nombre").ToString())
                Next
            End If
            With lstResponse
                If .Items.Count > 0 Then
                    If .Items.Count > 10 Then
                        .Height = 100
                    ElseIf .Items.Count > 3 Then
                        .Height = 60
                    Else
                        .Height = .Items.Count * 20
                    End If
                    .Visible = True
                    .BringToFront()
                Else
                    .Visible = False
                End If
            End With
        End If
    End Sub

    Private Sub lstResponse_DoubleClick(sender As Object, e As EventArgs) Handles lstResponse.DoubleClick
        ' Completo el texto de la caja de texto
        iniciando = True
        txtUsuario.Text = lstResponse.Text
        ' Obtengo el codigo del usuario de la tabla usuario_toma
        Sql = "SELECT [Cod Usuario] AS Usuario FROM activo WHERE Usuario = '" & Trim(txtUsuario.Text) & "'"
        Dim da = New OleDb.OleDbDataAdapter(Sql, cn)
        Dim dt As New DataSet
        da.Fill(dt, "usuario")
        txtCodUsuario.Text = dt.Tables(0).Rows(0).Item("Usuario")
        ' Oculto la lista de resultados
        lstResponse.Visible = False
        txtUsuario.Focus()
        iniciando = False
    End Sub

    Private Sub lstResponse_KeyDown(sender As Object, e As KeyEventArgs) Handles lstResponse.KeyDown
        If e.KeyCode = Keys.Enter Then
            ' Completo el texto de la caja de texto
            iniciando = True
            txtUsuario.Text = lstResponse.Text
            ' Oculto la lista de resultados
            lstResponse.Visible = False
            txtUsuario.Focus()
            ' Obtengo el codigo del usuario de la tabla usuario_toma
            Sql = "SELECT [Cod Usuario] AS Usuario FROM activo WHERE Usuario = '" & Trim(txtUsuario.Text) & "'"
            Dim da = New OleDb.OleDbDataAdapter(Sql, cn)
            Dim dt As New DataSet
            da.Fill(dt, "usuario")
            txtCodUsuario.Text = dt.Tables(0).Rows(0).Item("Usuario")
            iniciando = False
        ElseIf e.KeyCode = Keys.Escape Then
            'Oculto el listbox y regreso el cursor al campo descripcion
            iniciando = False
            ' Oculto la lista de resultados
            lstResponse.Visible = False
            txtUsuario.Focus()
        End If
    End Sub

    Private Sub FillFields(ByVal CodActivo As Long)
        ' Procedimiento para mostrar todos los datos del activo escogido
        Dim consulta As String = "SELECT * FROM activo WHERE cod_activo = " & Trim(CodActivo)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "activo")
        Dim NumRegistros As Byte = registro.Tables("activo").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar el activo...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            iniciando = True
            txtCodigoOriginal.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Codigo Original")), "", registro.Tables("activo").Rows(0).Item("Codigo Original"))
            txtCodigoBarras.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Codigo Barras")), "", registro.Tables("activo").Rows(0).Item("Codigo Barras"))

            'Procedimiento para recuperar el índice que corresponde al tipo de activo
            For j = 0 To UBound(VGCodTipoActivo, 1) - 1
                If Trim(VGCodTipoActivo(j)) = Trim(registro.Tables("activo").Rows(0).Item("Tipo de activo")) Then
                    cboTipoActivo.SelectedIndex = j
                    Exit For
                End If
            Next j

            txtDescripcion.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Descripcion 1")), "", registro.Tables("activo").Rows(0).Item("Descripcion 1"))

            'Procedimiento para recuperar el índice que corresponde a la clase de activo
            For j = 0 To UBound(VGCodClaseActivo, 1) - 1
                If Trim(VGCodClaseActivo(j)) = Trim(registro.Tables("activo").Rows(0).Item("Clase de activo")) Then
                    cboClaseActivo.SelectedIndex = j
                    Exit For
                End If
            Next j

            txtMarca.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Marca")), "", registro.Tables("activo").Rows(0).Item("Marca"))
            txtModelo.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Modelo")), "", registro.Tables("activo").Rows(0).Item("Modelo"))
            txtNroMotor.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Serie / # Motor")), "", registro.Tables("activo").Rows(0).Item("Serie / # Motor"))
            txtChasis.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("# Chasis")), "", registro.Tables("activo").Rows(0).Item("# Chasis"))
            txtPlaca.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("# Placa")), "", registro.Tables("activo").Rows(0).Item("# Placa"))
            txtColor.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Color")), "", registro.Tables("activo").Rows(0).Item("Color"))

            'sCodUsuario = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Usuario")), "", registro.Tables("activo").Rows(0).Item("Usuario"))
            'If sCodUsuario <> "" Then
            '    'Aquí busco en la tabla usuario_toma el nombre correspondiente y lo coloco en el textbox txtUsuario
            '    Dim query As String = "SELECT nombre FROM usuario_toma WHERE id_usuario = " & Trim(sCodUsuario)
            '    Dim adapter As New OleDb.OleDbDataAdapter(query, cn)
            '    Dim record As New DataSet
            '    adapter.Fill(record, "Usuario")
            '    Dim sNomUsuario As String = record.Tables("Usuario").Rows(0).Item("nombre")
            '    If sNomUsuario <> "" Then
            '        txtUsuario.Text = sNomUsuario
            '    End If
            'End If

            txtUsuario.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Usuario")), "", registro.Tables("activo").Rows(0).Item("Usuario"))
            txtCodUsuario.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Cod Usuario")), "", registro.Tables("activo").Rows(0).Item("Cod Usuario"))
            txtCargo.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Cargo")), "", registro.Tables("activo").Rows(0).Item("Cargo"))
            txtAreaFisica.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Area Fisica")), "", registro.Tables("activo").Rows(0).Item("Area Fisica"))
            txtNroMotor.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Serie / # Motor")), "", registro.Tables("activo").Rows(0).Item("Serie / # Motor"))

            If (registro.Tables("activo").Rows(0).Item("Fec Ubic Activo").ToString <> "") Then
                txtFecUbicActivo.Value = Convert.ToDateTime(registro.Tables("activo").Rows(0).Item("Fec Ubic Activo").ToString)
                txtFecUbicActivo.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
            Else
                txtFecUbicActivo.Value = Convert.ToDateTime(Now)
                txtFecUbicActivo.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Bold)
            End If

            'Procedimiento para recuperar el índice que corresponde a la ciudad
            For j = 0 To UBound(VGCodCiudad, 1) - 1
                If Trim(VGCodCiudad(j)) = Trim(registro.Tables("activo").Rows(0).Item("Ciudad")) Then
                    cboCiudad.SelectedIndex = j
                    Exit For
                End If
            Next j

            'Procedimiento para recuperar el índice que corresponde al departamento
            For j = 0 To UBound(VGCodDepartamento, 1) - 1
                If Trim(VGCodDepartamento(j)) = Trim(registro.Tables("activo").Rows(0).Item("Departamento")) Then
                    cboDepartamento.SelectedIndex = j
                    Exit For
                End If
            Next j

            'Procedimiento para recuperar el índice que corresponde a la sucursal
            For j = 0 To UBound(VGCodSucursal, 1) - 1
                If Trim(VGCodSucursal(j)) = Trim(registro.Tables("activo").Rows(0).Item("Sucursal")) Then
                    cboSucursal.SelectedIndex = j
                    Exit For
                End If
            Next j

            'Procedimiento para recuperar el índice que corresponde a la situación del activo
            For j = 0 To UBound(VGCodSituacionActivo, 1) - 1
                If Trim(VGCodSituacionActivo(j)) = Trim(registro.Tables("activo").Rows(0).Item("Situacion Activo")) Then
                    cboSituacion.SelectedIndex = j
                    Exit For
                End If
            Next j

            'txtFechaCompra.Value = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Fecha Compra")), Convert.ToDateTime(Now), Convert.ToDateTime(registro.Tables("activo").Rows(0).Item("Fecha Compra").ToString))

            If (registro.Tables("activo").Rows(0).Item("Fecha Compra").ToString <> "") Then
                Dim Posicion As Integer = InStr(registro.Tables("activo").Rows(0).Item("Fecha Compra").ToString, " ")
                Dim sFechaCompra As String
                If Posicion > 0 Then
                    sFechaCompra = Mid(registro.Tables("activo").Rows(0).Item("Fecha Compra").ToString, 1, InStr(registro.Tables("activo").Rows(0).Item("Fecha Compra").ToString, " ") - 1)
                Else
                    sFechaCompra = registro.Tables("activo").Rows(0).Item("Fecha Compra").ToString
                End If
                Dim FechaCompra As Date = Date.ParseExact(sFechaCompra, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                txtFechaCompra.Value = FechaCompra
                txtFechaCompra.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
            Else
                txtFechaCompra.Value = Convert.ToDateTime(Now)
                txtFechaCompra.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Bold)
            End If

            txtValorAdquisicion.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Valor Adquisicion")), "", registro.Tables("activo").Rows(0).Item("Valor Adquisicion"))
            txtValorLibros.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Valor Libros")), "", registro.Tables("activo").Rows(0).Item("Valor Libros"))
            txtPeriodoDepreciacion.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Periodo Depreciacion")), "", registro.Tables("activo").Rows(0).Item("Periodo Depreciacion"))
            txtVidaUtil.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Vida Util")), "", registro.Tables("activo").Rows(0).Item("Vida Util"))
            txtDepreciacion.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Deprec/Men/Anual")), "", registro.Tables("activo").Rows(0).Item("Deprec/Men/Anual"))
            txtAdicional1.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 1")), "", registro.Tables("activo").Rows(0).Item("Adicional 1"))
            txtAdicional2.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 2")), "", registro.Tables("activo").Rows(0).Item("Adicional 2"))
            txtAdicional3.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 3")), "", registro.Tables("activo").Rows(0).Item("Adicional 3"))
            txtAdicional4.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 4")), "", registro.Tables("activo").Rows(0).Item("Adicional 4"))
            txtAdicional5.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 5")), "", registro.Tables("activo").Rows(0).Item("Adicional 5"))
            txtAdicional6.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 6")), "", registro.Tables("activo").Rows(0).Item("Adicional 6"))
            txtAdicional7.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 7")), "", registro.Tables("activo").Rows(0).Item("Adicional 7"))
            txtAdicional8.Text = IIf(IsDBNull(registro.Tables("activo").Rows(0).Item("Adicional 8")), "", registro.Tables("activo").Rows(0).Item("Adicional 8"))

            'Deshabilito el checkbox para Ingresado por el usuario
            chkIngUsuario.Enabled = False
            'Habilito el checkbox para Modificado por el usuario
            chkModUsuario.Enabled = True

            iniciando = False
        End If
    End Sub

    Private Sub FPLimpiar()
        ' Procedimiento para limpiar los campos de la pantalla
        lblCodActivo.Text = 0
        txtFecUbicActivo.Text = CDate(Now)
        txtFecUbicActivo.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        txtFechaCompra.Text = CDate(Now)
        txtFechaCompra.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        txtCodigoOriginal.Text = ""
        txtCodigoBarras.Text = ""
        txtDescripcion.Text = ""
        txtMarca.Text = ""
        txtModelo.Text = ""
        txtNroMotor.Text = ""
        txtChasis.Text = ""
        txtPlaca.Text = ""
        txtColor.Text = ""
        txtUsuario.Text = ""
        txtCodUsuario.Text = ""
        txtCargo.Text = ""
        txtAreaFisica.Text = ""
        txtValorAdquisicion.Text = ""
        txtValorLibros.Text = ""
        txtPeriodoDepreciacion.Text = ""
        txtVidaUtil.Text = ""
        txtDepreciacion.Text = ""
        txtAdicional1.Text = ""
        txtAdicional2.Text = ""
        txtAdicional3.Text = ""
        txtAdicional4.Text = ""
        txtAdicional5.Text = ""
        txtAdicional6.Text = ""
        txtAdicional7.Text = ""
        txtAdicional8.Text = ""
        chkIngUsuario.Checked = False
        chkIngUsuario.Enabled = False
        chkModUsuario.Checked = False
        chkModUsuario.Enabled = False
        iniciando = False
    End Sub

    Private Sub FPLimpiar2()
        ' Procedimiento para limpiar los campos de la pantalla
        ' utilizado para la opción "Copiar"
        lblCodActivo.Text = 0
        txtFecUbicActivo.Text = CDate(Now)
        txtFecUbicActivo.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        txtFechaCompra.Text = CDate(Now)
        txtFechaCompra.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
        txtCodigoOriginal.Text = ""
        txtCodigoBarras.Text = ""
        'txtDescripcion.Text = ""   ' Este textbox no se borrará
        'txtMarca.Text = ""         ' Este textbox no se borrará
        'txtModelo.Text = ""        ' Este textbox no se borrará
        txtNroMotor.Text = ""
        txtChasis.Text = ""
        txtPlaca.Text = ""
        'txtColor.Text = ""         ' Este textbox no se borrará
        'txtUsuario.Text = ""
        txtCodUsuario.Text = ""
        txtCargo.Text = ""
        txtAreaFisica.Text = ""
        txtValorAdquisicion.Text = ""
        txtValorLibros.Text = ""
        txtPeriodoDepreciacion.Text = ""
        txtVidaUtil.Text = ""
        txtDepreciacion.Text = ""
        txtAdicional1.Text = ""
        txtAdicional2.Text = ""
        txtAdicional3.Text = ""
        txtAdicional4.Text = ""
        txtAdicional5.Text = ""
        txtAdicional6.Text = ""
        txtAdicional7.Text = ""
        txtAdicional8.Text = ""
        chkIngUsuario.Checked = False
        chkIngUsuario.Enabled = False
        chkModUsuario.Checked = False
        chkModUsuario.Enabled = False
        iniciando = False
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        FPLimpiar()
        'VLPasa = True
        chkIngUsuario.Checked = 0
        chkIngUsuario.Enabled = True
        'VLPasaCheckBoxIng = True
        chkModUsuario.Checked = 0
        chkModUsuario.Enabled = False
        'txtCodigoBarras.Text = sPrefijo
        txtCodigoBarras.Focus()
    End Sub

    Private Sub btnCopiar_Click(sender As Object, e As EventArgs) Handles btnCopiar.Click
        FPLimpiar2()
        'VLPasa = True
        chkIngUsuario.Checked = 0
        chkIngUsuario.Enabled = True
        'VLPasaCheckBoxIng = True
        chkModUsuario.Checked = 0
        chkModUsuario.Enabled = False
        'txtCodigoBarras.Text = sPrefijo
        txtCodigoBarras.Focus()
    End Sub

    Private Sub txtCodigoOriginal_TextChanged(sender As Object, e As EventArgs) Handles txtCodigoOriginal.TextChanged
        lstResultado.Items.Clear()
        If Len(Trim(txtCodigoOriginal.Text)) <= 0 Then
            lstResultado.Items.Clear()
            lstResultado.Visible = False
        End If
    End Sub

End Class