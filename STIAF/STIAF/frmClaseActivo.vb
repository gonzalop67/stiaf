﻿Public Class frmClaseActivo

    Private Sub frmClaseActivo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPGetTipoActivos()
        ' Estado de los botones de comando
        ' En todos los casos de niveles de seguridad
        btnModificar.Enabled = False     ' Modificar
        btnEliminar.Enabled = False     ' Eliminar
        If CInt(VGNivelSeg) <= 4 Then  ' Invitados y Usuarios de otros módulos
            btnIngresar.Enabled = False  ' Ingresar
        End If
    End Sub

    Private Sub FPGetTipoActivos()
        ' Obtengo los tipos de activos ingresados en la base de datos
        Dim consulta As String = "SELECT * FROM tipo_activo ORDER BY descripcion"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "tipo_activo")
        Dim NumRegistros As Byte = registro.Tables("tipo_activo").Rows.Count
        ReDim VGCodTipoActivo(NumRegistros)
        cboTipoActivo.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 1 To NumRegistros
                VGCodTipoActivo(i - 1) = registro.Tables("tipo_activo").Rows(i - 1).Item("cod_tipo_activo")
                cboTipoActivo.Items.Add(registro.Tables("tipo_activo").Rows(i - 1).Item("descripcion"))
            Next i
            cboTipoActivo.SelectedIndex = 0
            FPGetClaseActivos()
        End If
    End Sub

    Private Sub FPGetClaseActivos()
        Dim consulta As String = "SELECT * FROM clase_activo WHERE cod_tipo_activo = " & VGCodTipoActivo(cboTipoActivo.SelectedIndex) & " ORDER BY descripcion"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "clase_activo")
        Dim NumRegistros As Byte = registro.Tables("clase_activo").Rows.Count
        If NumRegistros = 0 Then
            MsgBox("No existen Clases de Activos asociadas al Tipo de Activo seleccionado...")
        Else
            ' mapeo los registros encontrados
            ReDim VGCodClaseActivo(NumRegistros)
            lstClaseActivo.Items.Clear()
            For i = 1 To NumRegistros
                VGCodClaseActivo(i - 1) = registro.Tables("clase_activo").Rows(i - 1).Item("cod_clase_activo")
                lstClaseActivo.Items.Add(registro.Tables("clase_activo").Rows(i - 1).Item("descripcion"))
            Next i
        End If
        TxtDescripcion.Focus()
    End Sub

    Private Sub cboTipoActivo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoActivo.SelectedIndexChanged
        FPGetClaseActivos()
    End Sub

    Private Sub lstClaseActivo_DoubleClick(sender As Object, e As EventArgs) Handles lstClaseActivo.DoubleClick
        ' Primero obtengo el cod_clase_activo
        TxtCodigo.Text = VGCodClaseActivo(lstClaseActivo.SelectedIndex)
        FillFields()
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM clase_activo WHERE cod_clase_activo = " & Trim(TxtCodigo.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "clase_activo")
        Dim NumRegistros As Byte = registro.Tables("clase_activo").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar el tipo de activo...")
        Else
            TxtDescripcion.Text = registro.Tables("clase_activo").Rows(0).Item("descripcion")
        End If
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = False
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ' Procedimiento para modificar la descripción del tipo de activo
        VGStrSQL$ = "UPDATE clase_activo SET descripcion = '"
        ' Campo que contiene el nombre
        VGStrSQL$ = VGStrSQL$ & Trim(TxtDescripcion.Text) & "'"
        ' Cláusula WHERE
        VGStrSQL$ = VGStrSQL$ & " where cod_clase_activo = " & TxtCodigo.Text
        Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
        comando.ExecuteNonQuery()
        MsgBox("El registro ha sido actualizado", vbInformation, "STIAF")
        FPGetClaseActivos()
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        FPLimpiar()
    End Sub

    Private Sub FPLimpiar()
        ' Borrar el contenido de los campos en pantalla
        TxtCodigo.Text = ""
        TxtDescripcion.Text = ""
        TxtDescripcion.Focus()
        ' Estado de los botones
        If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
            btnIngresar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If Trim(TxtDescripcion.Text) = "" Then
            MsgBox("Debe digitar el dato para la descripción del Tipo de Activo.", vbExclamation, "Error en el ingreso de datos")
            TxtDescripcion.Focus()
        ElseIf FPBuscarCampo("clase_activo", "descripcion", Trim(TxtDescripcion.Text)) Then
            MsgBox("La descripción de la Clase de Activo ya se encuentra ingresada " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtDescripcion.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla clase_activo
            VGStrSQL$ = "INSERT INTO clase_activo (cod_tipo_activo, descripcion) VALUES ("
            ' Código del Tipo de Activo
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodTipoActivo(cboTipoActivo.SelectedIndex)) & ","
            ' Nombre de la Empresa
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtDescripcion.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido insertado", vbInformation, "STIAF")
            ' Estado de los botones de comando
            If CInt(VGNivelSeg) > 4 Then  ' Administradores y Usuarios con permiso
                btnIngresar.Enabled = False
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            VGStrSQL$ = "SELECT max(cod_clase_activo) AS secuencial FROM clase_activo"
            Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "maximo")
            Dim NumRegistros As Byte = registro.Tables("maximo").Rows.Count
            If NumRegistros = 0 Then
                MsgBox("Ocurrió un error al recuperar el máximo cod_clase_activo...")
            Else
                TxtCodigo.Text = CStr(registro.Tables("maximo").Rows(0).Item("secuencial"))
            End If

            ' Actualizo la lista de empresas
            FPGetClaseActivos()
            FPLimpiar()
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla clase_activo
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            VGStrSQL$ = "delete * from clase_activo where cod_clase_activo = " & TxtCodigo.Text
            ' Ejecuta la sentencia SQL
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El registro ha sido eliminado", vbInformation, "STIAF")
            FPLimpiar()
            FPGetClaseActivos()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class