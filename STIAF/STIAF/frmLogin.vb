﻿Public Class frmLogin

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Primero se realiza la conexión
        Conectar()
        ' Esto es para la función de autocompletado
        Me.TxtLogin.AutoCompleteCustomSource = autoCompletarTextBox(Me.TxtLogin, "us_name", "usuario")
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
        cn.Close()
        Application.Exit()
    End Sub

    Private Sub btnConectar_Click(sender As Object, e As EventArgs) Handles btnConectar.Click
        If Trim(TxtLogin.Text) = "" Then
            MessageBox.Show("Ingrese su nombre de Usuario ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtLogin.Focus()
        ElseIf Trim(TxtClave.Text) = "" Then
            MessageBox.Show("Ingrese su clave de Usuario ...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtClave.Focus()
        Else
            Dim consulta As String = "SELECT * FROM usuario WHERE us_name = '" & Trim(TxtLogin.Text) & "' AND us_passwd = '" & EncryptString(Trim(TxtClave.Text), ENCRYPT) & "'"
            Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
            Dim registro As New DataSet
            adaptador.Fill(registro, "usuario")
            Dim lista As Byte = registro.Tables("usuario").Rows.Count
            If lista = 0 Then
                MessageBox.Show("Usuario no existe...")
            Else
                Dim VLTerminacion As Char
                If Trim(registro.Tables("usuario").Rows(0).Item("us_sexo")) = "M" Then
                    VLTerminacion = "O"
                Else
                    VLTerminacion = "A"
                End If
                Dim consulta2 As String = "SELECT usuario.cod_nivseg, ni_nombre FROM nivel_seguridad, usuario where nivel_seguridad.cod_nivseg = usuario.cod_nivseg and cod_usuario = " & Trim(registro.Tables("usuario").Rows(0).Item("cod_usuario"))
                Dim adaptador2 As New OleDb.OleDbDataAdapter(consulta2, cn)
                Dim registro2 As New DataSet
                adaptador2.Fill(registro2, "nivel_seguridad")

                Dim VLMensajeBienvenida As String = "           BIENVENID" + VLTerminacion + " " + _
                     registro.Tables("usuario").Rows(0).Item("us_fullname") + vbCrLf + " AL " + VGTituloSoftware + _
                     vbCrLf + vbCrLf + "           NIVEL DE ACCESO: " + registro2.Tables("nivel_seguridad").Rows(0).Item("ni_nombre")

                MessageBox.Show(VLMensajeBienvenida, "Mensaje de bienvenida...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                VGUsuario$ = registro.Tables("usuario").Rows(0).Item("us_name")
                VGPasswd$ = EncryptString(registro.Tables("usuario").Rows(0).Item("us_passwd"), DECRYPT)
                VGCodUsuario = registro.Tables("usuario").Rows(0).Item("cod_usuario")
                VGNivelSeg = registro.Tables("usuario").Rows(0).Item("cod_nivseg")
                ' Habilito los menus de acuerdo al nivel de seguridad
                frmMain.EspecificacionesToolStripMenuItem.Enabled = CInt(VGNivelSeg) > 2
                frmMain.ActivosToolStripMenuItem.Enabled = CInt(VGNivelSeg) > 2
                frmMain.UsuariosToolStripMenuItem.Enabled = CInt(VGNivelSeg) > 6
                ' Aquí viene el código para llamar al MDI
                frmMain.Show()
                ' Y luego se debe cerrar el formulario actual
                Me.Close()
            End If
        End If
    End Sub
End Class